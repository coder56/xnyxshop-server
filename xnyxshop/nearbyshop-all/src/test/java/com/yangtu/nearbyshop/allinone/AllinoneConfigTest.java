package com.yangtu.nearbyshop.allinone;

import cn.binarywang.wx.miniapp.config.WxMaConfig;
import com.github.binarywang.wxpay.config.WxPayConfig;
import com.github.binarywang.wxpay.exception.WxPayException;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class AllinoneConfigTest {
    @Autowired
    private Environment environment;

    @Autowired
    private WxMaConfig wxMaConfig;

    @Test
    public void test() {
        // 测试获取application-core.yml配置信息
        System.out.println(environment.getProperty("nearbyshop.express.appId"));
        // 测试获取application-db.yml配置信息
        System.out.println(environment.getProperty("spring.datasource.druid.url"));
        // 测试获取application-wx.yml配置信息
        System.out.println(environment.getProperty("nearbyshop.wx.app-id"));
        // 测试获取application-admin.yml配置信息
//        System.out.println(environment.getProperty(""));
        // 测试获取application.yml配置信息
        System.out.println(environment.getProperty("logging.level.com.yangtu.nearbyshop.wx"));
        System.out.println(environment.getProperty("logging.level.com.yangtu.nearbyshop.admin"));
        System.out.println(environment.getProperty("logging.level.com.yangtu.nearbyshop"));
    }

    @Test
    public void testWX() {
        System.out.println("apid=" + wxMaConfig.getAppid());
    }



}
