import com.yangtu.nearbyshop.core.notify.NotifyService;
import com.yangtu.nearbyshop.core.notify.NotifyType;
import com.yangtu.nearbyshop.db.dao.StatMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;
import java.util.Map;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class TestNotify {

    @Autowired
    private NotifyService notifyService;

    @Test
    public void testUser() {
        // 请依据自己的模版消息配置更改参数
        String[] parms = new String[]{
                "测试地址",
                "15116374637",
                "团长名字",
                "sn12323123",
                "goodsNames",

        };

        notifyService.notifyWxTemplate("o9KJp5Bo3UC0ayRnIehzit4m028E", NotifyType.ARRIVE, parms,
                "pages/index/index?orderId=1014");
    }

}
