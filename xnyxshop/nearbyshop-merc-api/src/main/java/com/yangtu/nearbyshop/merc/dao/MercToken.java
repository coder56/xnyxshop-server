package com.yangtu.nearbyshop.merc.dao;

import java.time.LocalDateTime;

public class MercToken {
    private Integer mercId;
    private String token;
    private String mercNo;
    private LocalDateTime expireTime;
    private LocalDateTime updateTime;

    public String getMercNo() {
        return mercNo;
    }

    public void setMercNo(String mercNo) {
        this.mercNo = mercNo;
    }

    public Integer getMercId() {
        return mercId;
    }

    public void setMercId(Integer mercId) {
        this.mercId = mercId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(LocalDateTime expireTime) {
        this.expireTime = expireTime;
    }

    public LocalDateTime getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(LocalDateTime updateTime) {
        this.updateTime = updateTime;
    }
}
