package com.yangtu.nearbyshop.merc.service;

import com.yangtu.nearbyshop.db.domain.NearbyshopMerc;
import com.yangtu.nearbyshop.db.service.NearbyshopMercService;
import com.yangtu.nearbyshop.merc.dao.MercInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class MercInfoService {
    @Autowired
    private NearbyshopMercService mercService;


    public MercInfo getInfo(Integer userId) {
        NearbyshopMerc merc = mercService.findById(userId);
        Assert.state(merc != null, "门店不存在");
        MercInfo userInfo = new MercInfo();
        userInfo.setNickName(merc.getNickname());
        userInfo.setAvatarUrl(merc.getAvatar());
        return userInfo;
    }
}
