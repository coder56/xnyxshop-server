package com.yangtu.nearbyshop.merc.service;

import com.yangtu.nearbyshop.core.util.CharUtil;
import com.yangtu.nearbyshop.merc.dao.MercToken;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * 维护用户token
 */
public class MercTokenManager {
    private static Map<String, MercToken> tokenMap = new HashMap<>();
    private static Map<Integer, MercToken> idMap = new HashMap<>();

    public static Integer getMercId(String token) {
        MercToken mercToken = tokenMap.get(token);
        if (mercToken == null) {
            return null;
        }

        if (mercToken.getExpireTime().isBefore(LocalDateTime.now())) {
            tokenMap.remove(token);
            idMap.remove(mercToken.getMercId());
            return null;
        }

        return mercToken.getMercId();
    }


    public static String getMercNo(Integer mercId) {
        MercToken userToken = idMap.get(mercId);
        if (userToken == null) {
            return null;
        }

        if (userToken.getExpireTime().isBefore(LocalDateTime.now())) {
            tokenMap.remove(userToken.getToken());
            idMap.remove(mercId);
            return null;
        }

        return userToken.getMercNo();
    }

    public static MercToken generateToken(Integer id, String mercNo) {
        MercToken mercToken = null;

        String token = CharUtil.getRandomString(32);
        while (tokenMap.containsKey(token)) {
            token = CharUtil.getRandomString(32);
        }

        LocalDateTime update = LocalDateTime.now();
        LocalDateTime expire = update.plusDays(30);

        mercToken = new MercToken();
        mercToken.setToken(token);
        mercToken.setUpdateTime(update);
        mercToken.setExpireTime(expire);
        mercToken.setMercId(id);
        mercToken.setMercNo(mercNo);
        tokenMap.put(token, mercToken);
        idMap.put(id, mercToken);

        return mercToken;
    }

    public static void removeToken(Integer userId) {
        MercToken mercToken = idMap.get(userId);
        String token = mercToken.getToken();
        idMap.remove(userId);
        tokenMap.remove(token);
    }
}
