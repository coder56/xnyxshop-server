package com.yangtu.nearbyshop.merc.web;

import com.github.pagehelper.PageInfo;
import com.yangtu.nearbyshop.db.domain.NearbyshopUser;
import com.yangtu.nearbyshop.db.service.NearbyshopUserService;
import com.yangtu.nearbyshop.merc.annotation.LoginMerc;
import com.yangtu.nearbyshop.merc.service.MercTokenManager;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.yangtu.nearbyshop.core.util.ResponseUtil;
import com.yangtu.nearbyshop.db.service.NearbyshopOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * 用户服务
 */
@RestController
@RequestMapping("/merc/user")
@Validated
public class MercUserController {
    private final Log logger = LogFactory.getLog(MercUserController.class);

    @Autowired
    private NearbyshopOrderService orderService;

    @Autowired
    private NearbyshopUserService userService;

    /**
     * 用户个人页面数据
     * <p>
     * 目前是用户订单统计信息
     *
     * @param userId 用户ID
     * @return 用户个人页面数据
     */
    @GetMapping("index")
    public Object list(@LoginMerc Integer userId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        Map<Object, Object> data = new HashMap<Object, Object>();
        data.put("order", orderService.orderInfo(userId));
        return ResponseUtil.ok(data);
    }

    /**
     * 订单列表
     *
     * @param mercId   用户ID
     * @param page     分页页数
     * @param size     分页大小
     * @return 订单列表
     */
    @GetMapping("list")
    public Object list(@LoginMerc Integer mercId,
                       @RequestParam(defaultValue = "") String mobile,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer size) {

        String mercNo = MercTokenManager.getMercNo(mercId);
        if (mercNo == null) {
            return  ResponseUtil.unlogin();
        }

        List<NearbyshopUser> userList = userService.queryByMercNo(mercNo, mobile, page, size);
        List<Map<String,Object>> userVoList = new ArrayList<>();
        long count = PageInfo.of(userList).getTotal();
        int totalPages = (int) Math.ceil((double) count / size);
        for (NearbyshopUser user: userList) {
            Map<String,Object> userVo = new HashMap<>();
            userVo.put("nickName", new String(Base64.getDecoder().decode(user.getNickname())));
            userVo.put("mobile", user.getMobile());
            userVo.put("avatarUrl", user.getAvatar());
            userVo.put("addTime", user.getAddTime());

            userVoList.add(userVo);
        }

        Map<String, Object> result = new HashMap<>();
        result.put("count", count);
        result.put("data", userVoList);
        result.put("totalPages", totalPages);
        return ResponseUtil.ok(result);
    }
}