package com.yangtu.nearbyshop.merc.service;

import com.yangtu.nearbyshop.db.domain.NearbyshopRegion;
import com.yangtu.nearbyshop.db.service.NearbyshopRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zhy
 * @date 2019-01-17 23:07
 **/
@Component
public class MercGetRegionService {

	@Autowired
	private NearbyshopRegionService regionService;

	private static List<NearbyshopRegion> nearbyshopRegions;

	protected List<NearbyshopRegion> getNearbyshopRegions() {
		if(nearbyshopRegions==null){
			createRegion();
	}
		return nearbyshopRegions;
	}

	private synchronized void createRegion(){
		if (nearbyshopRegions == null) {
			nearbyshopRegions = regionService.getAll();
		}
	}
}
