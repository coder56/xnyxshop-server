package com.yangtu.nearbyshop.wx.dao;

import com.yangtu.nearbyshop.db.domain.NearbyshopGoods;

import java.util.ArrayList;
import java.util.List;

public class CateGoodsVo {
    private String categoryName;
    private int categoryId;
    private List<NearbyshopGoods> goodsList = new ArrayList<>();

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public List<NearbyshopGoods> getGoodsList() {
        return goodsList;
    }

    public void setGoodsList(List<NearbyshopGoods> goodsList) {
        this.goodsList = goodsList;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }
}
