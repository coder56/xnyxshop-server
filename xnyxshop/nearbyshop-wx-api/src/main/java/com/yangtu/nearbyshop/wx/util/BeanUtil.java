package com.yangtu.nearbyshop.wx.util;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class BeanUtil {

    /**
     * 转换bean为map
     *
     * @param source 要转换的bean
     * @param <T>    bean类型
     * @return 转换结果
     */
    public static <T> Map<String, Object> bean2Map(T source) throws IllegalAccessException {
        Map<String, Object> result = new HashMap<>();

        Class<?> sourceClass = source.getClass();
        //拿到所有的字段,不包括继承的字段
        Field[] sourceFiled = sourceClass.getDeclaredFields();
        for (Field field : sourceFiled) {
            field.setAccessible(true);//设置可访问,不然拿不到private
            //配置了注解的话则使用注解名称,作为header字段
            FieldName fieldName = field.getAnnotation(FieldName.class);
            if (fieldName == null) {
                result.put(field.getName(), field.get(source));
            } else {
                if (fieldName.Ignore()) continue;
                result.put(fieldName.value(), field.get(source));
            }
        }
        return result;
    }
}
