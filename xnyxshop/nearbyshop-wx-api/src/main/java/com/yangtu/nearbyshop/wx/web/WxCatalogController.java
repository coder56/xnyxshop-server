package com.yangtu.nearbyshop.wx.web;

import com.yangtu.nearbyshop.core.system.SystemConfig;
import com.yangtu.nearbyshop.core.util.ResponseUtil;
import com.yangtu.nearbyshop.db.domain.NearbyshopCategory;
import com.yangtu.nearbyshop.db.domain.NearbyshopGoods;
import com.yangtu.nearbyshop.db.service.NearbyshopCategoryService;
import com.yangtu.nearbyshop.db.service.NearbyshopGoodsService;
import com.yangtu.nearbyshop.wx.dao.CateGoodsVo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.yangtu.nearbyshop.wx.service.HomeCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 类目服务
 */
@RestController
@RequestMapping("/wx/catalog")
@Validated
public class WxCatalogController {
    private final Log logger = LogFactory.getLog(WxCatalogController.class);

    @Autowired
    private NearbyshopCategoryService categoryService;

    @Autowired
    private NearbyshopGoodsService goodsService;

    /**
     * 分类详情
     *
     * @param id   分类类目ID。
     *             如果分类类目ID是空，则选择第一个分类类目。
     *             需要注意，这里分类类目是一级类目
     * @return 分类详情
     */
    @GetMapping("index")
    public Object index(Integer id) {

        // 所有一级分类目录
        List<NearbyshopCategory> l1CatList = categoryService.queryL1();

        // 当前一级分类目录
        NearbyshopCategory currentCategory = null;
        if (id != null) {
            currentCategory = categoryService.findById(id);
        } else {
            currentCategory = l1CatList.get(0);
        }

        // 当前一级分类目录对应的二级分类目录
        List<NearbyshopCategory> currentSubCategory = null;
        if (null != currentCategory) {
            currentSubCategory = categoryService.queryByPid(currentCategory.getId());
        }

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("categoryList", l1CatList);
        data.put("currentCategory", currentCategory);
        data.put("currentSubCategory", currentSubCategory);
        return ResponseUtil.ok(data);
    }

    /**
     * 所有分类数据
     *
     * @return 所有分类数据
     */
    @GetMapping("all")
    public Object queryAll() {
        //优先从缓存中读取
        if (HomeCacheManager.hasData(HomeCacheManager.CATALOG)) {
            return ResponseUtil.ok(HomeCacheManager.getCacheData(HomeCacheManager.CATALOG));
        }


        // 所有一级分类目录
        List<NearbyshopCategory> l1CatList = categoryService.queryL1();

        //所有子分类列表
        Map<Integer, List<NearbyshopCategory>> allList = new HashMap<>();
        List<NearbyshopCategory> sub;
        for (NearbyshopCategory category : l1CatList) {
            sub = categoryService.queryByPid(category.getId());
            allList.put(category.getId(), sub);
        }

        // 当前一级分类目录
        NearbyshopCategory currentCategory = l1CatList.get(0);

        // 当前一级分类目录对应的二级分类目录
        List<NearbyshopCategory> currentSubCategory = null;
        if (null != currentCategory) {
            currentSubCategory = categoryService.queryByPid(currentCategory.getId());
        }

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("categoryList", l1CatList);
        data.put("allList", allList);
        data.put("currentCategory", currentCategory);
        data.put("currentSubCategory", currentSubCategory);

        //缓存数据
        HomeCacheManager.loadData(HomeCacheManager.CATALOG, data);
        return ResponseUtil.ok(data);
    }

    /**
     * 当前分类栏目
     *
     * @param id 分类类目ID
     * @return 当前分类栏目
     */
    @GetMapping("current")
    public Object current(@NotNull Integer id) {
        // 当前分类
        NearbyshopCategory currentCategory = categoryService.findById(id);
        List<NearbyshopCategory> currentSubCategory = categoryService.queryByPid(currentCategory.getId());

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("currentCategory", currentCategory);
        data.put("currentSubCategory", currentSubCategory);
        return ResponseUtil.ok(data);
    }

    /**
     * 当前分类栏目
     *
     * @param id 分类类目ID
     * @return 当前分类栏目
     */
    @GetMapping("currentGoods")
    public Object currentGoods(@NotNull Integer id) {
        // 当前分类
        NearbyshopCategory currentCategory = categoryService.findById(id);
        List<NearbyshopCategory> currentSubCategory = categoryService.queryByPid(currentCategory.getId());
        //所有子分类列表

        List<CateGoodsVo> allGoods = new ArrayList<>();
        CateGoodsVo cateGoodsVo;
        for (NearbyshopCategory category : currentSubCategory) {
            cateGoodsVo = new CateGoodsVo();
            cateGoodsVo.setCategoryId(category.getId());
            cateGoodsVo.setCategoryName(category.getName());
            List<NearbyshopGoods> goodsList = goodsService.queryByCategory(category.getId(), 0, 20);
            for(NearbyshopGoods goods : goodsList){
                goods.setPicUrl(goods.getPicUrl() +"?x-oss-process=image/resize,m_fill,h_170,w_170");
            }
            cateGoodsVo.setGoodsList(goodsList);
            allGoods.add(cateGoodsVo);
        }

        Map<String, Object> data = new HashMap<String, Object>();
        data.put("currentCategory", currentCategory);
        data.put("currentSubCategory", currentSubCategory);
        data.put("allGoods", allGoods);
        return ResponseUtil.ok(data);
    }

    /**
     * 所有推荐分类
     *
     * @return 所有分类数据
     */
    @GetMapping("allPush")
    public Object queryAllPush() {
        List<NearbyshopCategory> l1CatList = categoryService.queryAllByPush();
        return ResponseUtil.ok(l1CatList);
    }
}
