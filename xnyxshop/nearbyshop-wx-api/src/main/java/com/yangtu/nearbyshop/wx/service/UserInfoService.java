package com.yangtu.nearbyshop.wx.service;

import com.yangtu.nearbyshop.db.domain.NearbyshopUser;
import com.yangtu.nearbyshop.db.service.NearbyshopUserService;
import com.yangtu.nearbyshop.wx.dao.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class UserInfoService {
    @Autowired
    private NearbyshopUserService userService;


    public UserInfo getInfo(Integer userId) {
        NearbyshopUser user = userService.findById(userId);
        Assert.state(user != null, "用户不存在");
        UserInfo userInfo = new UserInfo();
        userInfo.setNickName(user.getNickname());
        userInfo.setAvatarUrl(user.getAvatar());
        return userInfo;
    }
}
