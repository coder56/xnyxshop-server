/*
 Navicat Premium Data Transfer

 Source Server         : aliyun-yangtu
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : rm-uf69p65120x6tb5z1xo.mysql.rds.aliyuncs.com:3306
 Source Schema         : testshop

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 05/09/2019 13:31:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for nearbyshop_role
-- ----------------------------
DROP TABLE IF EXISTS `nearbyshop_role`;
CREATE TABLE `nearbyshop_role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  `desc` varchar(1023) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '角色描述',
  `enabled` tinyint(1) DEFAULT 1 COMMENT '是否启用',
  `add_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name_UNIQUE`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nearbyshop_role
-- ----------------------------
INSERT INTO `nearbyshop_role` VALUES (1, '超级管理员', '所有模块的权限', 1, '2019-01-01 00:00:00', '2019-05-05 14:15:52', 0);
INSERT INTO `nearbyshop_role` VALUES (2, '商场管理员', '只有商场模块的操作权限', 1, '2019-01-01 00:00:00', '2019-04-30 10:56:17', 0);
INSERT INTO `nearbyshop_role` VALUES (3, '推广管理员', '只有推广模块的操作权限', 1, '2019-01-01 00:00:00', '2019-04-30 10:56:14', 0);
INSERT INTO `nearbyshop_role` VALUES (4, '门店管理', '分润及开通门店', 1, '2019-06-21 15:47:01', '2019-06-21 15:47:01', 0);

SET FOREIGN_KEY_CHECKS = 1;
