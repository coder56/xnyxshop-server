/*
 Navicat Premium Data Transfer

 Source Server         : aliyun-yangtu
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : rm-uf69p65120x6tb5z1xo.mysql.rds.aliyuncs.com:3306
 Source Schema         : testshop

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 05/09/2019 13:25:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for nearbyshop_permission
-- ----------------------------
DROP TABLE IF EXISTS `nearbyshop_permission`;
CREATE TABLE `nearbyshop_permission`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT NULL COMMENT '角色ID',
  `permission` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '权限',
  `add_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 95 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nearbyshop_permission
-- ----------------------------
INSERT INTO `nearbyshop_permission` VALUES (1, 1, '*', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 0);
INSERT INTO `nearbyshop_permission` VALUES (2, 2, 'admin:category:read', '2019-01-07 15:18:53', '2019-01-07 15:18:53', 1);
INSERT INTO `nearbyshop_permission` VALUES (3, 2, 'admin:category:update', '2019-01-07 15:18:53', '2019-01-07 15:18:53', 1);
INSERT INTO `nearbyshop_permission` VALUES (4, 2, 'admin:category:delete', '2019-01-07 15:18:53', '2019-01-07 15:18:53', 1);
INSERT INTO `nearbyshop_permission` VALUES (5, 2, 'admin:category:create', '2019-01-07 15:18:53', '2019-01-07 15:18:53', 1);
INSERT INTO `nearbyshop_permission` VALUES (6, 2, 'admin:category:list', '2019-01-07 15:18:53', '2019-01-07 15:18:53', 1);
INSERT INTO `nearbyshop_permission` VALUES (7, 2, 'admin:brand:create', '2019-01-07 15:18:53', '2019-01-07 15:18:53', 1);
INSERT INTO `nearbyshop_permission` VALUES (8, 2, 'admin:brand:list', '2019-01-07 15:18:53', '2019-01-07 15:18:53', 1);
INSERT INTO `nearbyshop_permission` VALUES (9, 2, 'admin:brand:delete', '2019-01-07 15:18:53', '2019-01-07 15:18:53', 1);
INSERT INTO `nearbyshop_permission` VALUES (10, 2, 'admin:brand:read', '2019-01-07 15:18:53', '2019-01-07 15:18:53', 1);
INSERT INTO `nearbyshop_permission` VALUES (11, 2, 'admin:brand:update', '2019-01-07 15:18:53', '2019-01-07 15:18:53', 1);
INSERT INTO `nearbyshop_permission` VALUES (12, 3, 'admin:ad:list', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (13, 3, 'admin:ad:delete', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (14, 3, 'admin:ad:create', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (15, 3, 'admin:ad:update', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (16, 3, 'admin:ad:read', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (17, 3, 'admin:groupon:list', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (18, 3, 'admin:groupon:update', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (19, 3, 'admin:groupon:create', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (20, 3, 'admin:groupon:read', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (21, 3, 'admin:groupon:delete', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (22, 3, 'admin:topic:create', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (23, 3, 'admin:topic:read', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (24, 3, 'admin:topic:list', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (25, 3, 'admin:topic:delete', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (26, 3, 'admin:topic:update', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (27, 3, 'admin:coupon:list', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (28, 3, 'admin:coupon:delete', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (29, 3, 'admin:coupon:read', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (30, 3, 'admin:coupon:create', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (31, 3, 'admin:coupon:update', '2019-01-07 15:18:57', '2019-01-07 15:18:57', 0);
INSERT INTO `nearbyshop_permission` VALUES (32, 2, 'admin:brand:update', '2019-05-05 12:39:28', '2019-05-05 12:39:28', 1);
INSERT INTO `nearbyshop_permission` VALUES (33, 2, 'admin:brand:read', '2019-05-05 12:39:28', '2019-05-05 12:39:28', 1);
INSERT INTO `nearbyshop_permission` VALUES (34, 2, 'admin:brand:delete', '2019-05-05 12:39:28', '2019-05-05 12:39:28', 1);
INSERT INTO `nearbyshop_permission` VALUES (35, 2, 'admin:brand:create', '2019-05-05 12:39:28', '2019-05-05 12:39:28', 1);
INSERT INTO `nearbyshop_permission` VALUES (36, 2, 'admin:brand:list', '2019-05-05 12:39:28', '2019-05-05 12:39:28', 1);
INSERT INTO `nearbyshop_permission` VALUES (37, 2, 'admin:category:update', '2019-05-05 12:39:28', '2019-05-05 12:39:28', 1);
INSERT INTO `nearbyshop_permission` VALUES (38, 2, 'admin:category:read', '2019-05-05 12:39:28', '2019-05-05 12:39:28', 1);
INSERT INTO `nearbyshop_permission` VALUES (39, 2, 'admin:category:delete', '2019-05-05 12:39:28', '2019-05-05 12:39:28', 1);
INSERT INTO `nearbyshop_permission` VALUES (40, 2, 'admin:category:create', '2019-05-05 12:39:28', '2019-05-05 12:39:28', 1);
INSERT INTO `nearbyshop_permission` VALUES (41, 2, 'admin:category:list', '2019-05-05 12:39:28', '2019-05-05 12:39:28', 1);
INSERT INTO `nearbyshop_permission` VALUES (42, 2, 'admin:brand:update', '2019-06-21 15:27:39', '2019-06-21 15:27:39', 1);
INSERT INTO `nearbyshop_permission` VALUES (43, 2, 'admin:brand:read', '2019-06-21 15:27:39', '2019-06-21 15:27:39', 1);
INSERT INTO `nearbyshop_permission` VALUES (44, 2, 'admin:brand:delete', '2019-06-21 15:27:39', '2019-06-21 15:27:39', 1);
INSERT INTO `nearbyshop_permission` VALUES (45, 2, 'admin:brand:create', '2019-06-21 15:27:39', '2019-06-21 15:27:39', 1);
INSERT INTO `nearbyshop_permission` VALUES (46, 2, 'admin:brand:list', '2019-06-21 15:27:39', '2019-06-21 15:27:39', 1);
INSERT INTO `nearbyshop_permission` VALUES (47, 2, 'admin:category:update', '2019-06-21 15:27:39', '2019-06-21 15:27:39', 1);
INSERT INTO `nearbyshop_permission` VALUES (48, 2, 'admin:category:read', '2019-06-21 15:27:39', '2019-06-21 15:27:39', 1);
INSERT INTO `nearbyshop_permission` VALUES (49, 2, 'admin:category:delete', '2019-06-21 15:27:39', '2019-06-21 15:27:39', 1);
INSERT INTO `nearbyshop_permission` VALUES (50, 2, 'admin:category:create', '2019-06-21 15:27:39', '2019-06-21 15:27:39', 1);
INSERT INTO `nearbyshop_permission` VALUES (51, 2, 'admin:category:list', '2019-06-21 15:27:39', '2019-06-21 15:27:39', 1);
INSERT INTO `nearbyshop_permission` VALUES (52, 4, 'admin:merc:detail', '2019-06-21 18:05:14', '2019-06-21 18:05:14', 1);
INSERT INTO `nearbyshop_permission` VALUES (53, 4, 'admin:merc:update', '2019-06-21 18:05:14', '2019-06-21 18:05:14', 1);
INSERT INTO `nearbyshop_permission` VALUES (54, 4, 'admin:merc:delete', '2019-06-21 18:05:14', '2019-06-21 18:05:14', 1);
INSERT INTO `nearbyshop_permission` VALUES (55, 4, 'admin:merc:list', '2019-06-21 18:05:14', '2019-06-21 18:05:14', 1);
INSERT INTO `nearbyshop_permission` VALUES (56, 4, 'admin:merc:profit', '2019-06-21 18:05:14', '2019-06-21 18:05:14', 1);
INSERT INTO `nearbyshop_permission` VALUES (57, 4, 'admin:merc:create', '2019-06-21 18:05:14', '2019-06-21 18:05:14', 1);
INSERT INTO `nearbyshop_permission` VALUES (58, 4, 'admin:config:merc:list', '2019-06-21 18:05:14', '2019-06-21 18:05:14', 1);
INSERT INTO `nearbyshop_permission` VALUES (59, 4, 'admin:config:merc:updateConfigs', '2019-06-21 18:05:14', '2019-06-21 18:05:14', 1);
INSERT INTO `nearbyshop_permission` VALUES (60, 4, 'admin:merc:detail', '2019-06-21 18:07:02', '2019-06-21 18:07:02', 1);
INSERT INTO `nearbyshop_permission` VALUES (61, 4, 'admin:merc:update', '2019-06-21 18:07:02', '2019-06-21 18:07:02', 1);
INSERT INTO `nearbyshop_permission` VALUES (62, 4, 'admin:merc:delete', '2019-06-21 18:07:02', '2019-06-21 18:07:02', 1);
INSERT INTO `nearbyshop_permission` VALUES (63, 4, 'admin:merc:list', '2019-06-21 18:07:02', '2019-06-21 18:07:02', 1);
INSERT INTO `nearbyshop_permission` VALUES (64, 4, 'admin:merc:profit', '2019-06-21 18:07:02', '2019-06-21 18:07:02', 1);
INSERT INTO `nearbyshop_permission` VALUES (65, 4, 'admin:merc:create', '2019-06-21 18:07:02', '2019-06-21 18:07:02', 1);
INSERT INTO `nearbyshop_permission` VALUES (66, 4, 'admin:config:merc:list', '2019-06-21 18:07:02', '2019-06-21 18:07:02', 1);
INSERT INTO `nearbyshop_permission` VALUES (67, 4, 'admin:config:merc:updateConfigs', '2019-06-21 18:07:02', '2019-06-21 18:07:02', 1);
INSERT INTO `nearbyshop_permission` VALUES (68, 4, 'admin:config:merc:list', '2019-06-24 09:40:00', '2019-06-24 09:40:00', 0);
INSERT INTO `nearbyshop_permission` VALUES (69, 4, 'admin:config:merc:updateConfigs', '2019-06-24 09:40:00', '2019-06-24 09:40:00', 0);
INSERT INTO `nearbyshop_permission` VALUES (70, 4, 'admin:merc:detail', '2019-06-24 09:40:00', '2019-06-24 09:40:00', 0);
INSERT INTO `nearbyshop_permission` VALUES (71, 4, 'admin:merc:update', '2019-06-24 09:40:00', '2019-06-24 09:40:00', 0);
INSERT INTO `nearbyshop_permission` VALUES (72, 4, 'admin:merc:delete', '2019-06-24 09:40:00', '2019-06-24 09:40:00', 0);
INSERT INTO `nearbyshop_permission` VALUES (73, 4, 'admin:merc:list', '2019-06-24 09:40:00', '2019-06-24 09:40:00', 0);
INSERT INTO `nearbyshop_permission` VALUES (74, 4, 'admin:merc:profit', '2019-06-24 09:40:00', '2019-06-24 09:40:00', 0);
INSERT INTO `nearbyshop_permission` VALUES (75, 4, 'admin:merc:create', '2019-06-24 09:40:00', '2019-06-24 09:40:00', 0);
INSERT INTO `nearbyshop_permission` VALUES (76, 4, 'admin:stat:user', '2019-06-24 09:40:00', '2019-06-24 09:40:00', 0);
INSERT INTO `nearbyshop_permission` VALUES (77, 4, 'admin:stat:order', '2019-06-24 09:40:00', '2019-06-24 09:40:00', 0);
INSERT INTO `nearbyshop_permission` VALUES (78, 4, 'admin:stat:goods', '2019-06-24 09:40:00', '2019-06-24 09:40:00', 0);
INSERT INTO `nearbyshop_permission` VALUES (79, 2, 'admin:goods:read', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (80, 2, 'admin:goods:updatenum', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (81, 2, 'admin:goods:update', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (82, 2, 'admin:goods:list', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (83, 2, 'admin:goods:delete', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (84, 2, 'admin:goods:create', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (85, 2, 'admin:category:update', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (86, 2, 'admin:category:read', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (87, 2, 'admin:category:delete', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (88, 2, 'admin:category:create', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (89, 2, 'admin:category:list', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (90, 2, 'admin:comment:delete', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (91, 2, 'admin:comment:list', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (92, 2, 'admin:dispatch:merclist', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (93, 2, 'admin:dispatch:userlist', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);
INSERT INTO `nearbyshop_permission` VALUES (94, 2, 'admin:dispatch:list', '2019-08-15 17:00:53', '2019-08-15 17:00:53', 0);

SET FOREIGN_KEY_CHECKS = 1;
