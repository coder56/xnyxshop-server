/*
 Navicat Premium Data Transfer

 Source Server         : aliyun-yangtu
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : rm-uf69p65120x6tb5z1xo.mysql.rds.aliyuncs.com:3306
 Source Schema         : testshop

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 05/09/2019 13:20:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for nearbyshop_admin
-- ----------------------------
DROP TABLE IF EXISTS `nearbyshop_admin`;
CREATE TABLE `nearbyshop_admin`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员名称',
  `password` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '管理员密码',
  `last_login_ip` varchar(63) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '最近一次登录IP地址',
  `last_login_time` datetime(0) DEFAULT NULL COMMENT '最近一次登录时间',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '\'' COMMENT '头像图片',
  `add_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) DEFAULT 0 COMMENT '逻辑删除',
  `role_ids` varchar(127) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '[]' COMMENT '角色列表',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '管理员表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nearbyshop_admin
-- ----------------------------
INSERT INTO `nearbyshop_admin` VALUES (1, 'admin123', '$2a$10$Bhsaqln.RgYe75Naw7GN0OFoC8G5T/k7ce6wmdYduEMkI9kU/mTRe', NULL, NULL, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', '2018-02-01 00:00:00', '2019-05-20 16:19:53', 0, '[1]');
INSERT INTO `nearbyshop_admin` VALUES (4, 'promotion123', '$2a$10$wDZLOLLnzZ1EFZ3ldZ1XFOUWDEX6TnQCUFdJz4g.PoMaLTzS8TjWq', '', NULL, '\'', '2019-01-07 15:16:59', '2019-01-07 15:17:34', 0, '[3]');
INSERT INTO `nearbyshop_admin` VALUES (5, 'mall123', '$2a$10$9SB5ki0ziaeunA/1ogzexOuaFl.mzR22IMsuRiPDukNuJJ0PVKY3S', '', NULL, '\'', '2019-01-07 15:17:25', '2019-08-17 11:10:29', 0, '[2]');
INSERT INTO `nearbyshop_admin` VALUES (6, 'admin123test', '$2a$10$WqYy4bd8sCWebbVYiOMl0OnCHyDQcQMG2jakhdz..P9OpKsYPMVwC', '', NULL, '\'', '2019-05-06 15:43:56', '2019-05-06 15:43:56', 0, '[1]');
INSERT INTO `nearbyshop_admin` VALUES (7, 'admin1', '$2a$10$RbIewqEmja9KWECe7NGrKObetzLI4f0RFFzNjymUYo1.U70ot/ZJy', '', NULL, '\'', '2019-05-07 09:31:51', '2019-08-19 09:56:41', 0, '[1]');
INSERT INTO `nearbyshop_admin` VALUES (8, 'test123', '$2a$10$dM47v5l6BnoNYhd6YAHlf.qySUOAZibwxaPuFOxIOcq749Pe/fT9a', '', NULL, 'https://yangtuoss.oss-cn-shanghai.aliyuncs.com/3aefr942bzi73ehvjhca.jpg', '2019-06-21 15:27:07', '2019-06-21 18:06:04', 0, '[4]');
INSERT INTO `nearbyshop_admin` VALUES (9, 'wentao', '$2a$10$hLUIZsAwfQpw06Rq8zLJJe1vZ3U0M/UxuALI2/oRkgYi52M66nPWC', '', NULL, '\'', '2019-08-15 17:03:08', '2019-08-17 11:10:45', 0, '[2]');
INSERT INTO `nearbyshop_admin` VALUES (10, 'zhanglei', '$2a$10$06R62xgT/GiY0IXFMlrvk.1gtkjJd9MMyq6.VRz7yNRE.3En/5TFO', '', NULL, 'https://yangtuoss.oss-cn-shanghai.aliyuncs.com/orq5y0rp9f4z0jhg44cc.jpg', '2019-08-19 14:13:23', '2019-08-19 15:08:05', 0, '[1]');

SET FOREIGN_KEY_CHECKS = 1;
