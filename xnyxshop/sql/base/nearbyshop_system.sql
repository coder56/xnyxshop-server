/*
 Navicat Premium Data Transfer

 Source Server         : aliyun-yangtu
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : rm-uf69p65120x6tb5z1xo.mysql.rds.aliyuncs.com:3306
 Source Schema         : testshop

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 05/09/2019 13:32:30
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for nearbyshop_system
-- ----------------------------
DROP TABLE IF EXISTS `nearbyshop_system`;
CREATE TABLE `nearbyshop_system`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统配置名',
  `key_value` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '系统配置值',
  `add_time` datetime(0) DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime(0) DEFAULT NULL COMMENT '更新时间',
  `deleted` tinyint(1) DEFAULT 0 COMMENT '逻辑删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 35 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '系统配置表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of nearbyshop_system
-- ----------------------------
INSERT INTO `nearbyshop_system` VALUES (1, 'nearbyshop.system.banner.new.title', '大家都在买的', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (2, 'nearbyshop.system.banner.new.imageurl', 'http://yanxuan.nosdn.127.net/8976116db321744084774643a933c5ce.png', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (3, 'nearbyshop.system.banner.hot.title', '大家都在买的', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (4, 'nearbyshop.system.banner.hot.imageurl', 'http://yanxuan.nosdn.127.net/8976116db321744084774643a933c5ce.png', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (5, 'nearbyshop.system.freight.value', '8', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (6, 'nearbyshop.system.freight.limit', '88', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (7, 'nearbyshop.system.indexlimit.new', '6', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (8, 'nearbyshop.system.indexlimit.hot', '6', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (9, 'nearbyshop.system.indexlimit.brand', '4', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (10, 'nearbyshop.system.indexlimit.topic', '4', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (11, 'nearbyshop.system.indexlimit.catloglist', '4', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (12, 'nearbyshop.system.indexlimit.catloggood', '4', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (13, 'nearbyshop.system.mallname', 'nearbyshop', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (14, 'nearbyshop.system.shareimage.autocreate', '1', '2018-02-01 00:00:00', '2018-02-01 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (15, 'nearbyshop.system.merc.referpercent', '0.1', '2019-06-05 00:00:00', '2019-06-05 00:00:00', 0);
INSERT INTO `nearbyshop_system` VALUES (16, 'nearbyshop_mall_phone', '021-xxxx-xxxx', '2019-06-06 15:42:03', '2019-06-27 16:01:48', 0);
INSERT INTO `nearbyshop_system` VALUES (17, 'nearbyshop_wx_index_hot', '60', '2019-06-06 15:42:03', '2019-08-22 21:47:11', 0);
INSERT INTO `nearbyshop_system` VALUES (18, 'nearbyshop_mall_qq', '738696120', '2019-06-06 15:42:03', '2019-06-27 16:01:48', 0);
INSERT INTO `nearbyshop_system` VALUES (19, 'nearbyshop_order_comment', '7', '2019-06-06 15:42:03', '2019-08-22 15:32:14', 0);
INSERT INTO `nearbyshop_system` VALUES (20, 'nearbyshop_wx_catlog_list', '4', '2019-06-06 15:42:03', '2019-08-22 21:47:11', 0);
INSERT INTO `nearbyshop_system` VALUES (21, 'nearbyshop_express_freight_value', '8', '2019-06-06 15:42:03', '2019-06-06 15:42:03', 0);
INSERT INTO `nearbyshop_system` VALUES (22, 'nearbyshop_order_unpaid', '30', '2019-06-06 15:42:03', '2019-08-22 15:32:14', 0);
INSERT INTO `nearbyshop_system` VALUES (23, 'nearbyshop_wx_index_brand', '4', '2019-06-06 15:42:03', '2019-08-22 21:47:11', 0);
INSERT INTO `nearbyshop_system` VALUES (24, 'nearbyshop_express_freight_min', '88', '2019-06-06 15:42:04', '2019-06-06 15:42:04', 0);
INSERT INTO `nearbyshop_system` VALUES (25, 'nearbyshop_mall_name', '乡念优选', '2019-06-06 15:42:04', '2019-06-27 16:01:48', 0);
INSERT INTO `nearbyshop_system` VALUES (26, 'nearbyshop_wx_index_new', '60', '2019-06-06 15:42:04', '2019-08-22 21:47:11', 0);
INSERT INTO `nearbyshop_system` VALUES (27, 'nearbyshop_wx_share', 'true', '2019-06-06 15:42:04', '2019-08-22 21:47:11', 0);
INSERT INTO `nearbyshop_system` VALUES (28, 'nearbyshop_merc_referpercent', '0.1', '2019-06-06 15:42:04', '2019-06-19 16:39:01', 0);
INSERT INTO `nearbyshop_system` VALUES (29, 'nearbyshop_wx_catlog_goods', '4', '2019-06-06 15:42:04', '2019-08-22 21:47:11', 0);
INSERT INTO `nearbyshop_system` VALUES (30, 'nearbyshop_order_unconfirm', '7', '2019-06-06 15:42:04', '2019-08-22 15:32:14', 0);
INSERT INTO `nearbyshop_system` VALUES (31, 'nearbyshop_wx_index_topic', '4', '2019-06-06 15:42:04', '2019-08-22 21:47:11', 0);
INSERT INTO `nearbyshop_system` VALUES (32, 'nearbyshop_mall_address', '山东省临沂市', '2019-06-06 15:42:04', '2019-06-27 16:01:48', 0);
INSERT INTO `nearbyshop_system` VALUES (33, 'nearbyshop_wx_open_status', '0', '2019-08-21 23:01:19', '2019-08-22 21:47:11', 0);
INSERT INTO `nearbyshop_system` VALUES (34, 'nearbyshop_wx_open_timestr', '8/23 8:00--8/23 22:00', '2019-08-21 23:01:59', '2019-08-22 21:47:11', 0);

SET FOREIGN_KEY_CHECKS = 1;
