package com.yangtu.nearbyshop.db.service;

import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopGoodsMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopGoods;
import com.yangtu.nearbyshop.db.domain.NearbyshopGoodsExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class NearbyshopGoodsService {
    NearbyshopGoods.Column[] columns = new NearbyshopGoods.Column[]{NearbyshopGoods.Column.id, NearbyshopGoods.Column.name,
            NearbyshopGoods.Column.brief, NearbyshopGoods.Column.picUrl, NearbyshopGoods.Column.isHot,
            NearbyshopGoods.Column.isNew, NearbyshopGoods.Column.counterPrice,
            NearbyshopGoods.Column.retailPrice,NearbyshopGoods.Column.coverUrl,
            NearbyshopGoods.Column.adverUrl};
    @Resource
    private NearbyshopGoodsMapper goodsMapper;

    /**
     * 获取热卖商品
     *
     * @param offset
     * @param limit
     * @return
     */
    public List<NearbyshopGoods> queryByHot(int offset, int limit) {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        example.or().andIsHotEqualTo(true).andIsOnSaleEqualTo(true).andDeletedEqualTo(false);
        example.setOrderByClause("add_time desc");
        PageHelper.startPage(offset, limit);

        return goodsMapper.selectByExampleSelective(example, columns);
    }

    /**
     * 获取新品上市
     *
     * @param offset
     * @param limit
     * @return
     */
    public List<NearbyshopGoods> queryByNew(int offset, int limit) {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        example.or().andIsNewEqualTo(true).andIsOnSaleEqualTo(true).andDeletedEqualTo(false);
        example.setOrderByClause("add_time desc");
        PageHelper.startPage(offset, limit);

        return goodsMapper.selectByExampleSelective(example, columns);
    }

    /**
     * 获取分类下的商品
     *
     * @param catList
     * @param offset
     * @param limit
     * @return
     */
    public List<NearbyshopGoods> queryByCategory(List<Integer> catList, int offset, int limit) {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        example.or().andCategoryIdIn(catList).andIsOnSaleEqualTo(true).andDeletedEqualTo(false);
        example.setOrderByClause("add_time  desc");
        PageHelper.startPage(offset, limit);

        return goodsMapper.selectByExampleSelective(example, columns);
    }


    /**
     * 获取分类下的商品
     *
     * @param catId
     * @param offset
     * @param limit
     * @return
     */
    public List<NearbyshopGoods> queryByCategory(Integer catId, int offset, int limit) {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        example.or().andCategoryIdEqualTo(catId).andIsOnSaleEqualTo(true).andDeletedEqualTo(false);
        example.setOrderByClause("add_time desc");
        PageHelper.startPage(offset, limit);

        return goodsMapper.selectByExampleSelective(example, columns);
    }


    public List<NearbyshopGoods> querySelective(Integer catId, Integer brandId, String keywords, Boolean isHot, Boolean isNew, Integer offset, Integer limit, String sort, String order) {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        NearbyshopGoodsExample.Criteria criteria1 = example.or();
        NearbyshopGoodsExample.Criteria criteria2 = example.or();

        if (!StringUtils.isEmpty(catId) && catId != 0) {
            criteria1.andCategoryIdEqualTo(catId);
            criteria2.andCategoryIdEqualTo(catId);
        }
        if (!StringUtils.isEmpty(brandId)) {
            criteria1.andBrandIdEqualTo(brandId);
            criteria2.andBrandIdEqualTo(brandId);
        }
        if (!StringUtils.isEmpty(isNew)) {
            criteria1.andIsNewEqualTo(isNew);
            criteria2.andIsNewEqualTo(isNew);
        }
        if (!StringUtils.isEmpty(isHot)) {
            criteria1.andIsHotEqualTo(isHot);
            criteria2.andIsHotEqualTo(isHot);
        }
        if (!StringUtils.isEmpty(keywords)) {
            criteria1.andKeywordsLike("%" + keywords + "%");
            criteria2.andNameLike("%" + keywords + "%");
        }
        criteria1.andIsOnSaleEqualTo(true);
        criteria2.andIsOnSaleEqualTo(true);
        criteria1.andDeletedEqualTo(false);
        criteria2.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(offset, limit);

        return goodsMapper.selectByExampleSelective(example, columns);
    }

    public List<NearbyshopGoods> querySelective(String goodsSn, String name, Integer categoryId, Integer page,
                                                Integer size, String sort, String order,String type) {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        NearbyshopGoodsExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(goodsSn)) {
            criteria.andGoodsSnEqualTo(goodsSn);
        }
        if (!StringUtils.isEmpty(name)) {
            criteria.andNameLike("%" + name + "%");
        }

        if (null != categoryId) {
            criteria.andCategoryIdEqualTo(categoryId);
        }

        if (!StringUtils.isEmpty(type)) {
            if(type.equals("1")){
                criteria.andIsOnSaleEqualTo(true);
            }else if(type.equals("2")){
                criteria.andPushStatusEqualTo(1);
            }else if(type.equals("3")){
                criteria.andIsOnSaleEqualTo(false);
            }
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return goodsMapper.selectByExampleWithBLOBs(example);
    }

    /**
     * 获取某个商品信息,包含完整信息
     *
     * @param id
     * @return
     */
    public NearbyshopGoods findById(Integer id) {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        example.or().andIdEqualTo(id).andDeletedEqualTo(false);
        return goodsMapper.selectOneByExampleWithBLOBs(example);
    }

    /**
     * 获取某个商品信息，仅展示相关内容
     *
     * @param id
     * @return
     */
    public NearbyshopGoods findByIdVO(Integer id) {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        example.or().andIdEqualTo(id).andIsOnSaleEqualTo(true).andDeletedEqualTo(false);
        return goodsMapper.selectOneByExampleSelective(example, columns);
    }


    /**
     * 获取所有在售物品总数
     *
     * @return
     */
    public Integer queryOnSale() {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        example.or().andIsOnSaleEqualTo(true).andDeletedEqualTo(false);
        return (int) goodsMapper.countByExample(example);
    }

    public int updateById(NearbyshopGoods goods) {
        goods.setUpdateTime(LocalDateTime.now());
        return goodsMapper.updateByPrimaryKeySelective(goods);
    }

    public void deleteById(Integer id) {
        goodsMapper.logicalDeleteByPrimaryKey(id);
    }

    public void add(NearbyshopGoods goods) {
        goods.setAddTime(LocalDateTime.now());
        goods.setUpdateTime(LocalDateTime.now());
        goodsMapper.insertSelective(goods);
    }

    /**
     * 获取所有物品总数，包括在售的和下架的，但是不包括已删除的商品
     *
     * @return
     */
    public int count() {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        example.or().andDeletedEqualTo(false);
        return (int) goodsMapper.countByExample(example);
    }

    public List<Integer> getCatIds(Integer brandId, String keywords, Boolean isHot, Boolean isNew) {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        NearbyshopGoodsExample.Criteria criteria1 = example.or();
        NearbyshopGoodsExample.Criteria criteria2 = example.or();

        if (!StringUtils.isEmpty(brandId)) {
            criteria1.andBrandIdEqualTo(brandId);
            criteria2.andBrandIdEqualTo(brandId);
        }
        if (!StringUtils.isEmpty(isNew)) {
            criteria1.andIsNewEqualTo(isNew);
            criteria2.andIsNewEqualTo(isNew);
        }
        if (!StringUtils.isEmpty(isHot)) {
            criteria1.andIsHotEqualTo(isHot);
            criteria2.andIsHotEqualTo(isHot);
        }
        if (!StringUtils.isEmpty(keywords)) {
            criteria1.andKeywordsLike("%" + keywords + "%");
            criteria2.andNameLike("%" + keywords + "%");
        }
        criteria1.andIsOnSaleEqualTo(true);
        criteria2.andIsOnSaleEqualTo(true);
        criteria1.andDeletedEqualTo(false);
        criteria2.andDeletedEqualTo(false);

        List<NearbyshopGoods> goodsList = goodsMapper.selectByExampleSelective(example, NearbyshopGoods.Column.categoryId);
        List<Integer> cats = new ArrayList<Integer>();
        for (NearbyshopGoods goods : goodsList) {
            cats.add(goods.getCategoryId());
        }
        return cats;
    }

    public boolean checkExistByName(String name) {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        example.or().andNameEqualTo(name).andIsOnSaleEqualTo(true).andDeletedEqualTo(false);
        return goodsMapper.countByExample(example) != 0;
    }

    public int reduceLimit(Integer id, Short num){
        return goodsMapper.reduceLimit(id, num);
    }

    public NearbyshopGoods getLastOne(){
        return goodsMapper.getLastOne();
    }

    /**
     * 获取所有推荐商品
     *
     * @return
     */
    public List<NearbyshopGoods> queryAllByPush() {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        example.or().andPushStatusEqualTo(1) .andIsOnSaleEqualTo(true)
                .andDeletedEqualTo(false);
        example.setOrderByClause("push_sort asc");
        NearbyshopGoods.Column[] columns = new NearbyshopGoods.Column[]{NearbyshopGoods.Column.id,
                NearbyshopGoods.Column.pushStatus,NearbyshopGoods.Column.pushSort};
        return goodsMapper.selectByExampleSelective(example, columns);
    }

    /**
     * 获取所有推荐商品
     *
     * @return
     */
    public List<NearbyshopGoods> queryByPush(int offset, int limit) {
        NearbyshopGoodsExample example = new NearbyshopGoodsExample();
        example.or().andPushStatusEqualTo(1) .andIsOnSaleEqualTo(true)
                .andDeletedEqualTo(false);
        example.setOrderByClause("push_sort asc");
        PageHelper.startPage(offset, limit);
        List<NearbyshopGoods> list = goodsMapper.selectByExampleSelective(example, columns);
        for(NearbyshopGoods goods:list){
            if(!StringUtils.isEmpty(goods.getCoverUrl())){
                goods.setPicUrl(goods.getCoverUrl());
            }
        }
        return list;
    }

    /**
     * 计算商品数量
     * @param type 1:L1  2:l2
     * @return
     */
    public Integer countByCond(int type,Integer id) {
        if(type == 1){
            return goodsMapper.countByL1(id);
        }else if(type == 2){
            return goodsMapper.countByL2(id);
        }
        return 0;
    }
}
