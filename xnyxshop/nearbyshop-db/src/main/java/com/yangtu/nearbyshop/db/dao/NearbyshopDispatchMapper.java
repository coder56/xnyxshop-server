package com.yangtu.nearbyshop.db.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopDispatch;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yangtu.nearbyshop.db.vo.NearbyshopDispatchVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 配送表 Mapper 接口
 * </p>
 *
 * @author wanglei
 * @since 2019-08-28
 */
public interface NearbyshopDispatchMapper extends BaseMapper<NearbyshopDispatch> {

    IPage<NearbyshopDispatchVo> getDispatchList(Page<NearbyshopDispatchVo> pageVo,
            @Param("startTime") String startTime,@Param("endTime") String endTime,
                                                @Param("dispatchStatus") Integer dispatchStatus);

    void updateDispatchByPno(@Param("id") Integer id,@Param("pno") String pno,
                             @Param("purchaseStatus") Integer purchaseStatus,
            @Param("dispatchStatus") Integer dispatchStatus);

}
