package com.yangtu.nearbyshop.db.service;

import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopAddressMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopAddress;
import com.yangtu.nearbyshop.db.domain.NearbyshopAddressExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class NearbyshopAddressService {
    @Resource
    private NearbyshopAddressMapper addressMapper;

    public List<NearbyshopAddress> queryByUid(Integer uid) {
        NearbyshopAddressExample example = new NearbyshopAddressExample();
        example.or().andUserIdEqualTo(uid).andDeletedEqualTo(false);
        return addressMapper.selectByExample(example);
    }

    public NearbyshopAddress findById(Integer id) {
        return addressMapper.selectByPrimaryKey(id);
    }

    public int add(NearbyshopAddress address) {
        address.setAddTime(LocalDateTime.now());
        address.setUpdateTime(LocalDateTime.now());
        return addressMapper.insertSelective(address);
    }

    public int update(NearbyshopAddress address) {
        address.setUpdateTime(LocalDateTime.now());
        return addressMapper.updateByPrimaryKeySelective(address);
    }

    public void delete(Integer id) {
        addressMapper.logicalDeleteByPrimaryKey(id);
    }

    public NearbyshopAddress findDefault(Integer userId) {
        NearbyshopAddressExample example = new NearbyshopAddressExample();
        example.or().andUserIdEqualTo(userId).andIsDefaultEqualTo(true).andDeletedEqualTo(false);
        return addressMapper.selectOneByExample(example);
    }

    public void resetDefault(Integer userId) {
        NearbyshopAddress address = new NearbyshopAddress();
        address.setIsDefault(false);
        address.setUpdateTime(LocalDateTime.now());
        NearbyshopAddressExample example = new NearbyshopAddressExample();
        example.or().andUserIdEqualTo(userId).andDeletedEqualTo(false);
        addressMapper.updateByExampleSelective(address, example);
    }

    public List<NearbyshopAddress> querySelective(Integer userId, String name, Integer page, Integer limit, String sort, String order) {
        NearbyshopAddressExample example = new NearbyshopAddressExample();
        NearbyshopAddressExample.Criteria criteria = example.createCriteria();

        if (userId != null) {
            criteria.andUserIdEqualTo(userId);
        }
        if (!StringUtils.isEmpty(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, limit);
        return addressMapper.selectByExample(example);
    }
}
