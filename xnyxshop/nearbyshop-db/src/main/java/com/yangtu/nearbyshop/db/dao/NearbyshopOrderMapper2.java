package com.yangtu.nearbyshop.db.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopOrder2;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopPurchase;
import com.yangtu.nearbyshop.db.vo.CouponUseVo;
import com.yangtu.nearbyshop.db.vo.SaleCountVo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author wanglei
 * @since 2019-08-30
 */
public interface NearbyshopOrderMapper2 extends BaseMapper<NearbyshopOrder2> {

    IPage<NearbyshopOrder2> selectByPage(Page page,
                                           @Param("order") NearbyshopOrder2 order);

    IPage<SaleCountVo>  sumAllSale(Page page,@Param("startTime") String startTime,@Param("endTime") String endTime);

    IPage<SaleCountVo> sumAllSaleByGoods(Page page,@Param("startTime") String startTime,
                                        @Param("endTime") String endTime,
                                        @Param("goodsSn") String goodsSn);

    IPage<SaleCountVo> sumAllSaleByMerc(Page page,@Param("startTime") String startTime,
                                        @Param("endTime") String endTime,
                                        @Param("mercNo") String mercNo);

    IPage<SaleCountVo> sumAllSaleByBd(Page page,@Param("startTime") String startTime,
                                       @Param("endTime") String endTime,
                                       @Param("bdNo") String bdNo);

    String sumAllUser( @Param("mercNo") String mercNo,@Param("startTime") String startTime,
                                      @Param("endTime") String endTime);

    IPage<CouponUseVo> getCouponUse(@Param("startTime") String startTime,
                                     @Param("endTime") String endTime, @Param("couponId") Integer couponId,
                                    @Param("mercNo") String mercNo);
}
