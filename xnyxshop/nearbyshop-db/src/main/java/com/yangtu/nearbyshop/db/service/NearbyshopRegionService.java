package com.yangtu.nearbyshop.db.service;

import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopRegionMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopRegion;
import com.yangtu.nearbyshop.db.domain.NearbyshopRegionExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;

@Service
public class NearbyshopRegionService {

    @Resource
    private NearbyshopRegionMapper regionMapper;

    public List<NearbyshopRegion> getAll(){
        NearbyshopRegionExample example = new NearbyshopRegionExample();
        byte b = 4;
        example.or().andTypeNotEqualTo(b);
        return regionMapper.selectByExample(example);
    }

    public List<NearbyshopRegion> queryByPid(Integer parentId) {
        NearbyshopRegionExample example = new NearbyshopRegionExample();
        example.or().andPidEqualTo(parentId);
        return regionMapper.selectByExample(example);
    }

    public NearbyshopRegion findById(Integer id) {
        return regionMapper.selectByPrimaryKey(id);
    }

    public List<NearbyshopRegion> querySelective(String name, Integer code, Integer page, Integer size, String sort, String order) {
        NearbyshopRegionExample example = new NearbyshopRegionExample();
        NearbyshopRegionExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        if (!StringUtils.isEmpty(code)) {
            criteria.andCodeEqualTo(code);
        }

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return regionMapper.selectByExample(example);
    }
}
