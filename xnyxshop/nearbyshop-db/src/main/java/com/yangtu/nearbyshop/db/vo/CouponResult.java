package com.yangtu.nearbyshop.db.vo;

import java.util.ArrayList;
import java.util.List;

public class CouponResult {
    private Integer total;
    private List<CouponUseVo> items = new ArrayList<>();

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<CouponUseVo> getItems() {
        return items;
    }

    public void setItems(List<CouponUseVo> items) {
        this.items = items;
    }
}
