package com.yangtu.nearbyshop.db.service;

import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopStorageMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopStorage;
import com.yangtu.nearbyshop.db.domain.NearbyshopStorageExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class NearbyshopStorageService {
    @Autowired
    private NearbyshopStorageMapper storageMapper;

    public void deleteByKey(String key) {
        NearbyshopStorageExample example = new NearbyshopStorageExample();
        example.or().andKeyEqualTo(key);
        storageMapper.logicalDeleteByExample(example);
    }

    public void add(NearbyshopStorage storageInfo) {
        storageInfo.setAddTime(LocalDateTime.now());
        storageInfo.setUpdateTime(LocalDateTime.now());
        storageMapper.insertSelective(storageInfo);
    }

    public NearbyshopStorage findByKey(String key) {
        NearbyshopStorageExample example = new NearbyshopStorageExample();
        example.or().andKeyEqualTo(key).andDeletedEqualTo(false);
        return storageMapper.selectOneByExample(example);
    }

    public int update(NearbyshopStorage storageInfo) {
        storageInfo.setUpdateTime(LocalDateTime.now());
        return storageMapper.updateByPrimaryKeySelective(storageInfo);
    }

    public NearbyshopStorage findById(Integer id) {
        return storageMapper.selectByPrimaryKey(id);
    }

    public List<NearbyshopStorage> querySelective(String key, String name, Integer page, Integer limit, String sort, String order) {
        NearbyshopStorageExample example = new NearbyshopStorageExample();
        NearbyshopStorageExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(key)) {
            criteria.andKeyEqualTo(key);
        }
        if (!StringUtils.isEmpty(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, limit);
        return storageMapper.selectByExample(example);
    }
}
