package com.yangtu.nearbyshop.db.vo;

public class OrderSettleVo {
    int totalGoosNum;
    String totalOrderPrice;
    String totalActualPrice;

    public int getTotalGoosNum() {
        return totalGoosNum;
    }

    public void setTotalGoosNum(int totalGoosNum) {
        this.totalGoosNum = totalGoosNum;
    }

    public String getTotalOrderPrice() {
        return totalOrderPrice;
    }

    public void setTotalOrderPrice(String totalOrderPrice) {
        this.totalOrderPrice = totalOrderPrice;
    }

    public String getTotalActualPrice() {
        return totalActualPrice;
    }

    public void setTotalActualPrice(String totalActualPrice) {
        this.totalActualPrice = totalActualPrice;
    }
}
