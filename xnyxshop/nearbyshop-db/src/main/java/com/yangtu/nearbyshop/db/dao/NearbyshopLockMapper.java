package com.yangtu.nearbyshop.db.dao;

import com.yangtu.nearbyshop.db.domain.NearbyshopLock;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 锁定记录表 Mapper 接口
 * </p>
 *
 * @author wanglei
 * @since 2019-08-30
 */
public interface NearbyshopLockMapper extends BaseMapper<NearbyshopLock> {

}
