package com.yangtu.nearbyshop.db.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * BD门店销售统计表
 * </p>
 *
 * @author wanglei
 * @since 2019-09-18
 */
public class NearbyshopBdSettle implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * BD编号
     */
    private String bdNo;

    /**
     * 时间区间
     */
    private String timeDur;

    /**
     * 销售金额
     */
    private BigDecimal amount;

    /**
     * 创建时间
     */
    private Date addTime;

    @TableField(exist = false)
    private String bdName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBdNo() {
        return bdNo;
    }

    public void setBdNo(String bdNo) {
        this.bdNo = bdNo;
    }

    public String getTimeDur() {
        return timeDur;
    }

    public void setTimeDur(String timeDur) {
        this.timeDur = timeDur;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getBdName() {
        return bdName;
    }

    public void setBdName(String bdName) {
        this.bdName = bdName;
    }

    @Override
    public String toString() {
        return "NearbyshopBdSettle{" +
        "id=" + id +
        ", bdNo=" + bdNo +
        ", timeDur=" + timeDur +
        ", amount=" + amount +
        ", addTime=" + addTime +
        "}";
    }
}
