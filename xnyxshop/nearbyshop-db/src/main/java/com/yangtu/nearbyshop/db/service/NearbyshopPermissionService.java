package com.yangtu.nearbyshop.db.service;

import com.yangtu.nearbyshop.db.dao.NearbyshopPermissionMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopPermission;
import com.yangtu.nearbyshop.db.domain.NearbyshopPermissionExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class NearbyshopPermissionService {
    @Resource
    private NearbyshopPermissionMapper permissionMapper;

    public Set<String> queryByRoleIds(Integer[] roleIds) {
        Set<String> permissions = new HashSet<String>();
        if(roleIds.length == 0){
            return permissions;
        }

        NearbyshopPermissionExample example = new NearbyshopPermissionExample();
        example.or().andRoleIdIn(Arrays.asList(roleIds)).andDeletedEqualTo(false);
        List<NearbyshopPermission> permissionList = permissionMapper.selectByExample(example);

        for(NearbyshopPermission permission : permissionList){
            permissions.add(permission.getPermission());
        }

        return permissions;
    }


    public Set<String> queryByRoleId(Integer roleId) {
        Set<String> permissions = new HashSet<String>();
        if(roleId == null){
            return permissions;
        }

        NearbyshopPermissionExample example = new NearbyshopPermissionExample();
        example.or().andRoleIdEqualTo(roleId).andDeletedEqualTo(false);
        List<NearbyshopPermission> permissionList = permissionMapper.selectByExample(example);

        for(NearbyshopPermission permission : permissionList){
            permissions.add(permission.getPermission());
        }

        return permissions;
    }

    public boolean checkSuperPermission(Integer roleId) {
        if(roleId == null){
            return false;
        }

        NearbyshopPermissionExample example = new NearbyshopPermissionExample();
        example.or().andRoleIdEqualTo(roleId).andPermissionEqualTo("*").andDeletedEqualTo(false);
        return permissionMapper.countByExample(example) != 0;
    }

    public void deleteByRoleId(Integer roleId) {
        NearbyshopPermissionExample example = new NearbyshopPermissionExample();
        example.or().andRoleIdEqualTo(roleId).andDeletedEqualTo(false);
        permissionMapper.logicalDeleteByExample(example);
    }

    public void add(NearbyshopPermission nearbyshopPermission) {
        nearbyshopPermission.setAddTime(LocalDateTime.now());
        nearbyshopPermission.setUpdateTime(LocalDateTime.now());
        permissionMapper.insertSelective(nearbyshopPermission);
    }
}
