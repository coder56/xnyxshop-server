package com.yangtu.nearbyshop.db.service.itf;

import com.yangtu.nearbyshop.db.domain.NearbyshopBdSettle;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * BD门店销售统计表 服务类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-18
 */
public interface INearbyshopBdSettleService extends IService<NearbyshopBdSettle> {

}
