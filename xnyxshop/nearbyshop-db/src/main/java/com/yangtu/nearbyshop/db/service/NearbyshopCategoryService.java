package com.yangtu.nearbyshop.db.service;

import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopCategoryMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopCategory;
import com.yangtu.nearbyshop.db.domain.NearbyshopCategoryExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class NearbyshopCategoryService {
    @Resource
    private NearbyshopCategoryMapper categoryMapper;
    private NearbyshopCategory.Column[] CHANNEL = {NearbyshopCategory.Column.id, NearbyshopCategory.Column.name, NearbyshopCategory.Column.iconUrl};

    public List<NearbyshopCategory> queryL1WithoutRecommend(int offset, int limit) {
        NearbyshopCategoryExample example = new NearbyshopCategoryExample();
        example.or().andLevelEqualTo("L1").andNameNotEqualTo("推荐").andDeletedEqualTo(false);
        PageHelper.startPage(offset, limit);
        return categoryMapper.selectByExample(example);
    }

    public List<NearbyshopCategory> queryL1(int offset, int limit) {
        NearbyshopCategoryExample example = new NearbyshopCategoryExample();
        example.or().andLevelEqualTo("L1").andDeletedEqualTo(false);
        PageHelper.startPage(offset, limit);
        return categoryMapper.selectByExample(example);
    }

    public List<NearbyshopCategory> queryL1() {
        NearbyshopCategoryExample example = new NearbyshopCategoryExample();
        example.or().andLevelEqualTo("L1").andDeletedEqualTo(false);
        return categoryMapper.selectByExample(example);
    }

    public List<NearbyshopCategory> queryByPid(Integer pid) {
        NearbyshopCategoryExample example = new NearbyshopCategoryExample();
        example.or().andPidEqualTo(pid).andDeletedEqualTo(false);
        return categoryMapper.selectByExample(example);
    }

    public List<NearbyshopCategory> queryL2ByIds(List<Integer> ids) {
        NearbyshopCategoryExample example = new NearbyshopCategoryExample();
        example.or().andIdIn(ids).andLevelEqualTo("L2").andDeletedEqualTo(false);
        return categoryMapper.selectByExample(example);
    }

    public NearbyshopCategory findById(Integer id) {
        return categoryMapper.selectByPrimaryKey(id);
    }

    public List<NearbyshopCategory> querySelective(String id, String name, Integer page,
                                                   Integer size, String sort, String order,String type) {
        NearbyshopCategoryExample example = new NearbyshopCategoryExample();
        NearbyshopCategoryExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(id)) {
            criteria.andIdEqualTo(Integer.valueOf(id));
        }
        if (!StringUtils.isEmpty(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        if (!StringUtils.isEmpty(type)) {
            if(type.equals("1")){
                criteria.andLevelEqualTo("L1");
            }else if(type.equals("2")){
                criteria.andLevelEqualTo("L2");
            }else if(type.equals("3")){
                criteria.andPushStatusEqualTo(1);
            }
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return categoryMapper.selectByExample(example);
    }

    public int updateById(NearbyshopCategory category) {
        category.setUpdateTime(LocalDateTime.now());
        return categoryMapper.updateByPrimaryKeySelective(category);
    }

    public void deleteById(Integer id) {
        categoryMapper.logicalDeleteByPrimaryKey(id);
    }

    public void add(NearbyshopCategory category) {
        category.setAddTime(LocalDateTime.now());
        category.setUpdateTime(LocalDateTime.now());
        categoryMapper.insertSelective(category);
    }

    public List<NearbyshopCategory> queryChannel() {
        NearbyshopCategoryExample example = new NearbyshopCategoryExample();
        example.or().andLevelEqualTo("L1").andDeletedEqualTo(false);
        return categoryMapper.selectByExampleSelective(example, CHANNEL);
    }

    /**
     * 获取所有推荐
     *
     * @return
     */
    public List<NearbyshopCategory> queryAllByPush() {
        NearbyshopCategoryExample example = new NearbyshopCategoryExample();
        example.or().andPushStatusEqualTo(1)
                .andDeletedEqualTo(false);
        example.setOrderByClause("push_sort asc");
        NearbyshopCategory.Column[] columns = new NearbyshopCategory.Column[]{NearbyshopCategory.Column.id,
                NearbyshopCategory.Column.name,NearbyshopCategory.Column.iconUrl,
                NearbyshopCategory.Column.pushStatus,NearbyshopCategory.Column.pushSort};
        return categoryMapper.selectByExampleSelective(example, columns);
    }

}
