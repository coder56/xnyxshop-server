package com.yangtu.nearbyshop.db.util;

public class CouponUserConstant {
    //可用
    public static final Short STATUS_USABLE = 0;
    //已经使用
    public static final Short STATUS_USED = 1;
    //已经过期
    public static final Short STATUS_EXPIRED = 2;

    public static final Short STATUS_OUT = 3;
}
