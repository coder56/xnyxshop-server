package com.yangtu.nearbyshop.db.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopOrder2;
import com.yangtu.nearbyshop.db.dao.NearbyshopOrderMapper2;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopOrderService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yangtu.nearbyshop.db.vo.CouponUseVo;
import com.yangtu.nearbyshop.db.vo.SaleCountVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author wanglei
 * @since 2019-08-30
 */
@Service
public class NearbyshopOrderServiceImpl extends ServiceImpl<NearbyshopOrderMapper2,
        NearbyshopOrder2> implements INearbyshopOrderService {

    @Autowired
    NearbyshopOrderMapper2 orderMapper2;

    @Override
    public IPage<NearbyshopOrder2> selectByPage(Page<NearbyshopOrder2> page, NearbyshopOrder2 nearbyshopOrder2) {
        return null;
    }

    @Override
    public IPage<SaleCountVo> sumAllSale(Page<SaleCountVo> page,String startTime, String endTime) {
        return orderMapper2.sumAllSale(page,startTime,endTime);
    }

    @Override
    public IPage<SaleCountVo> sumAllSaleByGoods(Page<SaleCountVo> page,String startTime,
                                                String endTime, String goodsSn) {
        return orderMapper2.sumAllSaleByGoods(page,startTime,endTime,goodsSn);
    }

    @Override
    public IPage<SaleCountVo> sumAllSaleByMerc(Page<SaleCountVo> page,String startTime, String endTime, String mercNo) {
        return orderMapper2.sumAllSaleByMerc(page,startTime,endTime,mercNo);
    }

    @Override
    public IPage<SaleCountVo> sumAllSaleByBd(Page<SaleCountVo> page,String startTime, String endTime, String bdNo) {
        return orderMapper2.sumAllSaleByBd(page,startTime,endTime,bdNo);
    }

    @Override
    public String sumAllUser(String mercNo, String startTime, String endTime) {
        return orderMapper2.sumAllUser(mercNo,startTime,endTime);
    }

    @Override
    public IPage<CouponUseVo> getCouponUse(Page<CouponUseVo> page, String startTime, String endTime, Integer couponId, String mercNo) {
        return orderMapper2.getCouponUse(startTime,endTime,couponId,mercNo);
    }


}
