package com.yangtu.nearbyshop.db.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.yangtu.nearbyshop.db.domain.NearbyshopOrder2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 配送表
 * </p>
 *
 * @author wanglei
 * @since 2019-08-28
 */
public class NearbyshopDispatchVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    /**
     * 锁定批次
     */
    private String purchaseNo;

    /**
     * 团长编号
     */
    private String mercNo;

    /**
     * 商品数量
     */
    private Integer goodsNum;

    /**
     * 商品规格
     */
    private String goodsPrice;

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date dispatchTime;

    /**
     * 配送状态 0:配送  1:已配送
     */
    private Integer dispatchStatus;

    /**
     * 采购状态  0:待采购 1：已采购
     */
    private Integer purchaseStatus;

    private String mercName;
    private String mercAddr;
    private String username;
    private String mobile;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date startTime;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date endTime;

    /**
     * 逻辑删除
     */
    private Integer deleted;

    private String bdName;
    private String bdPhone;

    private List<NearbyshopOrder2> orderList = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getPurchaseNo() {
        return purchaseNo;
    }

    public void setPurchaseNo(String purchaseNo) {
        this.purchaseNo = purchaseNo;
    }
    public String getMercNo() {
        return mercNo;
    }

    public void setMercNo(String mercNo) {
        this.mercNo = mercNo;
    }
    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }
    public String getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(String goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Integer getDispatchStatus() {
        return dispatchStatus;
    }

    public void setDispatchStatus(Integer dispatchStatus) {
        this.dispatchStatus = dispatchStatus;
    }
    public Integer getPurchaseStatus() {
        return purchaseStatus;
    }

    public void setPurchaseStatus(Integer purchaseStatus) {
        this.purchaseStatus = purchaseStatus;
    }
    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getMercName() {
        return mercName;
    }

    public void setMercName(String mercName) {
        this.mercName = mercName;
    }

    public String getMercAddr() {
        return mercAddr;
    }

    public void setMercAddr(String mercAddr) {
        this.mercAddr = mercAddr;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Date getDispatchTime() {
        return dispatchTime;
    }

    public void setDispatchTime(Date dispatchTime) {
        this.dispatchTime = dispatchTime;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getBdName() {
        return bdName;
    }

    public void setBdName(String bdName) {
        this.bdName = bdName;
    }

    public String getBdPhone() {
        return bdPhone;
    }

    public void setBdPhone(String bdPhone) {
        this.bdPhone = bdPhone;
    }

    public List<NearbyshopOrder2> getOrderList() {
        return orderList;
    }

    public void setOrderList(List<NearbyshopOrder2> orderList) {
        this.orderList = orderList;
    }

    @Override
    public String toString() {
        return "NearbyshopDispatch{" +
        "id=" + id +
        ", purchaseNo=" + purchaseNo +
        ", mercNo=" + mercNo +
        ", goodsNum=" + goodsNum +
        ", goodsPrice=" + goodsPrice +
        ", createTime=" + createTime +
        ", dispatchStatus=" + dispatchStatus +
        ", purchaseStatus=" + purchaseStatus +
        ", deleted=" + deleted +
        "}";
    }
}
