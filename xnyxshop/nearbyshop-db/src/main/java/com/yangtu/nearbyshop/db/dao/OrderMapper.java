package com.yangtu.nearbyshop.db.dao;

import com.yangtu.nearbyshop.db.domain.NearbyshopOrder;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface OrderMapper {
    int updateWithOptimisticLocker(@Param("lastUpdateTime") LocalDateTime lastUpdateTime, @Param("order") NearbyshopOrder order);

    void updateBatch(@Param("list") List<NearbyshopOrder> list);
}
