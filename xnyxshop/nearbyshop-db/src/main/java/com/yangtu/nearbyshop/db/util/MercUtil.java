package com.yangtu.nearbyshop.db.util;

import com.yangtu.nearbyshop.db.domain.NearbyshopMerc;
import com.yangtu.nearbyshop.db.domain.NearbyshopOrder;

import java.util.ArrayList;
import java.util.List;

/*货
 * 用户状态：
 * 101 订单生成，未支付；102，下单未支付用户取消；103，下单未支付超期系统自动取消
 * 201 支付完成，商家未发货；202，订单生产，已付款未发货，用户申请退款；203，管理员执行退款操作，确认退款成功；
 * 301 商家发货，用户未确认；
 * 401 用户确认收货，订单结束； 402 用户没有确认收货，但是快递反馈已收获后，超过一定时间，系统自动确认收货，订单结束。
 *
 * 当101用户未付款时，此时用户可以进行的操作是取消或者付款
 * 当201支付完成而商家未发货时，此时用户可以退款
 * 当301商家已发货时，此时用户可以有确认收货
 * 当401用户确认收货以后，此时用户可以进行的操作是退货、删除、去评价或者再次购买
 * 当402系统自动确认收货以后，此时用户可以删除、去评价、或者再次购买
 */
public class MercUtil {

    public static final Short STATUS_CREATE = 101;
    public static final Short STATUS_NORMAL = 201;
    public static final Short STATUS_FROZEN = 301;
    public static final Short STATUS_DELETE = 401;
    public static final Short STATUS_CLOSE = 501;


    public static String orderStatusText(NearbyshopMerc merc) {
        int status = merc.getStatus().intValue();

        if (status == 101) {
            return "已创建";
        }

        if (status == 201) {
            return "正常使用";
        }

        if (status == 301) {
            return "已冻结";
        }

        if (status == 401) {
            return "已删除";
        }

        if (status == 501) {
            return "已停业";
        }

        throw new IllegalStateException("orderStatus不支持");
    }


    public static boolean isCreateStatus(NearbyshopOrder nearbyshopOrder) {
        return MercUtil.STATUS_CREATE == nearbyshopOrder.getOrderStatus().shortValue();
    }

    public static boolean isNormalStatus(NearbyshopOrder nearbyshopOrder) {
        return MercUtil.STATUS_NORMAL == nearbyshopOrder.getOrderStatus().shortValue();
    }

    public static boolean isFrozenStatus(NearbyshopOrder nearbyshopOrder) {
        return MercUtil.STATUS_FROZEN == nearbyshopOrder.getOrderStatus().shortValue();
    }

    public static boolean isDeleteStatus(NearbyshopOrder nearbyshopOrder) {
        return MercUtil.STATUS_DELETE == nearbyshopOrder.getOrderStatus().shortValue();
    }

    public static boolean isCloseStatus(NearbyshopOrder nearbyshopOrder) {
        return MercUtil.STATUS_CLOSE == nearbyshopOrder.getOrderStatus().shortValue();
    }

}
