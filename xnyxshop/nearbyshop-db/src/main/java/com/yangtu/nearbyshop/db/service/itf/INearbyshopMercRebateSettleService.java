package com.yangtu.nearbyshop.db.service.itf;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercRebate;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercRebateSettle;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 门店返利日统计表 服务类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-20
 */
public interface INearbyshopMercRebateSettleService extends IService<NearbyshopMercRebateSettle> {

    IPage<NearbyshopMercRebateSettle> getDailyRebateSettle(Page<NearbyshopMercRebateSettle> page,
                                               String mercName, String mercNo, String startTime,
                                               String endTime);

    NearbyshopMercRebateSettle getWeekRebateSettle(String mercName, String mercNo, String startTime,
                                                           String endTime);
}
