package com.yangtu.nearbyshop.db.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopMerc2;
import com.yangtu.nearbyshop.db.dao.NearbyshopMercMapper2;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopMercService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 门店表 服务实现类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-19
 */
@Service
public class NearbyshopMercServiceImpl extends ServiceImpl<NearbyshopMercMapper2,
        NearbyshopMerc2> implements INearbyshopMercService {

    @Autowired
    NearbyshopMercMapper2 mercMapper2;

    @Override
    public IPage<NearbyshopMerc2> getSonList(Page<NearbyshopMerc2> page,
                                             String mercNo, Integer level) {

        if(level.equals(1)){
            return mercMapper2.getSonListLevelOne(page,mercNo);
        }else if(level.equals(2)){
            return mercMapper2.getSonListLevelTwo(page,mercNo);
        }
        return null;
    }
}
