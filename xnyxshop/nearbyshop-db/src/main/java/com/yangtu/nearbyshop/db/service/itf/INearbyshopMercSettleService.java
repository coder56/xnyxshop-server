package com.yangtu.nearbyshop.db.service.itf;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercSettle;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 门店销售统计表 服务类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-19
 */
public interface INearbyshopMercSettleService extends IService<NearbyshopMercSettle> {

    List<NearbyshopMercSettle> getMercSettleByWeek(String startTime, String endTime);

    IPage<NearbyshopMercSettle> getMercSettleList(Page<NearbyshopMercSettle> page, String mercNo);
}
