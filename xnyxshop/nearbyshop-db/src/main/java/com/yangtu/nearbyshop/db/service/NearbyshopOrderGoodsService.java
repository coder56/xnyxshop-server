package com.yangtu.nearbyshop.db.service;

import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopOrderGoodsMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopOrderGoods;
import com.yangtu.nearbyshop.db.domain.NearbyshopOrderGoodsExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Service
public class NearbyshopOrderGoodsService {
    @Resource
    private NearbyshopOrderGoodsMapper orderGoodsMapper;

    public int add(NearbyshopOrderGoods orderGoods) {
        orderGoods.setAddTime(LocalDateTime.now());
        orderGoods.setUpdateTime(LocalDateTime.now());
        return orderGoodsMapper.insertSelective(orderGoods);
    }

    public List<NearbyshopOrderGoods> queryByOid(Integer orderId) {
        NearbyshopOrderGoodsExample example = new NearbyshopOrderGoodsExample();
        example.or().andOrderIdEqualTo(orderId).andDeletedEqualTo(false);
        return orderGoodsMapper.selectByExample(example);
    }

    public List<NearbyshopOrderGoods> findByOidAndGid(Integer orderId, Integer goodsId) {
        NearbyshopOrderGoodsExample example = new NearbyshopOrderGoodsExample();
        example.or().andOrderIdEqualTo(orderId).andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);
        return orderGoodsMapper.selectByExample(example);
    }

    public NearbyshopOrderGoods findById(Integer id) {
        return orderGoodsMapper.selectByPrimaryKey(id);
    }

    public void updateById(NearbyshopOrderGoods orderGoods) {
        orderGoods.setUpdateTime(LocalDateTime.now());
        orderGoodsMapper.updateByPrimaryKeySelective(orderGoods);
    }

    public Short getComments(Integer orderId) {
        NearbyshopOrderGoodsExample example = new NearbyshopOrderGoodsExample();
        example.or().andOrderIdEqualTo(orderId).andDeletedEqualTo(false);
        long count = orderGoodsMapper.countByExample(example);
        return (short) count;
    }

    public boolean checkExist(Integer goodsId) {
        NearbyshopOrderGoodsExample example = new NearbyshopOrderGoodsExample();
        example.or().andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);
        return orderGoodsMapper.countByExample(example) != 0;
    }

    public int countByGoodsId(Integer userId,Integer goodsId) {
        Integer count = orderGoodsMapper.countGoodsByUser(userId,goodsId);
        if(null == count){
            count = 0;
        }
        return count;
    }

    /**
     * 按照门店，产品，时间获取统计后数据
     * @author: ZhangJinChang
     * @Descripition:
     * @param mercNo
     * @param goodsId
     * @param startTm
     * @param endTm
     * @param qryType 0 按商品查  1 按照门店查   2按照用户查
     * @Date: 2019/4/25 19:58
     */
    public List<Map<String, String>> getGoodsByCond(String mercNo, String goodsId, String ordStstue,
                                                    String startTm, String endTm, Integer page,
                                                    Integer size, int qryType) {

        PageHelper.startPage(page, size);
        if (qryType == 0) {
            return  orderGoodsMapper.selectGoodsByCond(mercNo, goodsId, ordStstue,  startTm, endTm,"0");
        }

        if (qryType == 1) {
            return  orderGoodsMapper.selectGoodsMercByCond(mercNo, goodsId, ordStstue,  startTm, endTm);
        }

        if (qryType == 2) {
            return  orderGoodsMapper.selectGoodsUserByCond(mercNo, goodsId, ordStstue,  startTm, endTm);
        }

        return null;
    }

    public long getSaleNum(Integer goodsId) {
        NearbyshopOrderGoodsExample example = new NearbyshopOrderGoodsExample();
        example.or().andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);
        return orderGoodsMapper.countByExample(example);
    }

    public long getMercSaleNum(String mercNo) {
        NearbyshopOrderGoodsExample example = new NearbyshopOrderGoodsExample();
        example.or().andMercNoEqualTo(mercNo).andDeletedEqualTo(false);
        return orderGoodsMapper.countByExample(example);
    }

    public List<NearbyshopOrderGoods> queryByGid(Integer goodsId, Integer page, Integer limit, String sort, String order) {
        NearbyshopOrderGoodsExample example = new NearbyshopOrderGoodsExample();
        example.or().andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);
        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, limit);

        return orderGoodsMapper.selectByExample(example);
    }

    public List<Map<String, Object>> selectMercOrderByCond(String ordStstue, String startTm, String endTm) {
        return orderGoodsMapper.selectMercOrderByCond(ordStstue,startTm,endTm);
    }

}
