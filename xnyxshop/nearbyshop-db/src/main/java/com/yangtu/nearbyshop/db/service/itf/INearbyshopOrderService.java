package com.yangtu.nearbyshop.db.service.itf;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopOrder2;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yangtu.nearbyshop.db.domain.NearbyshopPurchase;
import com.yangtu.nearbyshop.db.vo.CouponUseVo;
import com.yangtu.nearbyshop.db.vo.SaleCountVo;

import java.math.BigDecimal;
import java.util.List;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author wanglei
 * @since 2019-08-30
 */
public interface INearbyshopOrderService extends IService<NearbyshopOrder2> {

    public IPage<NearbyshopOrder2> selectByPage(Page<NearbyshopOrder2> page, NearbyshopOrder2 nearbyshopOrder2);

    IPage<SaleCountVo>  sumAllSale(Page<SaleCountVo> page,String startTime, String endTime);

    IPage<SaleCountVo> sumAllSaleByGoods(Page<SaleCountVo> page,String startTime, String endTime,String goodsSn);

    IPage<SaleCountVo> sumAllSaleByMerc(Page<SaleCountVo> page,String startTime, String endTime,String mercNo);

    IPage<SaleCountVo> sumAllSaleByBd(Page<SaleCountVo> page,String startTime, String endTime,String bd);

    String sumAllUser(String mercNo,String startTime, String endTime);

    IPage<CouponUseVo> getCouponUse(Page<CouponUseVo> page,String startTime,
                                    String endTime,Integer couponId,String mercNo);
}
