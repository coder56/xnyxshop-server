package com.yangtu.nearbyshop.db.dao;

import com.yangtu.nearbyshop.db.domain.NearbyshopBdSettle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * BD门店销售统计表 Mapper 接口
 * </p>
 *
 * @author wanglei
 * @since 2019-09-18
 */
public interface NearbyshopBdSettleMapper extends BaseMapper<NearbyshopBdSettle> {

}
