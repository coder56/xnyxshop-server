package com.yangtu.nearbyshop.db.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import java.lang.reflect.Array;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * BD表
 * </p>
 *
 * @author wanglei
 * @since 2019-09-15
 */
public class NearbyshopBd implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * BD名称
     */
    private String bdName;

    /**
     * BD编号
     */
    private String bdNo;

    /**
     * BD手机号
     */
    private String bdPhone;

    /**
     * BD账号
     */
    private String bdAccount;

    /**
     * 创建时间
     */
    private Date addTime;

    /**
     * 逻辑删除
     */
    private Integer deleted;

    @TableField(exist = false)
    private Integer mercNum;

    @TableField(exist = false)
    private List<NearbyshopBdSettle> settleList = new ArrayList<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getBdName() {
        return bdName;
    }

    public void setBdName(String bdName) {
        this.bdName = bdName;
    }
    public String getBdNo() {
        return bdNo;
    }

    public void setBdNo(String bdNo) {
        this.bdNo = bdNo;
    }
    public String getBdPhone() {
        return bdPhone;
    }

    public void setBdPhone(String bdPhone) {
        this.bdPhone = bdPhone;
    }
    public String getBdAccount() {
        return bdAccount;
    }

    public void setBdAccount(String bdAccount) {
        this.bdAccount = bdAccount;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Integer getMercNum() {
        return mercNum;
    }

    public void setMercNum(Integer mercNum) {
        this.mercNum = mercNum;
    }

    public List<NearbyshopBdSettle> getSettleList() {
        return settleList;
    }

    public void setSettleList(List<NearbyshopBdSettle> settleList) {
        this.settleList = settleList;
    }

    @Override
    public String toString() {
        return "NearbyshopBd{" +
        "id=" + id +
        ", bdName=" + bdName +
        ", bdNo=" + bdNo +
        ", bdPhone=" + bdPhone +
        ", bdAccount=" + bdAccount +
        ", addTime=" + addTime +
        ", deleted=" + deleted +
        "}";
    }
}
