package com.yangtu.nearbyshop.db.service;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.yangtu.nearbyshop.db.dao.NearbyshopDispatchMapper;
import com.yangtu.nearbyshop.db.dao.NearbyshopOrderMapper2;
import com.yangtu.nearbyshop.db.dao.NearbyshopPurchaseMapper;
import com.yangtu.nearbyshop.db.domain.*;
import com.yangtu.nearbyshop.db.dao.NearbyshopLockMapper;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopLockService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 锁定记录表 服务实现类
 * </p>
 *
 * @author wanglei
 * @since 2019-08-30
 */
@Service
public class NearbyshopLockServiceImpl extends ServiceImpl<NearbyshopLockMapper,
        NearbyshopLock> implements INearbyshopLockService {

    @Autowired
    NearbyshopLockMapper lockMapper;

    @Autowired
    NearbyshopPurchaseMapper purchaseMapper;

    @Autowired
    NearbyshopDispatchMapper dispatchMapper;

    @Autowired
    NearbyshopOrderMapper2 orderMapper2;

    @Override
    public void updatePurchaseStatus(String purchaseNo, Integer purchaseStatus) {
        NearbyshopLock lock = new NearbyshopLock();
        lock.setPurchaseStatus(purchaseStatus);
        lockMapper.update(lock,new UpdateWrapper<NearbyshopLock>()
            .eq("purchase_no",purchaseNo));

        NearbyshopPurchase purchase = new NearbyshopPurchase();
        purchase.setPurchaseStatus(purchaseStatus);
        purchaseMapper.update(purchase,new UpdateWrapper<NearbyshopPurchase>()
            .eq("purchase_no",purchaseNo));

        NearbyshopDispatch dispatch = new NearbyshopDispatch();
        dispatch.setPurchaseStatus(purchaseStatus);
        dispatchMapper.update(dispatch,new UpdateWrapper<NearbyshopDispatch>()
                .eq("purchase_no",purchaseNo));

        NearbyshopOrder2 order = new NearbyshopOrder2();
        order.setPurchaseStatus(purchaseStatus);
        orderMapper2.update(order,new UpdateWrapper<NearbyshopOrder2>()
                .eq("purchase_no",purchaseNo));
    }
}
