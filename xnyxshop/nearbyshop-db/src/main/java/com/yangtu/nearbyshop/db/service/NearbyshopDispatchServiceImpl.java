package com.yangtu.nearbyshop.db.service;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.dao.NearbyshopOrderMapper2;
import com.yangtu.nearbyshop.db.domain.NearbyshopDispatch;
import com.yangtu.nearbyshop.db.dao.NearbyshopDispatchMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopOrder2;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopDispatchService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yangtu.nearbyshop.db.util.OrderUtil;
import com.yangtu.nearbyshop.db.vo.NearbyshopDispatchVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 配送表 服务实现类
 * </p>
 *
 * @author wanglei
 * @since 2019-08-28
 */
@Service
public class NearbyshopDispatchServiceImpl extends ServiceImpl<NearbyshopDispatchMapper, NearbyshopDispatch> implements INearbyshopDispatchService {

    @Autowired
    NearbyshopDispatchMapper dispatchMapper;

    @Autowired
    NearbyshopOrderMapper2 orderMapper2;



    @Override
    public IPage<NearbyshopDispatchVo> getDispatchList(Page<NearbyshopDispatchVo> pageVo,
                                                       String startTime, String endTime,
                                                       Integer dispatchStatus) {
        return dispatchMapper.getDispatchList(pageVo,startTime,endTime,dispatchStatus);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateDispatchDone(Integer id) {
        NearbyshopDispatch dispatch = new NearbyshopDispatch();
        dispatch.setId(id);
        dispatch.setDispatchStatus(1);
        dispatch.setDispatchTime(new Date());
        dispatchMapper.updateById(dispatch);

        NearbyshopDispatch nearbyshopDispatch = dispatchMapper.selectById(id);

        NearbyshopOrder2 order = new NearbyshopOrder2();
        order.setDispatchStatus(1);
        order.setOrderStatus(OrderUtil.STATUS_SHIP.intValue());
        order.setUpdateTime(new Date());
        orderMapper2.update(order,new UpdateWrapper<NearbyshopOrder2>()
            .eq("merc_no",nearbyshopDispatch.getMercNo())
            .eq("purchase_no",nearbyshopDispatch.getPurchaseNo()));


    }
}
