package com.yangtu.nearbyshop.db.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopBd;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopBdSettle;
import com.yangtu.nearbyshop.db.domain.NearbyshopMerc;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * BD表 Mapper 接口
 * </p>
 *
 * @author wanglei
 * @since 2019-09-15
 */
public interface NearbyshopBdMapper extends BaseMapper<NearbyshopBd> {

    IPage<NearbyshopBd> getBdList(Page<NearbyshopBd> page, @Param("bdName") String bdName);

    IPage<NearbyshopMerc> getMercListByBd(Page<NearbyshopMerc> page,@Param("bdNo") String bdNo);

    List<NearbyshopBdSettle> getBdSettleByWeek(@Param("startTime") String startTime,
                                         @Param("endTime") String endTime);

    List<NearbyshopBdSettle> getBdSettleList(@Param("bdNo") String bdNo);
}
