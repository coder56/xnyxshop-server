package com.yangtu.nearbyshop.db.service.itf;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yangtu.nearbyshop.db.domain.NearbyshopMerc2;

/**
 * <p>
 * 门店表 服务类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-19
 */
public interface INearbyshopMercService extends IService<NearbyshopMerc2> {

    IPage<NearbyshopMerc2> getSonList(Page<NearbyshopMerc2> page, String mercNo,
                                      Integer level);
}
