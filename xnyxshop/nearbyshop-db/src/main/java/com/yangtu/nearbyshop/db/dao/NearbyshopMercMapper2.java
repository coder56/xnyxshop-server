package com.yangtu.nearbyshop.db.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopMerc2;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 门店表 Mapper 接口
 * </p>
 *
 * @author wanglei
 * @since 2019-09-19
 */
public interface NearbyshopMercMapper2 extends BaseMapper<NearbyshopMerc2> {

    IPage<NearbyshopMerc2> getSonListLevelOne(Page<NearbyshopMerc2> page,
                                      @Param("mercNo") String mercNo);

    IPage<NearbyshopMerc2> getSonListLevelTwo(Page<NearbyshopMerc2> page,
                                      @Param("mercNo") String mercNo);
}
