package com.yangtu.nearbyshop.db.dao;

import com.yangtu.nearbyshop.db.domain.NearbyshopOrderRefund;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yangtu.nearbyshop.db.vo.OrderRefundVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 退款表 Mapper 接口
 * </p>
 *
 * @author wanglei
 * @since 2019-09-14
 */
public interface NearbyshopOrderRefundMapper extends BaseMapper<NearbyshopOrderRefund> {

    OrderRefundVo countRefund(@Param("userId") Integer userId,
                              @Param("orderStatus") Integer orderStatus,
                              @Param("orderId") Integer orderId,
                              @Param("refundBy") String refundBy,
                              @Param("startTime") String startTime,
                              @Param("endTime") String endTime);

}
