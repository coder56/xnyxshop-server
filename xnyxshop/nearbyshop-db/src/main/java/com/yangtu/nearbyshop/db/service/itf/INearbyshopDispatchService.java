package com.yangtu.nearbyshop.db.service.itf;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopDispatch;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yangtu.nearbyshop.db.vo.NearbyshopDispatchVo;
import com.yangtu.nearbyshop.db.vo.NearbyshopPurchaseVo;

import java.util.List;

/**
 * <p>
 * 配送表 服务类
 * </p>
 *
 * @author wanglei
 * @since 2019-08-28
 */
public interface INearbyshopDispatchService extends IService<NearbyshopDispatch> {

    IPage<NearbyshopDispatchVo> getDispatchList(Page<NearbyshopDispatchVo> page,
                                                String startTime, String endTime,Integer dispatchStatus);


    void updateDispatchDone(Integer id);
}
