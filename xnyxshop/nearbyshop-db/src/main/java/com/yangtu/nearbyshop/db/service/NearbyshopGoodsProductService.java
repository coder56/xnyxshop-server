package com.yangtu.nearbyshop.db.service;

import com.yangtu.nearbyshop.db.dao.GoodsProductMapper;
import com.yangtu.nearbyshop.db.dao.NearbyshopGoodsProductMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopGoodsProduct;
import com.yangtu.nearbyshop.db.domain.NearbyshopGoodsProductExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class NearbyshopGoodsProductService {
    @Resource
    private NearbyshopGoodsProductMapper nearbyshopGoodsProductMapper;
    @Resource
    private GoodsProductMapper goodsProductMapper;

    public List<NearbyshopGoodsProduct> queryByGid(Integer gid) {
        NearbyshopGoodsProductExample example = new NearbyshopGoodsProductExample();
        example.or().andGoodsIdEqualTo(gid).andDeletedEqualTo(false);
        return nearbyshopGoodsProductMapper.selectByExample(example);
    }

    public NearbyshopGoodsProduct findById(Integer id) {
        return nearbyshopGoodsProductMapper.selectByPrimaryKey(id);
    }

    public void deleteById(Integer id) {
        nearbyshopGoodsProductMapper.logicalDeleteByPrimaryKey(id);
    }

    public void add(NearbyshopGoodsProduct goodsProduct) {
        goodsProduct.setAddTime(LocalDateTime.now());
        goodsProduct.setUpdateTime(LocalDateTime.now());
        nearbyshopGoodsProductMapper.insertSelective(goodsProduct);
    }

    public int count() {
        NearbyshopGoodsProductExample example = new NearbyshopGoodsProductExample();
        example.or().andDeletedEqualTo(false);
        return (int) nearbyshopGoodsProductMapper.countByExample(example);
    }

    public void deleteByGid(Integer gid) {
        NearbyshopGoodsProductExample example = new NearbyshopGoodsProductExample();
        example.or().andGoodsIdEqualTo(gid);
        nearbyshopGoodsProductMapper.logicalDeleteByExample(example);
    }

    public int addStock(Integer id, Short num){
        return goodsProductMapper.addStock(id, num);
    }

    public int reduceStock(Integer id, Short num){
        return goodsProductMapper.reduceStock(id, num);
    }

    public void update(NearbyshopGoodsProduct goodsProduct) {
        goodsProduct.setAddTime(LocalDateTime.now());
        goodsProduct.setUpdateTime(LocalDateTime.now());
        nearbyshopGoodsProductMapper.updateByPrimaryKey(goodsProduct);
    }
}