package com.yangtu.nearbyshop.db.dao;

import com.yangtu.nearbyshop.db.domain.NearbyshopSetting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 设置表 Mapper 接口
 * </p>
 *
 * @author wanglei
 * @since 2019-10-07
 */
public interface NearbyshopSettingMapper extends BaseMapper<NearbyshopSetting> {

}
