package com.yangtu.nearbyshop.db.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 门店销售统计表
 * </p>
 *
 * @author wanglei
 * @since 2019-09-19
 */
public class NearbyshopMercSettle implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 门店编号
     */
    private String mercNo;

    /**
     * 时间区间
     */
    private String timeDur;

    /**
     * 销售金额
     */
    private BigDecimal amount;

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date addTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMercNo() {
        return mercNo;
    }

    public void setMercNo(String mercNo) {
        this.mercNo = mercNo;
    }

    public String getTimeDur() {
        return timeDur;
    }

    public void setTimeDur(String timeDur) {
        this.timeDur = timeDur;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    @Override
    public String toString() {
        return "NearbyshopMercSettle{" +
        "id=" + id +
        ", mercNo=" + mercNo +
        ", timeDur=" + timeDur +
        ", amount=" + amount +
        ", addTime=" + addTime +
        "}";
    }
}
