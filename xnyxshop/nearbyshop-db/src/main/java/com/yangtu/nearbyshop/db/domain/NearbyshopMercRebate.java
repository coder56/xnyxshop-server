package com.yangtu.nearbyshop.db.domain;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 门店返利统计表
 * </p>
 *
 * @author wanglei
 * @since 2019-09-19
 */
public class NearbyshopMercRebate implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 门店编号
     */
    private String mercNo;

    /**
     * 返利类型
     */
    private String rebateType;

    /**
     * 返利描述
     */
    private String rebateDesc;

    /**
     * 返利的订单ID
     */
    private Integer rebateOrderId;

    /**
     * 返利金额
     */
    private BigDecimal amount;

    /**
     * 返利的门店编号
     */
    private String fromMercNoOne;

    private String fromMercNoTwo;

    private String items;

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date addTime;

    @TableField(exist = false)
    private String mercName;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMercNo() {
        return mercNo;
    }

    public void setMercNo(String mercNo) {
        this.mercNo = mercNo;
    }

    public String getRebateType() {
        return rebateType;
    }

    public void setRebateType(String rebateType) {
        this.rebateType = rebateType;
    }

    public String getRebateDesc() {
        return rebateDesc;
    }

    public void setRebateDesc(String rebateDesc) {
        this.rebateDesc = rebateDesc;
    }

    public Integer getRebateOrderId() {
        return rebateOrderId;
    }

    public void setRebateOrderId(Integer rebateOrderId) {
        this.rebateOrderId = rebateOrderId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public String getFromMercNoOne() {
        return fromMercNoOne;
    }

    public void setFromMercNoOne(String fromMercNoOne) {
        this.fromMercNoOne = fromMercNoOne;
    }

    public String getFromMercNoTwo() {
        return fromMercNoTwo;
    }

    public void setFromMercNoTwo(String fromMercNoTwo) {
        this.fromMercNoTwo = fromMercNoTwo;
    }

    public String getMercName() {
        return mercName;
    }

    public void setMercName(String mercName) {
        this.mercName = mercName;
    }

    public String getItems() {
        return items;
    }

    public void setItems(String items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "NearbyshopMercRebate{" +
        "id=" + id +
        ", mercNo=" + mercNo +
        ", rebateType=" + rebateType +
        ", rebateDesc=" + rebateDesc +
        ", rebateOrderId=" + rebateOrderId +
        ", amount=" + amount +
        ", addTime=" + addTime +
        "}";
    }
}
