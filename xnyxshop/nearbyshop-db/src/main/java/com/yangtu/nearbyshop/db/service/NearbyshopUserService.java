package com.yangtu.nearbyshop.db.service;

import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopUserMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopUser;
import com.yangtu.nearbyshop.db.domain.NearbyshopUserExample;
import com.yangtu.nearbyshop.db.domain.UserVo;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class NearbyshopUserService {
    @Resource
    private NearbyshopUserMapper userMapper;

    public NearbyshopUser findById(Integer userId) {
        return userMapper.selectByPrimaryKey(userId);
    }

    public UserVo findUserVoById(Integer userId) {
        NearbyshopUser user = findById(userId);
        UserVo userVo = new UserVo();
        userVo.setNickname(user.getNickname());
        userVo.setAvatar(user.getAvatar());
        return userVo;
    }

    public NearbyshopUser queryByOid(String openId) {
        NearbyshopUserExample example = new NearbyshopUserExample();
        example.or().andWeixinOpenidEqualTo(openId).andDeletedEqualTo(false);
        return userMapper.selectOneByExample(example);
    }

    public void add(NearbyshopUser user) {
        user.setAddTime(LocalDateTime.now());
        user.setUpdateTime(LocalDateTime.now());
        userMapper.insertSelective(user);
    }

    public int updateById(NearbyshopUser user) {
        user.setUpdateTime(LocalDateTime.now());
        return userMapper.updateByPrimaryKeySelective(user);
    }

    public List<NearbyshopUser> querySelective(String username, String mobile, Integer page, Integer size, String sort, String order) {
        NearbyshopUserExample example = new NearbyshopUserExample();
        NearbyshopUserExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(username)) {
            criteria.andUsernameLike("%" + username + "%");
        }
        if (!StringUtils.isEmpty(mobile)) {
            criteria.andMobileEqualTo(mobile);
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return userMapper.selectByExample(example);
    }

    public int count() {
        NearbyshopUserExample example = new NearbyshopUserExample();
        example.or().andDeletedEqualTo(false);

        return (int) userMapper.countByExample(example);
    }

    public List<NearbyshopUser> queryByUsername(String username) {
        NearbyshopUserExample example = new NearbyshopUserExample();
        example.or().andUsernameEqualTo(username).andDeletedEqualTo(false);
        return userMapper.selectByExample(example);
    }

    public boolean checkByUsername(String username) {
        NearbyshopUserExample example = new NearbyshopUserExample();
        example.or().andUsernameEqualTo(username).andDeletedEqualTo(false);
        return userMapper.countByExample(example) != 0;
    }

    public List<NearbyshopUser> queryByMobile(String mobile) {
        NearbyshopUserExample example = new NearbyshopUserExample();
        example.or().andMobileEqualTo(mobile).andDeletedEqualTo(false);
        return userMapper.selectByExample(example);
    }

    public List<NearbyshopUser> queryByOpenid(String openid) {
        NearbyshopUserExample example = new NearbyshopUserExample();
        example.or().andWeixinOpenidEqualTo(openid).andDeletedEqualTo(false);
        return userMapper.selectByExample(example);
    }

    public void deleteById(Integer id) {
        userMapper.logicalDeleteByPrimaryKey(id);
    }

    public List<NearbyshopUser> queryByMercNo(String mercNo, String mobile, Integer page, Integer size) {

        NearbyshopUserExample example = new NearbyshopUserExample();
        NearbyshopUserExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(mobile)) {
            criteria.andMobileEqualTo(mobile);
        }
        criteria.andDeletedEqualTo(false);
        criteria.andMercNoEqualTo(mercNo);

        example.setOrderByClause("add_time" + " " + "desc");

        PageHelper.startPage(page, size);
        return userMapper.selectByExample(example);
    }

    public long getFansNum(String mercNo) {
        NearbyshopUserExample example = new NearbyshopUserExample();
        example.or().andMercNoEqualTo(mercNo).andDeletedEqualTo(false);
        return userMapper.countByExample(example);
    }
}


