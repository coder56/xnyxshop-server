package com.yangtu.nearbyshop.db.service;

import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopRoleMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopRole;
import com.yangtu.nearbyshop.db.domain.NearbyshopRoleExample;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class NearbyshopRoleService {
    @Resource
    private NearbyshopRoleMapper roleMapper;


    public Set<String> queryByIds(Integer[] roleIds) {
        Set<String> roles = new HashSet<String>();
        if(roleIds.length == 0){
            return roles;
        }

        NearbyshopRoleExample example = new NearbyshopRoleExample();
        example.or().andIdIn(Arrays.asList(roleIds)).andEnabledEqualTo(true).andDeletedEqualTo(false);
        List<NearbyshopRole> roleList = roleMapper.selectByExample(example);

        for(NearbyshopRole role : roleList){
            roles.add(role.getName());
        }

        return roles;

    }

    public List<NearbyshopRole> querySelective(String roleName, Integer page, Integer size, String sort, String order) {
        NearbyshopRoleExample example = new NearbyshopRoleExample();
        NearbyshopRoleExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(roleName)) {
            criteria.andNameEqualTo("%" + roleName + "%");
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return roleMapper.selectByExample(example);
    }

    public NearbyshopRole findById(Integer id) {
        return roleMapper.selectByPrimaryKey(id);
    }

    public void add(NearbyshopRole role) {
        role.setAddTime(LocalDateTime.now());
        role.setUpdateTime(LocalDateTime.now());
        roleMapper.insertSelective(role);
    }

    public void deleteById(Integer id) {
        roleMapper.logicalDeleteByPrimaryKey(id);
    }

    public void updateById(NearbyshopRole role) {
        role.setUpdateTime(LocalDateTime.now());
        roleMapper.updateByPrimaryKeySelective(role);
    }

    public boolean checkExist(String name) {
        NearbyshopRoleExample example = new NearbyshopRoleExample();
        example.or().andNameEqualTo(name).andDeletedEqualTo(false);
        return roleMapper.countByExample(example) != 0;
    }

    public List<NearbyshopRole> queryAll() {
        NearbyshopRoleExample example = new NearbyshopRoleExample();
        example.or().andDeletedEqualTo(false);
        return roleMapper.selectByExample(example);
    }
}
