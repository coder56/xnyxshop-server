package com.yangtu.nearbyshop.db.service;

import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopCartMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopCart;
import com.yangtu.nearbyshop.db.domain.NearbyshopCartExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class NearbyshopCartService {
    @Resource
    private NearbyshopCartMapper cartMapper;

    public NearbyshopCart queryExist(Integer goodsId, Integer productId, Integer userId) {
        NearbyshopCartExample example = new NearbyshopCartExample();
        example.or().andGoodsIdEqualTo(goodsId).andProductIdEqualTo(productId).andUserIdEqualTo(userId).andDeletedEqualTo(false);
        return cartMapper.selectOneByExample(example);
    }

    public void add(NearbyshopCart cart) {
        cart.setAddTime(LocalDateTime.now());
        cart.setUpdateTime(LocalDateTime.now());
        cartMapper.insertSelective(cart);
    }

    public int updateById(NearbyshopCart cart) {
        cart.setUpdateTime(LocalDateTime.now());
        return cartMapper.updateByPrimaryKeySelective(cart);
    }

    public List<NearbyshopCart> queryByUid(int userId) {
        NearbyshopCartExample example = new NearbyshopCartExample();
        example.or().andUserIdEqualTo(userId).andDeletedEqualTo(false);
        return cartMapper.selectByExample(example);
    }


    public List<NearbyshopCart> queryByUidAndChecked(Integer userId) {
        NearbyshopCartExample example = new NearbyshopCartExample();
        example.or().andUserIdEqualTo(userId).andCheckedEqualTo(true).andDeletedEqualTo(false);
        return cartMapper.selectByExample(example);
    }

    public int delete(List<Integer> productIdList, int userId) {
        NearbyshopCartExample example = new NearbyshopCartExample();
        example.or().andUserIdEqualTo(userId).andProductIdIn(productIdList);
        return cartMapper.logicalDeleteByExample(example);
    }

    public NearbyshopCart findById(Integer id) {
        return cartMapper.selectByPrimaryKey(id);
    }

    public int updateCheck(Integer userId, List<Integer> idsList, Boolean checked) {
        NearbyshopCartExample example = new NearbyshopCartExample();
        example.or().andUserIdEqualTo(userId).andProductIdIn(idsList).andDeletedEqualTo(false);
        NearbyshopCart cart = new NearbyshopCart();
        cart.setChecked(checked);
        cart.setUpdateTime(LocalDateTime.now());
        return cartMapper.updateByExampleSelective(cart, example);
    }

    public void clearGoods(Integer userId) {
        NearbyshopCartExample example = new NearbyshopCartExample();
        example.or().andUserIdEqualTo(userId).andCheckedEqualTo(true);
        NearbyshopCart cart = new NearbyshopCart();
        cart.setDeleted(true);
        cartMapper.updateByExampleSelective(cart, example);
    }

    public List<NearbyshopCart> querySelective(Integer userId, Integer goodsId, Integer page, Integer limit, String sort, String order) {
        NearbyshopCartExample example = new NearbyshopCartExample();
        NearbyshopCartExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(userId)) {
            criteria.andUserIdEqualTo(userId);
        }
        if (!StringUtils.isEmpty(goodsId)) {
            criteria.andGoodsIdEqualTo(goodsId);
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, limit);
        return cartMapper.selectByExample(example);
    }

    public void deleteById(Integer id) {
        cartMapper.logicalDeleteByPrimaryKey(id);
    }

    public boolean checkExist(Integer goodsId) {
        NearbyshopCartExample example = new NearbyshopCartExample();
        example.or().andGoodsIdEqualTo(goodsId).andCheckedEqualTo(true).andDeletedEqualTo(false);
        return cartMapper.countByExample(example) != 0;
    }

    public boolean deleteCart(Integer goodsId) {
        NearbyshopCartExample example = new NearbyshopCartExample();
        example.or().andGoodsIdEqualTo(goodsId).andCheckedEqualTo(true).andDeletedEqualTo(false);
        return cartMapper.deleteByExample(example) != 0;
    }

    public List<NearbyshopCart> queryByUidGoodIdCount(int userId, Integer goodsId) {
        NearbyshopCartExample example = new NearbyshopCartExample();
        example.or().andUserIdEqualTo(userId).andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);
        return cartMapper.selectByExample(example);
    }
}
