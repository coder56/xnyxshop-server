package com.yangtu.nearbyshop.db.service;

import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopGoodsMapper;
import com.yangtu.nearbyshop.db.dao.NearbyshopGrouponRulesMapper;
import com.yangtu.nearbyshop.db.domain.*;
import com.yangtu.nearbyshop.db.util.CouponConstant;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NearbyshopGrouponRulesService {
    @Resource
    private NearbyshopGrouponRulesMapper mapper;
    @Resource
    private NearbyshopGoodsMapper goodsMapper;
    private NearbyshopGoods.Column[] goodsColumns = new NearbyshopGoods.Column[]{NearbyshopGoods.Column.id, NearbyshopGoods.Column.name, NearbyshopGoods.Column.brief, NearbyshopGoods.Column.picUrl, NearbyshopGoods.Column.counterPrice, NearbyshopGoods.Column.retailPrice};

    public int createRules(NearbyshopGrouponRules rules) {
        rules.setAddTime(LocalDateTime.now());
        rules.setUpdateTime(LocalDateTime.now());
        return mapper.insertSelective(rules);
    }

    /**
     * 根据ID查找对应团购项
     *
     * @param id
     * @return
     */
    public NearbyshopGrouponRules queryById(Integer id) {
        NearbyshopGrouponRulesExample example = new NearbyshopGrouponRulesExample();
        example.or().andIdEqualTo(id).andDeletedEqualTo(false);
        return mapper.selectOneByExample(example);
    }

    /**
     * 查询某个商品关联的团购规则
     *
     * @param goodsId
     * @return
     */
    public List<NearbyshopGrouponRules> queryByGoodsId(Integer goodsId) {
        NearbyshopGrouponRulesExample example = new NearbyshopGrouponRulesExample();
        example.or().andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);
        return mapper.selectByExample(example);
    }

    /**
     * 获取首页团购活动列表
     *
     * @param offset
     * @param limit
     * @return
     */
    public List<Map<String, Object>> queryList(int offset, int limit) {
        return queryList(offset, limit, "add_time", "desc");
    }

    public List<Map<String, Object>> queryList(int offset, int limit, String sort, String order) {
        NearbyshopGrouponRulesExample example = new NearbyshopGrouponRulesExample();
        example.or().andDeletedEqualTo(false);
        example.setOrderByClause(sort + " " + order);
        PageHelper.startPage(offset, limit);
        List<NearbyshopGrouponRules> grouponRules = mapper.selectByExample(example);

        List<Map<String, Object>> grouponList = new ArrayList<>(grouponRules.size());
        for (NearbyshopGrouponRules rule : grouponRules) {
            Integer goodsId = rule.getGoodsId();
            NearbyshopGoods goods = goodsMapper.selectByPrimaryKeySelective(goodsId, goodsColumns);
            if (goods == null)
                continue;

            Map<String, Object> item = new HashMap<>();
            item.put("goods", goods);
            item.put("groupon_price", goods.getRetailPrice().subtract(rule.getDiscount()));
            item.put("groupon_member", rule.getDiscountMember());
            grouponList.add(item);
        }

        return grouponList;
    }

    /**
     * 判断某个团购活动是否已经过期
     *
     * @return
     */
    public boolean isExpired(NearbyshopGrouponRules rules) {
        return (rules == null || rules.getExpireTime().isBefore(LocalDateTime.now()));
    }

    /**
     * 获取团购活动列表
     *
     * @param goodsId
     * @param page
     * @param size
     * @param sort
     * @param order
     * @return
     */
    public List<NearbyshopGrouponRules> querySelective(String goodsId, Integer page, Integer size, String sort, String order) {
        NearbyshopGrouponRulesExample example = new NearbyshopGrouponRulesExample();
        example.setOrderByClause(sort + " " + order);

        NearbyshopGrouponRulesExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(goodsId)) {
            criteria.andGoodsIdEqualTo(Integer.parseInt(goodsId));
        }
        criteria.andDeletedEqualTo(false);

        PageHelper.startPage(page, size);
        return mapper.selectByExample(example);
    }

    public void delete(Integer id) {
        mapper.logicalDeleteByPrimaryKey(id);
    }

    public int updateById(NearbyshopGrouponRules grouponRules) {
        grouponRules.setUpdateTime(LocalDateTime.now());
        return mapper.updateByPrimaryKeySelective(grouponRules);
    }

    /**
     * 查询过期的优惠券:
     * 注意：如果timeType=0, 即基于领取时间有效期的优惠券，则优惠券不会过期
     *
     * @return
     */
    public List<NearbyshopGrouponRules> queryExpired() {
        NearbyshopGrouponRulesExample example = new NearbyshopGrouponRulesExample();
        example.or().andExpireTimeLessThan(LocalDateTime.now())
                .andStatusEqualTo(0)
                .andDeletedEqualTo(false);
        return mapper.selectByExample(example);
    }
}
