package com.yangtu.nearbyshop.db.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 设置表
 * </p>
 *
 * @author wanglei
 * @since 2019-10-07
 */
public class NearbyshopSetting implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 名称
     */
    private String settingKey;

    /**
     * 值
     */
    private String settingValue;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSettingKey() {
        return settingKey;
    }

    public void setSettingKey(String settingKey) {
        this.settingKey = settingKey;
    }

    public String getSettingValue() {
        return settingValue;
    }

    public void setSettingValue(String settingValue) {
        this.settingValue = settingValue;
    }

    @Override
    public String toString() {
        return "NearbyshopSetting{" +
        "id=" + id +
        ", settingKey=" + settingKey +
        ", settingValue=" + settingValue +
        "}";
    }
}
