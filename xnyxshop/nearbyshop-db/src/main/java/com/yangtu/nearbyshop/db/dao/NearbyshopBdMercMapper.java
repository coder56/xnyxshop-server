package com.yangtu.nearbyshop.db.dao;

import com.yangtu.nearbyshop.db.domain.NearbyshopBdMerc;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * BD门店表 Mapper 接口
 * </p>
 *
 * @author wanglei
 * @since 2019-09-15
 */
public interface NearbyshopBdMercMapper extends BaseMapper<NearbyshopBdMerc> {

}
