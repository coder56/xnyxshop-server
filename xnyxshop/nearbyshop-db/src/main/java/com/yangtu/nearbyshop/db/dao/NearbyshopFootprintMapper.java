package com.yangtu.nearbyshop.db.dao;

import com.yangtu.nearbyshop.db.domain.NearbyshopFootprint;
import com.yangtu.nearbyshop.db.domain.NearbyshopFootprintExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NearbyshopFootprintMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     */
    long countByExample(NearbyshopFootprintExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     */
    int deleteByExample(NearbyshopFootprintExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     */
    int insert(NearbyshopFootprint record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     */
    int insertSelective(NearbyshopFootprint record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopFootprint selectOneByExample(NearbyshopFootprintExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopFootprint selectOneByExampleSelective(@Param("example") NearbyshopFootprintExample example, @Param("selective") NearbyshopFootprint.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    List<NearbyshopFootprint> selectByExampleSelective(@Param("example") NearbyshopFootprintExample example, @Param("selective") NearbyshopFootprint.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     */
    List<NearbyshopFootprint> selectByExample(NearbyshopFootprintExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopFootprint selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") NearbyshopFootprint.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     */
    NearbyshopFootprint selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopFootprint selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") NearbyshopFootprint record, @Param("example") NearbyshopFootprintExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") NearbyshopFootprint record, @Param("example") NearbyshopFootprintExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(NearbyshopFootprint record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(NearbyshopFootprint record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int logicalDeleteByExample(@Param("example") NearbyshopFootprintExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_footprint
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int logicalDeleteByPrimaryKey(Integer id);
}