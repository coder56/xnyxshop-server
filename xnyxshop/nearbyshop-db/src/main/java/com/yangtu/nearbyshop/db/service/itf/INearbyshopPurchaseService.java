package com.yangtu.nearbyshop.db.service.itf;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yangtu.nearbyshop.db.domain.NearbyshopPurchase;
import com.yangtu.nearbyshop.db.vo.NearbyshopPurchaseVo;

/**
 * <p>
 * 采购表 服务类
 * </p>
 *
 * @author wanglei
 * @since 2019-08-27
 */
public interface INearbyshopPurchaseService extends IService<NearbyshopPurchase> {

    public IPage<NearbyshopPurchase> selectByPage(Page<NearbyshopPurchase> page, Integer purchaseStatus,String purchaseNo);

    public IPage<NearbyshopPurchaseVo> selectOrderByPage(Page<NearbyshopPurchaseVo> page, Integer purchaseStatus);


    void updatePurchaseStatus(String pno,Integer purchaseStatus);
}
