package com.yangtu.nearbyshop.db.dao;

import com.yangtu.nearbyshop.db.domain.NearbyshopSearchHistory;
import com.yangtu.nearbyshop.db.domain.NearbyshopSearchHistoryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NearbyshopSearchHistoryMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     */
    long countByExample(NearbyshopSearchHistoryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     */
    int deleteByExample(NearbyshopSearchHistoryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     */
    int insert(NearbyshopSearchHistory record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     */
    int insertSelective(NearbyshopSearchHistory record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopSearchHistory selectOneByExample(NearbyshopSearchHistoryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopSearchHistory selectOneByExampleSelective(@Param("example") NearbyshopSearchHistoryExample example, @Param("selective") NearbyshopSearchHistory.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    List<NearbyshopSearchHistory> selectByExampleSelective(@Param("example") NearbyshopSearchHistoryExample example, @Param("selective") NearbyshopSearchHistory.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     */
    List<NearbyshopSearchHistory> selectByExample(NearbyshopSearchHistoryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopSearchHistory selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") NearbyshopSearchHistory.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     */
    NearbyshopSearchHistory selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopSearchHistory selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") NearbyshopSearchHistory record, @Param("example") NearbyshopSearchHistoryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") NearbyshopSearchHistory record, @Param("example") NearbyshopSearchHistoryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(NearbyshopSearchHistory record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(NearbyshopSearchHistory record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int logicalDeleteByExample(@Param("example") NearbyshopSearchHistoryExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_search_history
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int logicalDeleteByPrimaryKey(Integer id);
}