package com.yangtu.nearbyshop.db.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercRebate;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercRebateSettle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 门店返利日统计表 Mapper 接口
 * </p>
 *
 * @author wanglei
 * @since 2019-09-20
 */
public interface NearbyshopMercRebateSettleMapper extends BaseMapper<NearbyshopMercRebateSettle> {

    IPage<NearbyshopMercRebateSettle> getDailyRebateSettle(Page<NearbyshopMercRebateSettle> page,
                                               @Param("mercName") String mercName,
                                               @Param("mercNo") String mercNo,
                                               @Param("startTime") String startTime,
                                               @Param("endTime") String endTime);

    NearbyshopMercRebateSettle getWeekRebateSettle(@Param("mercName") String mercName,
                                                   @Param("mercNo") String mercNo,
                                                   @Param("startTime") String startTime,
                                                   @Param("endTime") String endTime);
}
