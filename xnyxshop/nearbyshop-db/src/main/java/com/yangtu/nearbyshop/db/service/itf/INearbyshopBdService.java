package com.yangtu.nearbyshop.db.service.itf;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopBd;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yangtu.nearbyshop.db.domain.NearbyshopBdSettle;
import com.yangtu.nearbyshop.db.domain.NearbyshopMerc;
import com.yangtu.nearbyshop.db.vo.SaleCountVo;

import java.util.List;

/**
 * <p>
 * BD表 服务类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-15
 */
public interface INearbyshopBdService extends IService<NearbyshopBd> {

    IPage<NearbyshopBd> getBdList(Page<NearbyshopBd> page, String bdName);

    IPage<NearbyshopMerc> getMercListByBd(Page<NearbyshopMerc> page,String bdNo);

    List<NearbyshopBdSettle> getBdSettleByWeek(String startTime,String endTime);

    List<NearbyshopBdSettle> getBdSettleList(String bdNo);
}
