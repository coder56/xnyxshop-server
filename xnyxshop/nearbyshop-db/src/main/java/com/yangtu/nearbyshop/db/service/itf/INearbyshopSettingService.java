package com.yangtu.nearbyshop.db.service.itf;

import com.yangtu.nearbyshop.db.domain.NearbyshopSetting;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 设置表 服务类
 * </p>
 *
 * @author wanglei
 * @since 2019-10-07
 */
public interface INearbyshopSettingService extends IService<NearbyshopSetting> {

}
