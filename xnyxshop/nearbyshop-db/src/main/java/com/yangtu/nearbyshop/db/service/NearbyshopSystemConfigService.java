package com.yangtu.nearbyshop.db.service;

import com.yangtu.nearbyshop.db.dao.NearbyshopSystemMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopSystem;
import com.yangtu.nearbyshop.db.domain.NearbyshopSystemExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class NearbyshopSystemConfigService {
    @Resource
    private NearbyshopSystemMapper systemMapper;

    public Map<String, String> queryAll() {
        NearbyshopSystemExample example = new NearbyshopSystemExample();
        example.or().andDeletedEqualTo(false);

        List<NearbyshopSystem> systemList = systemMapper.selectByExample(example);
        Map<String, String> systemConfigs = new HashMap<>();
        for (NearbyshopSystem item : systemList) {
            systemConfigs.put(item.getKeyName(), item.getKeyValue());
        }

        return systemConfigs;
    }

    public Map<String, String> listMail() {
        NearbyshopSystemExample example = new NearbyshopSystemExample();
        example.or().andKeyNameLike("nearbyshop_mall_%").andDeletedEqualTo(false);
        List<NearbyshopSystem> systemList = systemMapper.selectByExample(example);
        Map<String, String> data = new HashMap<>();
        for(NearbyshopSystem system : systemList){
            data.put(system.getKeyName(), system.getKeyValue());
        }
        return data;
    }

    public Map<String, String> listWx() {
        NearbyshopSystemExample example = new NearbyshopSystemExample();
        example.or().andKeyNameLike("nearbyshop_wx_%").andDeletedEqualTo(false);
        List<NearbyshopSystem> systemList = systemMapper.selectByExample(example);
        Map<String, String> data = new HashMap<>();
        for(NearbyshopSystem system : systemList){
            data.put(system.getKeyName(), system.getKeyValue());
        }
        return data;
    }

    public Map<String, String> listOrder() {
        NearbyshopSystemExample example = new NearbyshopSystemExample();
        example.or().andKeyNameLike("nearbyshop_order_%").andDeletedEqualTo(false);
        List<NearbyshopSystem> systemList = systemMapper.selectByExample(example);
        Map<String, String> data = new HashMap<>();
        for(NearbyshopSystem system : systemList){
            data.put(system.getKeyName(), system.getKeyValue());
        }
        return data;
    }

    public Map<String, String> listExpress() {
        NearbyshopSystemExample example = new NearbyshopSystemExample();
        example.or().andKeyNameLike("nearbyshop_express_%").andDeletedEqualTo(false);
        List<NearbyshopSystem> systemList = systemMapper.selectByExample(example);
        Map<String, String> data = new HashMap<>();
        for(NearbyshopSystem system : systemList){
            data.put(system.getKeyName(), system.getKeyValue());
        }
        return data;
    }

    public Map<String, String> listMerc() {
        NearbyshopSystemExample example = new NearbyshopSystemExample();
        example.or().andKeyNameLike("nearbyshop_merc_%").andDeletedEqualTo(false);
        List<NearbyshopSystem> systemList = systemMapper.selectByExample(example);
        Map<String, String> data = new HashMap<>();
        for(NearbyshopSystem system : systemList){
            data.put(system.getKeyName(), system.getKeyValue());
        }
        return data;
    }

    public void updateConfig(Map<String, String> data) {
        for (Map.Entry<String, String> entry : data.entrySet()) {
            NearbyshopSystemExample example = new NearbyshopSystemExample();
            example.or().andKeyNameEqualTo(entry.getKey()).andDeletedEqualTo(false);

            NearbyshopSystem system = new NearbyshopSystem();
            system.setKeyName(entry.getKey());
            system.setKeyValue(entry.getValue());
            system.setUpdateTime(LocalDateTime.now());
            systemMapper.updateByExampleSelective(system, example);
        }

    }

    public void addConfig(String key, String value) {
        NearbyshopSystem system = new NearbyshopSystem();
        system.setKeyName(key);
        system.setKeyValue(value);
        system.setAddTime(LocalDateTime.now());
        system.setUpdateTime(LocalDateTime.now());
        systemMapper.insertSelective(system);
    }
}
