package com.yangtu.nearbyshop.db.service;

import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.dao.NearbyshopMercMapper2;
import com.yangtu.nearbyshop.db.domain.*;
import com.yangtu.nearbyshop.db.dao.NearbyshopMercRebateMapper;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopMercRebateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.*;

/**
 * <p>
 * 门店返利统计表 服务实现类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-19
 */
@Service
public class NearbyshopMercRebateServiceImpl extends ServiceImpl<NearbyshopMercRebateMapper,
        NearbyshopMercRebate> implements INearbyshopMercRebateService {

    @Autowired
    NearbyshopMercRebateMapper rebateMapper;

    @Autowired
    NearbyshopOrderGoodsService goodsService;

    @Autowired
    NearbyshopMercMapper2 mercMapper2;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addRebateData(NearbyshopOrder order) {
        List<NearbyshopOrderGoods> goodsList = goodsService.queryByOid(order.getId());
        List<NearbyshopMercRebate> rebates = new ArrayList<>();
        String items = "[\"";
        //1.给团长进行商品返利
        NearbyshopMercRebate settle = new NearbyshopMercRebate();
        BigDecimal fenzAmount = BigDecimal.ZERO;
        for (NearbyshopOrderGoods orderGoods: goodsList) {
            BigDecimal fl = orderGoods.getMercProfit().multiply(new BigDecimal(orderGoods.getNumber()));
            fenzAmount = fenzAmount.add(fl);
            items += (orderGoods.getGoodsName()+":单价"+orderGoods.getPrice()
                    +",提成"+orderGoods.getMercProfit()+"*"+"数量"
                    +orderGoods.getNumber()+"="+fl.toPlainString()) + "\",";
        }
        items = items.substring(0,items.length()-1) + "]";
        fenzAmount = fenzAmount.setScale(2, BigDecimal.ROUND_DOWN).stripTrailingZeros();
        settle.setAddTime(new Date());
        settle.setAmount(fenzAmount);
        settle.setMercNo(order.getMercNo());
        settle.setRebateOrderId(order.getId());
        settle.setRebateType("ticheng-goods");
        settle.setRebateDesc("订单"+order.getOrderSn()+"的货品提成");
        settle.setItems(JSONUtils.toJSONString(items));
        rebates.add(settle);

        //2.给上级返利1%
        NearbyshopMerc2 mercMe = mercMapper2.selectOne(new QueryWrapper<NearbyshopMerc2>()
            .eq("merc_no",order.getMercNo()));
        if(!StringUtils.isEmpty(mercMe.getReferMerc())) {
            NearbyshopMerc2 mercOne = mercMapper2.selectOne(new QueryWrapper<NearbyshopMerc2>()
                    .eq("merc_no", mercMe.getReferMerc()));
            if (mercOne != null) {
                NearbyshopMercRebate settle1 = new NearbyshopMercRebate();
                BigDecimal fenzAmount1 = BigDecimal.ZERO;
                fenzAmount1 = order.getActualPrice().multiply(new BigDecimal("0.01")).setScale(2, BigDecimal.ROUND_DOWN).stripTrailingZeros();
                settle1.setAddTime(new Date());
                settle1.setAmount(fenzAmount1);
                settle1.setMercNo(mercOne.getMercNo());
                settle1.setFromMercNoOne(order.getMercNo());
                settle1.setRebateOrderId(order.getId());
                settle1.setRebateType("ticheng-levelOne");
                settle1.setRebateDesc("一级门店" + mercMe.getMercName() + "{1%}提成");
                settle1.setItems("成交额:" + order.getActualPrice());
                rebates.add(settle1);

                //3.给上上级返利1%
                if(!StringUtils.isEmpty(mercOne.getReferMerc())) {
                    NearbyshopMerc2 mercTwo = mercMapper2.selectOne(new QueryWrapper<NearbyshopMerc2>()
                            .eq("merc_no", mercOne.getReferMerc()));
                    if (mercTwo != null) {
                        NearbyshopMercRebate settle2 = new NearbyshopMercRebate();
                        BigDecimal fenzAmount2 = BigDecimal.ZERO;
                        fenzAmount2 = order.getActualPrice().multiply(new BigDecimal("0.003")).setScale(2, BigDecimal.ROUND_DOWN).stripTrailingZeros();
                        settle2.setAddTime(new Date());
                        settle2.setAmount(fenzAmount2);
                        settle2.setMercNo(mercTwo.getMercNo());
                        settle2.setFromMercNoOne(mercOne.getMercNo());
                        settle2.setFromMercNoTwo(order.getMercNo());
                        settle2.setRebateOrderId(order.getId());
                        settle2.setRebateType("ticheng-levelTwo");
                        settle2.setRebateDesc("二级门店" + mercMe.getMercName() + "{0.3%}提成");
                        settle2.setItems("成交额:" + order.getActualPrice());
                        rebates.add(settle2);
                    }
                }
            }
        }

        saveBatch(rebates);
    }

    @Override
    public IPage<NearbyshopMercRebate> getTimeRebate(Page<NearbyshopMercRebate> page,
                                                     String mercName, String mercNo,
                                                     String startTime, String endTime) {
        return rebateMapper.getTimeRebate(page,mercName,mercNo,startTime,endTime);
    }

    @Override
    public IPage<NearbyshopMercRebateSettle> getDailyRebate(Page<NearbyshopMercRebate> page,
                                                     String mercName, String mercNo,
                                                     String startTime, String endTime) {
        return rebateMapper.getDailyRebate(page,mercName,mercNo,startTime,endTime);
    }

    @Override
    public String countAllRebate(String mercNo,
                                 String startTime, String endTime) {
        return rebateMapper.countAllRebate(mercNo,startTime, endTime);
    }
}
