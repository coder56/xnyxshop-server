package com.yangtu.nearbyshop.db.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercRebate;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercRebateSettle;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 门店返利统计表 Mapper 接口
 * </p>
 *
 * @author wanglei
 * @since 2019-09-19
 */
public interface NearbyshopMercRebateMapper extends BaseMapper<NearbyshopMercRebate> {

    IPage<NearbyshopMercRebate> getTimeRebate(Page<NearbyshopMercRebate> page,
                                              @Param("mercName") String mercName,
                                              @Param("mercNo") String mercNo,
                                              @Param("startTime") String startTime,
                                              @Param("endTime") String endTime);

    IPage<NearbyshopMercRebateSettle> getDailyRebate(Page<NearbyshopMercRebate> page,
                                                     @Param("mercName") String mercName,
                                                     @Param("mercNo") String mercNo,
                                                     @Param("startTime") String startTime,
                                                     @Param("endTime") String endTime);

    String countAllRebate(@Param("mercNo") String mercNo,@Param("startTime") String startTime,
                          @Param("endTime") String endTime);
}
