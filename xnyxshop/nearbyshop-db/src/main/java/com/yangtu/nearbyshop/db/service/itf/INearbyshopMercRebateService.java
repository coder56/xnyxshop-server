package com.yangtu.nearbyshop.db.service.itf;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercRebate;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercRebateSettle;
import com.yangtu.nearbyshop.db.domain.NearbyshopOrder;

/**
 * <p>
 * 门店返利统计表 服务类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-19
 */
public interface INearbyshopMercRebateService extends IService<NearbyshopMercRebate> {

    void addRebateData(NearbyshopOrder order);

    IPage<NearbyshopMercRebate> getTimeRebate(Page<NearbyshopMercRebate> page,
                                              String mercName,String mercNo,String startTime,
                                              String endTime);

    IPage<NearbyshopMercRebateSettle> getDailyRebate(Page<NearbyshopMercRebate> page,
                                                     String mercName, String mercNo, String startTime,
                                                     String endTime);

    String countAllRebate(String mercNo,String startTime,
                          String endTime);

}
