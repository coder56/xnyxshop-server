package com.yangtu.nearbyshop.db.service;

import com.yangtu.nearbyshop.db.dao.NearbyshopGoodsAttributeMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopGoodsAttribute;
import com.yangtu.nearbyshop.db.domain.NearbyshopGoodsAttributeExample;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class NearbyshopGoodsAttributeService {
    @Resource
    private NearbyshopGoodsAttributeMapper goodsAttributeMapper;

    public List<NearbyshopGoodsAttribute> queryByGid(Integer goodsId) {
        NearbyshopGoodsAttributeExample example = new NearbyshopGoodsAttributeExample();
        example.or().andGoodsIdEqualTo(goodsId).andDeletedEqualTo(false);
        return goodsAttributeMapper.selectByExample(example);
    }

    public void add(NearbyshopGoodsAttribute goodsAttribute) {
        goodsAttribute.setAddTime(LocalDateTime.now());
        goodsAttribute.setUpdateTime(LocalDateTime.now());
        goodsAttributeMapper.insertSelective(goodsAttribute);
    }

    public NearbyshopGoodsAttribute findById(Integer id) {
        return goodsAttributeMapper.selectByPrimaryKey(id);
    }

    public void deleteByGid(Integer gid) {
        NearbyshopGoodsAttributeExample example = new NearbyshopGoodsAttributeExample();
        example.or().andGoodsIdEqualTo(gid);
        goodsAttributeMapper.logicalDeleteByExample(example);
    }
}
