package com.yangtu.nearbyshop.db.service;

import com.yangtu.nearbyshop.db.domain.NearbyshopBdSettle;
import com.yangtu.nearbyshop.db.dao.NearbyshopBdSettleMapper;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopBdSettleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * BD门店销售统计表 服务实现类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-18
 */
@Service
public class NearbyshopBdSettleServiceImpl extends ServiceImpl<NearbyshopBdSettleMapper, NearbyshopBdSettle> implements INearbyshopBdSettleService {

}
