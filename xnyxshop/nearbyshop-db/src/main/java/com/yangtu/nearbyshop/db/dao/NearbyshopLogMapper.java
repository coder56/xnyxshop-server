package com.yangtu.nearbyshop.db.dao;

import com.yangtu.nearbyshop.db.domain.NearbyshopLog;
import com.yangtu.nearbyshop.db.domain.NearbyshopLogExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NearbyshopLogMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     */
    long countByExample(NearbyshopLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     */
    int deleteByExample(NearbyshopLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     */
    int insert(NearbyshopLog record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     */
    int insertSelective(NearbyshopLog record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopLog selectOneByExample(NearbyshopLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopLog selectOneByExampleSelective(@Param("example") NearbyshopLogExample example, @Param("selective") NearbyshopLog.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    List<NearbyshopLog> selectByExampleSelective(@Param("example") NearbyshopLogExample example, @Param("selective") NearbyshopLog.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     */
    List<NearbyshopLog> selectByExample(NearbyshopLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopLog selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") NearbyshopLog.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     */
    NearbyshopLog selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopLog selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") NearbyshopLog record, @Param("example") NearbyshopLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") NearbyshopLog record, @Param("example") NearbyshopLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(NearbyshopLog record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(NearbyshopLog record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int logicalDeleteByExample(@Param("example") NearbyshopLogExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_log
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int logicalDeleteByPrimaryKey(Integer id);
}