package com.yangtu.nearbyshop.db.dao;

import com.yangtu.nearbyshop.db.domain.NearbyshopGoodsProduct;
import com.yangtu.nearbyshop.db.domain.NearbyshopGoodsProductExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface NearbyshopGoodsProductMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     */
    long countByExample(NearbyshopGoodsProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     */
    int deleteByExample(NearbyshopGoodsProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     */
    int insert(NearbyshopGoodsProduct record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     */
    int insertSelective(NearbyshopGoodsProduct record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopGoodsProduct selectOneByExample(NearbyshopGoodsProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopGoodsProduct selectOneByExampleSelective(@Param("example") NearbyshopGoodsProductExample example, @Param("selective") NearbyshopGoodsProduct.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    List<NearbyshopGoodsProduct> selectByExampleSelective(@Param("example") NearbyshopGoodsProductExample example, @Param("selective") NearbyshopGoodsProduct.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     */
    List<NearbyshopGoodsProduct> selectByExample(NearbyshopGoodsProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopGoodsProduct selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") NearbyshopGoodsProduct.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     */
    NearbyshopGoodsProduct selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    NearbyshopGoodsProduct selectByPrimaryKeyWithLogicalDelete(@Param("id") Integer id, @Param("andLogicalDeleted") boolean andLogicalDeleted);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") NearbyshopGoodsProduct record, @Param("example") NearbyshopGoodsProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") NearbyshopGoodsProduct record, @Param("example") NearbyshopGoodsProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(NearbyshopGoodsProduct record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(NearbyshopGoodsProduct record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int logicalDeleteByExample(@Param("example") NearbyshopGoodsProductExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table nearbyshop_goods_product
     *
     * @mbg.generated
     * @project https://github.com/itfsw/mybatis-generator-plugin
     */
    int logicalDeleteByPrimaryKey(Integer id);
}