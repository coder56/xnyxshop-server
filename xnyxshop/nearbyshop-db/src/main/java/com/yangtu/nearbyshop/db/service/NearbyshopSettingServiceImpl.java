package com.yangtu.nearbyshop.db.service;

import com.yangtu.nearbyshop.db.domain.NearbyshopSetting;
import com.yangtu.nearbyshop.db.dao.NearbyshopSettingMapper;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopSettingService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 设置表 服务实现类
 * </p>
 *
 * @author wanglei
 * @since 2019-10-07
 */
@Service
public class NearbyshopSettingServiceImpl extends ServiceImpl<NearbyshopSettingMapper, NearbyshopSetting> implements INearbyshopSettingService {

}
