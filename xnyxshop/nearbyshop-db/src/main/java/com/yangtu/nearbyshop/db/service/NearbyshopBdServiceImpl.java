package com.yangtu.nearbyshop.db.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopBd;
import com.yangtu.nearbyshop.db.dao.NearbyshopBdMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopBdSettle;
import com.yangtu.nearbyshop.db.domain.NearbyshopMerc;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopBdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * BD表 服务实现类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-15
 */
@Service
public class NearbyshopBdServiceImpl extends ServiceImpl<NearbyshopBdMapper, NearbyshopBd>
        implements INearbyshopBdService {

    @Autowired
    NearbyshopBdMapper bdMapper;

    @Override
    public IPage<NearbyshopBd> getBdList(Page<NearbyshopBd> page, String bdName) {
        return bdMapper.getBdList(page,bdName);
    }

    @Override
    public IPage<NearbyshopMerc> getMercListByBd(Page<NearbyshopMerc> page, String bdNo) {
        return bdMapper.getMercListByBd(page,bdNo);
    }

    @Override
    public List<NearbyshopBdSettle> getBdSettleByWeek(String startTime,String endTime) {
        return bdMapper.getBdSettleByWeek(startTime,endTime);
    }

    @Override
    public List<NearbyshopBdSettle> getBdSettleList(String bdNo) {
        return bdMapper.getBdSettleList(bdNo);
    }
}
