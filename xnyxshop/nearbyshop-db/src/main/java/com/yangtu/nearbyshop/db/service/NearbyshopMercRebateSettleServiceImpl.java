package com.yangtu.nearbyshop.db.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercRebate;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercRebateSettle;
import com.yangtu.nearbyshop.db.dao.NearbyshopMercRebateSettleMapper;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopMercRebateSettleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 门店返利日统计表 服务实现类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-20
 */
@Service
public class NearbyshopMercRebateSettleServiceImpl extends ServiceImpl<NearbyshopMercRebateSettleMapper, NearbyshopMercRebateSettle> implements INearbyshopMercRebateSettleService {

    @Autowired
    private NearbyshopMercRebateSettleMapper rebateSettleMapper;

    @Override
    public IPage<NearbyshopMercRebateSettle> getDailyRebateSettle(Page<NearbyshopMercRebateSettle> page, String mercName, String mercNo, String startTime, String endTime) {
        return rebateSettleMapper.getDailyRebateSettle(page,mercName,mercNo,startTime,endTime);
    }

    @Override
    public NearbyshopMercRebateSettle getWeekRebateSettle(String mercName, String mercNo, String startTime, String endTime) {
        return rebateSettleMapper.getWeekRebateSettle(mercName,mercNo,startTime,endTime);
    }
}
