package com.yangtu.nearbyshop.db.service;

import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopBrandMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopBrand;
import com.yangtu.nearbyshop.db.domain.NearbyshopBrandExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class NearbyshopBrandService {
    @Resource
    private NearbyshopBrandMapper brandMapper;
    private NearbyshopBrand.Column[] columns = new NearbyshopBrand.Column[]{NearbyshopBrand.Column.id, NearbyshopBrand.Column.name, NearbyshopBrand.Column.desc, NearbyshopBrand.Column.picUrl, NearbyshopBrand.Column.floorPrice};

    public List<NearbyshopBrand> queryVO(int offset, int limit) {
        NearbyshopBrandExample example = new NearbyshopBrandExample();
        example.or().andDeletedEqualTo(false);
        example.setOrderByClause("add_time desc");
        PageHelper.startPage(offset, limit);
        return brandMapper.selectByExampleSelective(example, columns);
    }

    public int queryTotalCount() {
        NearbyshopBrandExample example = new NearbyshopBrandExample();
        example.or().andDeletedEqualTo(false);
        return (int) brandMapper.countByExample(example);
    }

    public NearbyshopBrand findById(Integer id) {
        return brandMapper.selectByPrimaryKey(id);
    }

    public List<NearbyshopBrand> querySelective(String id, String name, Integer page, Integer size, String sort, String order) {
        NearbyshopBrandExample example = new NearbyshopBrandExample();
        NearbyshopBrandExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(id)) {
            criteria.andIdEqualTo(Integer.valueOf(id));
        }
        if (!StringUtils.isEmpty(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return brandMapper.selectByExample(example);
    }

    public int updateById(NearbyshopBrand brand) {
        brand.setUpdateTime(LocalDateTime.now());
        return brandMapper.updateByPrimaryKeySelective(brand);
    }

    public void deleteById(Integer id) {
        brandMapper.logicalDeleteByPrimaryKey(id);
    }

    public void add(NearbyshopBrand brand) {
        brand.setAddTime(LocalDateTime.now());
        brand.setUpdateTime(LocalDateTime.now());
        brandMapper.insertSelective(brand);
    }

    public List<NearbyshopBrand> all() {
        NearbyshopBrandExample example = new NearbyshopBrandExample();
        example.or().andDeletedEqualTo(false);
        return brandMapper.selectByExample(example);
    }
}
