package com.yangtu.nearbyshop.db.service.itf;

import com.yangtu.nearbyshop.db.domain.NearbyshopOrderRefund;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yangtu.nearbyshop.db.vo.OrderRefundVo;

/**
 * <p>
 * 退款表 服务类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-14
 */
public interface INearbyshopOrderRefundService extends IService<NearbyshopOrderRefund> {

    void refundApply(NearbyshopOrderRefund body);

    OrderRefundVo countRefund(Integer userId,Integer orderStatus,Integer orderId,String refundBy,String startTm,String endTm);

}
