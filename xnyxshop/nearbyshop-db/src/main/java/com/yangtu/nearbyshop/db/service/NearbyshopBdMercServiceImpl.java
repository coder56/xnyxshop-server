package com.yangtu.nearbyshop.db.service;

import com.yangtu.nearbyshop.db.domain.NearbyshopBdMerc;
import com.yangtu.nearbyshop.db.dao.NearbyshopBdMercMapper;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopBdMercService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * BD门店表 服务实现类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-15
 */
@Service
public class NearbyshopBdMercServiceImpl extends ServiceImpl<NearbyshopBdMercMapper, NearbyshopBdMerc> implements INearbyshopBdMercService {

}
