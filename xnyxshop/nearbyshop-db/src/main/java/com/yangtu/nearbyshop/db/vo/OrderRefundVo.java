package com.yangtu.nearbyshop.db.vo;

public class OrderRefundVo {
    private String totalGoodsNum;
    private String totalOrderPrice;
    private String totalActualPrice;

    public String getTotalGoodsNum() {
        return totalGoodsNum;
    }

    public void setTotalGoodsNum(String totalGoodsNum) {
        this.totalGoodsNum = totalGoodsNum;
    }

    public String getTotalOrderPrice() {
        return totalOrderPrice;
    }

    public void setTotalOrderPrice(String totalOrderPrice) {
        this.totalOrderPrice = totalOrderPrice;
    }

    public String getTotalActualPrice() {
        return totalActualPrice;
    }

    public void setTotalActualPrice(String totalActualPrice) {
        this.totalActualPrice = totalActualPrice;
    }
}
