package com.yangtu.nearbyshop.db.service.itf;

import com.yangtu.nearbyshop.db.domain.NearbyshopBdMerc;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * BD门店表 服务类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-15
 */
public interface INearbyshopBdMercService extends IService<NearbyshopBdMerc> {

}
