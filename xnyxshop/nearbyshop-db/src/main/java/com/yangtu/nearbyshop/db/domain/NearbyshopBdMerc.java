package com.yangtu.nearbyshop.db.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * BD门店表
 * </p>
 *
 * @author wanglei
 * @since 2019-09-15
 */
public class NearbyshopBdMerc implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * BD编号
     */
    private String bdNo;

    /**
     * 门店编号
     */
    private String mercNo;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBdNo() {
        return bdNo;
    }

    public void setBdNo(String bdNo) {
        this.bdNo = bdNo;
    }

    public String getMercNo() {
        return mercNo;
    }

    public void setMercNo(String mercNo) {
        this.mercNo = mercNo;
    }

    @Override
    public String toString() {
        return "NearbyshopBdMerc{" +
        "id=" + id +
        ", bdNo=" + bdNo +
        ", mercNo=" + mercNo +
        "}";
    }
}
