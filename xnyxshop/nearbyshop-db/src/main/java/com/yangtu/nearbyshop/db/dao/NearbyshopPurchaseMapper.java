package com.yangtu.nearbyshop.db.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopPurchase;
import com.yangtu.nearbyshop.db.vo.NearbyshopPurchaseVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 采购表 Mapper 接口
 * </p>
 *
 * @author wanglei
 * @since 2019-08-26
 */
public interface NearbyshopPurchaseMapper extends BaseMapper<NearbyshopPurchase> {

    IPage<NearbyshopPurchase> selectByPage(Page page,
                                           @Param("purchaseStatus") Integer purchaseStatus,
                                           @Param("purchaseNo") String purchaseNo);

    IPage<NearbyshopPurchaseVo> selectOrderByPage(Page<NearbyshopPurchaseVo> page,
                                                  @Param("purchaseStatus")  Integer purchaseStatus);

    void updatePurchaseStatus(@Param("pno")String pno,@Param("purchaseStatus")Integer purchaseStatus);
}
