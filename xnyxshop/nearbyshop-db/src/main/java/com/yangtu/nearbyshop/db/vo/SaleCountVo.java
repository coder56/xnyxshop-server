package com.yangtu.nearbyshop.db.vo;

public class SaleCountVo {
    private String countName;
    private String countValue;

    public String getCountName() {
        return countName;
    }

    public void setCountName(String countName) {
        this.countName = countName;
    }

    public String getCountValue() {
        return countValue;
    }

    public void setCountValue(String countValue) {
        this.countValue = countValue;
    }
}
