package com.yangtu.nearbyshop.db.service;

import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopLogMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopAd;
import com.yangtu.nearbyshop.db.domain.NearbyshopLog;
import com.yangtu.nearbyshop.db.domain.NearbyshopLogExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class NearbyshopLogService {
    @Resource
    private NearbyshopLogMapper logMapper;

    public void deleteById(Integer id) {
        logMapper.logicalDeleteByPrimaryKey(id);
    }

    public void add(NearbyshopLog log) {
        log.setAddTime(LocalDateTime.now());
        log.setUpdateTime(LocalDateTime.now());
        logMapper.insertSelective(log);
    }

    public List<NearbyshopLog> querySelective(String name, Integer page, Integer size, String sort, String order) {
        NearbyshopLogExample example = new NearbyshopLogExample();
        NearbyshopLogExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(name)) {
            criteria.andAdminLike("%" + name + "%");
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return logMapper.selectByExample(example);
    }
}
