package com.yangtu.nearbyshop.db.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercSettle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 门店销售统计表 Mapper 接口
 * </p>
 *
 * @author wanglei
 * @since 2019-09-19
 */
public interface NearbyshopMercSettleMapper extends BaseMapper<NearbyshopMercSettle> {

    List<NearbyshopMercSettle> getMercSettleByWeek(@Param("startTime") String startTime,
                                               @Param("endTime") String endTime);

    IPage<NearbyshopMercSettle> getMercSettleList(Page<NearbyshopMercSettle> page, @Param("mercNo") String mercNo);
}
