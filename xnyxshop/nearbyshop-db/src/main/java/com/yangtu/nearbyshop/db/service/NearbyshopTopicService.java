package com.yangtu.nearbyshop.db.service;

import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopTopicMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopTopic;
import com.yangtu.nearbyshop.db.domain.NearbyshopTopicExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class NearbyshopTopicService {
    @Resource
    private NearbyshopTopicMapper topicMapper;
    private NearbyshopTopic.Column[] columns = new NearbyshopTopic.Column[]{NearbyshopTopic.Column.id, NearbyshopTopic.Column.title, NearbyshopTopic.Column.subtitle, NearbyshopTopic.Column.price, NearbyshopTopic.Column.picUrl, NearbyshopTopic.Column.readCount};

    public List<NearbyshopTopic> queryList(int offset, int limit) {
        return queryList(offset, limit, "add_time", "desc");
    }

    public List<NearbyshopTopic> queryList(int offset, int limit, String sort, String order) {
        NearbyshopTopicExample example = new NearbyshopTopicExample();
        example.or().andDeletedEqualTo(false);
        example.setOrderByClause(sort + " " + order);
        PageHelper.startPage(offset, limit);
        return topicMapper.selectByExampleSelective(example, columns);
    }

    public int queryTotal() {
        NearbyshopTopicExample example = new NearbyshopTopicExample();
        example.or().andDeletedEqualTo(false);
        return (int) topicMapper.countByExample(example);
    }

    public NearbyshopTopic findById(Integer id) {
        NearbyshopTopicExample example = new NearbyshopTopicExample();
        example.or().andIdEqualTo(id).andDeletedEqualTo(false);
        return topicMapper.selectOneByExampleWithBLOBs(example);
    }

    public List<NearbyshopTopic> queryRelatedList(Integer id, int offset, int limit) {
        NearbyshopTopicExample example = new NearbyshopTopicExample();
        example.or().andIdEqualTo(id).andDeletedEqualTo(false);
        List<NearbyshopTopic> topics = topicMapper.selectByExample(example);
        if (topics.size() == 0) {
            return queryList(offset, limit, "add_time", "desc");
        }
        NearbyshopTopic topic = topics.get(0);

        example = new NearbyshopTopicExample();
        example.or().andIdNotEqualTo(topic.getId()).andDeletedEqualTo(false);
        PageHelper.startPage(offset, limit);
        List<NearbyshopTopic> relateds = topicMapper.selectByExampleWithBLOBs(example);
        if (relateds.size() != 0) {
            return relateds;
        }

        return queryList(offset, limit, "add_time", "desc");
    }

    public List<NearbyshopTopic> querySelective(String title, String subtitle, Integer page, Integer limit, String sort, String order) {
        NearbyshopTopicExample example = new NearbyshopTopicExample();
        NearbyshopTopicExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(title)) {
            criteria.andTitleLike("%" + title + "%");
        }
        if (!StringUtils.isEmpty(subtitle)) {
            criteria.andSubtitleLike("%" + subtitle + "%");
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, limit);
        return topicMapper.selectByExampleWithBLOBs(example);
    }

    public int updateById(NearbyshopTopic topic) {
        topic.setUpdateTime(LocalDateTime.now());
        NearbyshopTopicExample example = new NearbyshopTopicExample();
        example.or().andIdEqualTo(topic.getId());
        return topicMapper.updateByExampleSelective(topic, example);
    }

    public void deleteById(Integer id) {
        topicMapper.logicalDeleteByPrimaryKey(id);
    }

    public void add(NearbyshopTopic topic) {
        topic.setAddTime(LocalDateTime.now());
        topic.setUpdateTime(LocalDateTime.now());
        topicMapper.insertSelective(topic);
    }


}
