package com.yangtu.nearbyshop.db.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopBdMercMapper;
import com.yangtu.nearbyshop.db.dao.NearbyshopMercMapper;
import com.yangtu.nearbyshop.db.domain.*;
import com.yangtu.nearbyshop.db.util.MercUtil;
import com.yangtu.nearbyshop.db.vo.MercSonCountVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

@Service
public class NearbyshopMercService {
    @Resource
    private NearbyshopMercMapper mercMapper;

    @Autowired
    private NearbyshopBdMercMapper bdMercMapper;

    public NearbyshopMerc findById(Integer userId) {
        return mercMapper.selectByPrimaryKey(userId);
    }

    public MercVo findMercVoById(Integer userId) {
        NearbyshopMerc user = findById(userId);
        MercVo userVo = new MercVo();
        userVo.setNickname(user.getNickname());
        userVo.setAvatar(user.getAvatar());
        return userVo;
    }

    public NearbyshopMerc queryByOid(String openId) {
        NearbyshopMercExample example = new NearbyshopMercExample();
        example.or().andWeixinOpenidEqualTo(openId).andDeletedEqualTo(false);
        return mercMapper.selectOneByExample(example);
    }

    public void add(NearbyshopMerc user,String bdNo) {
        user.setAddTime(LocalDateTime.now());
        user.setUpdateTime(LocalDateTime.now());
        mercMapper.insertSelective(user);

        if(!StringUtils.isEmpty(bdNo)){
            NearbyshopBdMerc bdMerc = new NearbyshopBdMerc();
            bdMerc.setMercNo(user.getMercNo());
            bdMerc.setBdNo(bdNo);
            bdMercMapper.insert(bdMerc);
        }
    }

    public int updateById(NearbyshopMerc user,String bdNo) {
        if(!StringUtils.isEmpty(bdNo)){
            NearbyshopBdMerc bdMerc = bdMercMapper.selectOne(new QueryWrapper<NearbyshopBdMerc>()
                    .eq("merc_no",user.getMercNo()));
            if(bdMerc == null){
                bdMerc = new NearbyshopBdMerc();
                bdMerc.setMercNo(user.getMercNo());
                bdMerc.setBdNo(bdNo);
                bdMercMapper.insert(bdMerc);
            }else {
                bdMerc.setBdNo(bdNo);
                bdMercMapper.updateById(bdMerc);
            }
        }

        user.setUpdateTime(LocalDateTime.now());
        return mercMapper.updateByPrimaryKeySelective(user);
    }

    public List<NearbyshopMerc> querySelective(String mercname, String mobile,String mercNo,
                                               Integer page, Integer size, String sort, String order) {
        NearbyshopMercExample example = new NearbyshopMercExample();
        NearbyshopMercExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(mercname)) {
            criteria.andMercNameLike("%" + mercname + "%");
        }
        if (!StringUtils.isEmpty(mobile)) {
            criteria.andMobileEqualTo(mobile);
        }
        if (!StringUtils.isEmpty(mercNo)) {
            criteria.andMercNoEqualTo(mercNo);
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return mercMapper.selectByExample(example);
    }

    public int count() {
        NearbyshopMercExample example = new NearbyshopMercExample();
        example.or().andDeletedEqualTo(false);

        return (int) mercMapper.countByExample(example);
    }

    public NearbyshopMerc queryByMercNo(String mercNo) {
        NearbyshopMercExample example = new NearbyshopMercExample();
        example.or().andMercNoEqualTo(mercNo).andDeletedEqualTo(false).andStatusEqualTo(MercUtil.STATUS_NORMAL);
        return mercMapper.selectOneByExample(example);
    }

    public List<NearbyshopMerc> queryByname(String username) {
        NearbyshopMercExample example = new NearbyshopMercExample();
        example.or().andUsernameEqualTo(username).andDeletedEqualTo(false);
        return mercMapper.selectByExample(example);
    }
    public boolean checkByMercname(String username) {
        NearbyshopMercExample example = new NearbyshopMercExample();
        example.or().andMercNameEqualTo(username).andDeletedEqualTo(false);
        return mercMapper.countByExample(example) != 0;
    }

    public List<NearbyshopMerc> queryByMobile(String mobile) {
        NearbyshopMercExample example = new NearbyshopMercExample();
        example.or().andMobileEqualTo(mobile).andDeletedEqualTo(false);
        return mercMapper.selectByExample(example);
    }

    public List<NearbyshopMerc> queryByOpenid(String openid) {
        NearbyshopMercExample example = new NearbyshopMercExample();
        example.or().andWeixinOpenidEqualTo(openid).andDeletedEqualTo(false);
        return mercMapper.selectByExample(example);
    }

    public void deleteById(Integer id) {
        mercMapper.logicalDeleteByPrimaryKey(id);
    }

    public List<Map<String, String>> queryNearbyMercList(String longitude, String latitude, String mercNm){
        return mercMapper.selectNearByMerc(longitude, latitude, mercNm);
    }

    public List<NearbyshopMerc> queryByReferMercNo(String mercNo) {
        NearbyshopMercExample example = new NearbyshopMercExample();
        example.or().andReferMercEqualTo(mercNo).andDeletedEqualTo(false);
        return mercMapper.selectByExample(example);
    }

    public List<NearbyshopMerc> queryByMercNoList(String mercNo, Integer page, Integer size) {
        NearbyshopMercExample example = new NearbyshopMercExample();
        NearbyshopMercExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(mercNo)) {
            criteria.andMercNoEqualTo(mercNo);
        }

        PageHelper.startPage(page, size);
        return mercMapper.selectByExample(example);
    }

    public MercSonCountVo countSon(Integer id) {
        return mercMapper.countSon(id);
    }
}
