package com.yangtu.nearbyshop.db.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercSettle;
import com.yangtu.nearbyshop.db.dao.NearbyshopMercSettleMapper;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopMercSettleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 门店销售统计表 服务实现类
 * </p>
 *
 * @author wanglei
 * @since 2019-09-19
 */
@Service
public class NearbyshopMercSettleServiceImpl extends ServiceImpl<NearbyshopMercSettleMapper,
        NearbyshopMercSettle> implements INearbyshopMercSettleService {

    @Autowired
    NearbyshopMercSettleMapper mercSettleMapper;

    @Override
    public List<NearbyshopMercSettle> getMercSettleByWeek(String startTime, String endTime) {
        return mercSettleMapper.getMercSettleByWeek(startTime,endTime);
    }

    @Override
    public IPage<NearbyshopMercSettle> getMercSettleList(Page<NearbyshopMercSettle> page,
                                                         String mercNo) {
        return mercSettleMapper.getMercSettleList(page,mercNo);
    }
}
