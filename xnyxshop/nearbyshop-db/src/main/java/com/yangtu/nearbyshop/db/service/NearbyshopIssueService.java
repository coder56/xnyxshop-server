package com.yangtu.nearbyshop.db.service;

import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopIssueMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopIssue;
import com.yangtu.nearbyshop.db.domain.NearbyshopIssueExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class NearbyshopIssueService {
    @Resource
    private NearbyshopIssueMapper issueMapper;

    public List<NearbyshopIssue> query() {
        NearbyshopIssueExample example = new NearbyshopIssueExample();
        example.or().andDeletedEqualTo(false);
        return issueMapper.selectByExample(example);
    }

    public void deleteById(Integer id) {
        issueMapper.logicalDeleteByPrimaryKey(id);
    }

    public void add(NearbyshopIssue issue) {
        issue.setAddTime(LocalDateTime.now());
        issue.setUpdateTime(LocalDateTime.now());
        issueMapper.insertSelective(issue);
    }

    public List<NearbyshopIssue> querySelective(String question, Integer page, Integer size, String sort, String order) {
        NearbyshopIssueExample example = new NearbyshopIssueExample();
        NearbyshopIssueExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(question)) {
            criteria.andQuestionLike("%" + question + "%");
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return issueMapper.selectByExample(example);
    }

    public int updateById(NearbyshopIssue issue) {
        issue.setUpdateTime(LocalDateTime.now());
        return issueMapper.updateByPrimaryKeySelective(issue);
    }

    public NearbyshopIssue findById(Integer id) {
        return issueMapper.selectByPrimaryKey(id);
    }
}
