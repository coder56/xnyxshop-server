package com.yangtu.nearbyshop.db.service.itf;

import com.yangtu.nearbyshop.db.domain.NearbyshopLock;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 锁定记录表 服务类
 * </p>
 *
 * @author wanglei
 * @since 2019-08-30
 */
public interface INearbyshopLockService extends IService<NearbyshopLock> {

    void updatePurchaseStatus(String purchaseNo,Integer purchaseStatus);

}
