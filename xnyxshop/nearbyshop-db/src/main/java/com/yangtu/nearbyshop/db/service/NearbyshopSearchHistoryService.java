package com.yangtu.nearbyshop.db.service;

import com.github.pagehelper.PageHelper;
import com.yangtu.nearbyshop.db.dao.NearbyshopSearchHistoryMapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopSearchHistory;
import com.yangtu.nearbyshop.db.domain.NearbyshopSearchHistoryExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class NearbyshopSearchHistoryService {
    @Resource
    private NearbyshopSearchHistoryMapper searchHistoryMapper;

    public void save(NearbyshopSearchHistory searchHistory) {
        searchHistory.setAddTime(LocalDateTime.now());
        searchHistory.setUpdateTime(LocalDateTime.now());
        searchHistoryMapper.insertSelective(searchHistory);
    }

    public List<NearbyshopSearchHistory> queryByUid(int uid) {
        NearbyshopSearchHistoryExample example = new NearbyshopSearchHistoryExample();
        example.or().andUserIdEqualTo(uid).andDeletedEqualTo(false);
        example.setDistinct(true);
        return searchHistoryMapper.selectByExampleSelective(example, NearbyshopSearchHistory.Column.keyword);
    }

    public void deleteByUid(int uid) {
        NearbyshopSearchHistoryExample example = new NearbyshopSearchHistoryExample();
        example.or().andUserIdEqualTo(uid);
        searchHistoryMapper.logicalDeleteByExample(example);
    }

    public List<NearbyshopSearchHistory> querySelective(String userId, String keyword, Integer page, Integer size, String sort, String order) {
        NearbyshopSearchHistoryExample example = new NearbyshopSearchHistoryExample();
        NearbyshopSearchHistoryExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(userId)) {
            criteria.andUserIdEqualTo(Integer.valueOf(userId));
        }
        if (!StringUtils.isEmpty(keyword)) {
            criteria.andKeywordLike("%" + keyword + "%");
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return searchHistoryMapper.selectByExample(example);
    }
}
