package com.yangtu.nearbyshop.db.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 配送表
 * </p>
 *
 * @author wanglei
 * @since 2019-08-28
 */
public class NearbyshopDispatch implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 锁定批次
     */
    private String purchaseNo;

    /**
     * 团长编号
     */
    private String mercNo;

    /**
     * 商品数量
     */
    private Integer goodsNum;

    /**
     * 商品规格
     */
    private BigDecimal goodsPrice;

    /**
     * 创建时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date createTime;

    /**
     * 配送时间
     */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    private Date dispatchTime;

    /**
     * 配送状态 0:配送  1:已配送
     */
    private Integer dispatchStatus;

    /**
     * 采购状态  0:待采购 1：已采购
     */
    private Integer purchaseStatus;

    /**
     * 逻辑删除
     */
    private Integer deleted;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    public String getPurchaseNo() {
        return purchaseNo;
    }

    public void setPurchaseNo(String purchaseNo) {
        this.purchaseNo = purchaseNo;
    }
    public String getMercNo() {
        return mercNo;
    }

    public void setMercNo(String mercNo) {
        this.mercNo = mercNo;
    }
    public Integer getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(Integer goodsNum) {
        this.goodsNum = goodsNum;
    }
    public BigDecimal getGoodsPrice() {
        return goodsPrice;
    }

    public void setGoodsPrice(BigDecimal goodsPrice) {
        this.goodsPrice = goodsPrice;
    }

    public Integer getDispatchStatus() {
        return dispatchStatus;
    }

    public void setDispatchStatus(Integer dispatchStatus) {
        this.dispatchStatus = dispatchStatus;
    }
    public Integer getPurchaseStatus() {
        return purchaseStatus;
    }

    public void setPurchaseStatus(Integer purchaseStatus) {
        this.purchaseStatus = purchaseStatus;
    }
    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getDispatchTime() {
        return dispatchTime;
    }

    public void setDispatchTime(Date dispatchTime) {
        this.dispatchTime = dispatchTime;
    }

    @Override
    public String toString() {
        return "NearbyshopDispatch{" +
        "id=" + id +
        ", purchaseNo=" + purchaseNo +
        ", mercNo=" + mercNo +
        ", goodsNum=" + goodsNum +
        ", goodsPrice=" + goodsPrice +
        ", createTime=" + createTime +
        ", dispatchStatus=" + dispatchStatus +
        ", purchaseStatus=" + purchaseStatus +
        ", deleted=" + deleted +
        "}";
    }
}
