package com.yangtu.nearbyshop.admin.job;

import com.yangtu.nearbyshop.db.service.NearbyshopCouponService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import com.yangtu.nearbyshop.db.service.NearbyshopCouponUserService;
import com.yangtu.nearbyshop.db.domain.*;
import com.yangtu.nearbyshop.db.util.CouponConstant;
import com.yangtu.nearbyshop.db.util.CouponUserConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * 检测优惠券过期情况
 */
@Component
public class CouponJob {
    private final Log logger = LogFactory.getLog(CouponJob.class);

    @Autowired
    private NearbyshopCouponService couponService;
    @Autowired
    private NearbyshopCouponUserService couponUserService;

    /**
     * 每隔一个小时检查
     */
    @Scheduled(fixedDelay = 60 * 60 * 1000)
    public void checkCouponExpired() {
        logger.info("系统开启任务检查优惠券是否已经过期");

        List<NearbyshopCoupon> couponList = couponService.queryExpired();
        for(NearbyshopCoupon coupon : couponList){
            coupon.setStatus(CouponConstant.STATUS_EXPIRED);
            couponService.updateById(coupon);
        }

        List<NearbyshopCouponUser> couponUserList = couponUserService.queryExpired();
        for(NearbyshopCouponUser couponUser : couponUserList){
            couponUser.setStatus(CouponUserConstant.STATUS_EXPIRED);
            couponUserService.update(couponUser);
        }
    }

}
