package com.yangtu.nearbyshop.admin.job;

import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.db.domain.NearbyshopBdSettle;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercRebate;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercRebateSettle;
import com.yangtu.nearbyshop.db.domain.NearbyshopMercSettle;
import com.yangtu.nearbyshop.db.service.itf.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * 门店日统计
 */
@Component
public class MercCountJob {
    private final Log logger = LogFactory.getLog(MercCountJob.class);

    @Autowired
    private INearbyshopBdSettleService settleService;

    @Autowired
    private INearbyshopMercRebateService rebateService;

    @Autowired
    private INearbyshopMercRebateSettleService rebateSettleService;

    /**
     * 统计门店日返利额
     * <p>
     * 每天的23点执行，对当天返利数据进行统计
     */
    @Scheduled(cron = "0 0 23 * * ?")
    //@Scheduled(fixedDelay = 5 * 60 * 1000)
    public void countMercDailyRebate() {
        logger.info("系统开启任务=统计当天团长返利数据");
        String startTime = null;
        String endTime = null;
        String date = getToday();
        if(!StringUtils.isEmpty(date)){
            startTime = date + " 00:00:00";
            endTime = date + " 23:59:59";
        }
        Page<NearbyshopMercRebate> pageVo = new Page<>();
        pageVo.setCurrent(1);
        pageVo.setSize(10000);
        IPage<NearbyshopMercRebateSettle> pList = rebateService.getDailyRebate(pageVo,
                null,null,startTime,endTime);
        for(NearbyshopMercRebateSettle rebate:pList.getRecords()){
            rebate.setAddTime(new Date());
        }
        rebateSettleService.saveBatch(pList.getRecords());
        logger.info("结束：系统结束任务=统计当天团长返利数据。dataSize=" + pList.getRecords().size());
    }

    /**
     * 获取过去第几天的日期
     *
     * @param past
     * @return
     */
    public static String getPastDate(int past) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_YEAR, calendar.get(Calendar.DAY_OF_YEAR) - past);
        Date today = calendar.getTime();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String result = format.format(today);
        return result;
    }

    /**
     * 获取过去第几天的日期
     *
     * @return
     */
    public static String getToday() {
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String result = format.format(today);
        return result;
    }

}
