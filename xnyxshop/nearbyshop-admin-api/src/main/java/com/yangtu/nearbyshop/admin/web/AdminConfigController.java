package com.yangtu.nearbyshop.admin.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.yangtu.nearbyshop.db.domain.NearbyshopSetting;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopSettingService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.yangtu.nearbyshop.admin.annotation.RequiresPermissionsDesc;
import com.yangtu.nearbyshop.core.system.SystemConfig;
import com.yangtu.nearbyshop.core.util.JacksonUtil;
import com.yangtu.nearbyshop.core.util.ResponseUtil;
import com.yangtu.nearbyshop.db.service.NearbyshopSystemConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Map;

@RestController
@RequestMapping("/admin/config")
@Validated
public class AdminConfigController {
    private final Log logger = LogFactory.getLog(AdminConfigController.class);

    @Autowired
    private NearbyshopSystemConfigService systemConfigService;

    @Autowired
    private INearbyshopSettingService settingService;

    @RequiresPermissions("admin:config:mall:list")
    @RequiresPermissionsDesc(menu={"配置管理" , "商场配置"}, button="详情")
    @GetMapping("/mall")
    public Object listMall() {
        Map<String, String> data = systemConfigService.listMail();
        return ResponseUtil.ok(data);
    }

    @RequiresPermissions("admin:config:mall:updateConfigs")
    @RequiresPermissionsDesc(menu={"配置管理" , "商场配置"}, button="编辑")
    @PostMapping("/mall")
    public Object updateMall(@RequestBody String body ) {
        Map<String, String> data = JacksonUtil.toMap(body);
        systemConfigService.updateConfig(data);
        SystemConfig.updateConfigs(data);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:config:express:list")
    @RequiresPermissionsDesc(menu={"配置管理" , "运费配置"}, button="详情")
    @GetMapping("/express")
    public Object listExpress() {
        Map<String, String> data = systemConfigService.listExpress();
        return ResponseUtil.ok(data);
    }

    @RequiresPermissions("admin:config:express:updateConfigs")
    @RequiresPermissionsDesc(menu={"配置管理" , "运费配置"}, button="编辑")
    @PostMapping("/express")
    public Object updateExpress(@RequestBody String body) {
        Map<String, String> data = JacksonUtil.toMap(body);
        systemConfigService.updateConfig(data);
        SystemConfig.updateConfigs(data);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:config:order:list")
    @RequiresPermissionsDesc(menu={"配置管理" , "订单配置"}, button="详情")
    @GetMapping("/order")
    public Object lisOrder() {
        Map<String, String> data = systemConfigService.listOrder();
        return ResponseUtil.ok(data);
    }

    @RequiresPermissions("admin:config:order:updateConfigs")
    @RequiresPermissionsDesc(menu={"配置管理" , "订单配置"}, button="编辑")
    @PostMapping("/order")
    public Object updateOrder(@RequestBody String body) {
        Map<String, String> data = JacksonUtil.toMap(body);
        systemConfigService.updateConfig(data);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:config:wx:list")
    @RequiresPermissionsDesc(menu={"配置管理" , "小程序配置"}, button="详情")
    @GetMapping("/wx")
    public Object listWx() {
        Map<String, String> data = systemConfigService.listWx();
        return ResponseUtil.ok(data);
    }

    @RequiresPermissions("admin:config:wx:updateConfigs")
    @RequiresPermissionsDesc(menu={"配置管理" , "小程序配置"}, button="编辑")
    @PostMapping("/wx")
    public Object updateWx(@RequestBody String body) {
        Map<String, String> data = JacksonUtil.toMap(body);
        systemConfigService.updateConfig(data);
        SystemConfig.updateConfigs(data);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:config:merc:list")
    @RequiresPermissionsDesc(menu={"团长管理" , "团长分润配置"}, button="详情")
    @GetMapping("/merc")
    public Object listMerc() {
        Map<String, String> data = systemConfigService.listMerc();
        return ResponseUtil.ok(data);
    }

    @RequiresPermissions("admin:config:merc:updateConfigs")
    @RequiresPermissionsDesc(menu={"团长管理" , "团长分润配置"}, button="编辑")
    @PostMapping("/merc")
    public Object updateMerc(@RequestBody String body) {
        Map<String, String> data = JacksonUtil.toMap(body);
        systemConfigService.updateConfig(data);
        SystemConfig.updateConfigs(data);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:setting:mercGuide:update")
    @RequiresPermissionsDesc(menu={"配置管理" , "团长配置"}, button="入团说明")
    @PostMapping("/mercGuideUpdate")
    public Object mercGuide(@RequestBody NearbyshopSetting body ) {
        NearbyshopSetting setting = settingService.getOne(new QueryWrapper<NearbyshopSetting>()
            .eq("setting_key",body.getSettingKey()));
        if(null == setting){
            setting = new NearbyshopSetting();
            setting.setSettingKey(body.getSettingKey());
            setting.setSettingValue(body.getSettingValue());
            settingService.save(setting);
        }else {
            setting.setSettingValue(body.getSettingValue());
            settingService.updateById(setting);
        }
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:setting:mercGuide:detail")
    @RequiresPermissionsDesc(menu={"配置管理" , "团长配置"}, button="获取入团说明")
    @GetMapping("/mercGuideDetail")
    public Object mercGuideDetail(@NotNull String settingKey) {
        NearbyshopSetting setting = settingService.getOne(new QueryWrapper<NearbyshopSetting>()
                .eq("setting_key",settingKey));
        if(null == setting){
            setting = new NearbyshopSetting();
            setting.setSettingKey(settingKey);
            setting.setSettingValue("暂无规则");
            settingService.save(setting);
        }
        return ResponseUtil.ok(setting);
    }
}
