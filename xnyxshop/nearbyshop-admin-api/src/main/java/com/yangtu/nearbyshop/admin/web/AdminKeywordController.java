package com.yangtu.nearbyshop.admin.web;

import com.github.pagehelper.PageInfo;
import com.yangtu.nearbyshop.admin.annotation.RequiresPermissionsDesc;
import com.yangtu.nearbyshop.core.util.ResponseUtil;
import com.yangtu.nearbyshop.core.validator.Order;
import com.yangtu.nearbyshop.core.validator.Sort;
import com.yangtu.nearbyshop.db.service.NearbyshopKeywordService;
import com.yangtu.nearbyshop.db.domain.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/keyword")
@Validated
public class AdminKeywordController {
    private final Log logger = LogFactory.getLog(AdminKeywordController.class);

    @Autowired
    private NearbyshopKeywordService keywordService;

    @RequiresPermissions("admin:keyword:list")
    @RequiresPermissionsDesc(menu={"配置管理" , "关键词"}, button="查询")
    @GetMapping("/list")
    public Object list(String keyword, String url,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        List<NearbyshopKeyword> brandList = keywordService.querySelective(keyword, url, page, limit, sort, order);
        long total = PageInfo.of(brandList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", brandList);

        return ResponseUtil.ok(data);
    }

    private Object validate(NearbyshopKeyword keywords) {
        String keyword = keywords.getKeyword();
        if (StringUtils.isEmpty(keyword)) {
            return ResponseUtil.badArgument();
        }
        String url = keywords.getUrl();
        if (StringUtils.isEmpty(url)) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

    @RequiresPermissions("admin:keyword:create")
    @RequiresPermissionsDesc(menu={"配置管理" , "关键词"}, button="添加")
    @PostMapping("/create")
    public Object create(@RequestBody NearbyshopKeyword keywords) {
        Object error = validate(keywords);
        if (error != null) {
            return error;
        }
        keywordService.add(keywords);
        return ResponseUtil.ok(keywords);
    }

    @RequiresPermissions("admin:keyword:read")
    @RequiresPermissionsDesc(menu={"配置管理" , "关键词"}, button="详情")
    @GetMapping("/read")
    public Object read(@NotNull Integer id) {
        NearbyshopKeyword brand = keywordService.findById(id);
        return ResponseUtil.ok(brand);
    }

    @RequiresPermissions("admin:keyword:update")
    @RequiresPermissionsDesc(menu={"配置管理" , "关键词"}, button="编辑")
    @PostMapping("/update")
    public Object update(@RequestBody NearbyshopKeyword keywords) {
        Object error = validate(keywords);
        if (error != null) {
            return error;
        }
        if (keywordService.updateById(keywords) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok(keywords);
    }

    @RequiresPermissions("admin:keyword:delete")
    @RequiresPermissionsDesc(menu={"配置管理" , "关键词"}, button="删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody NearbyshopKeyword keyword) {
        Integer id = keyword.getId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }
        keywordService.deleteById(id);
        return ResponseUtil.ok();
    }

}
