package com.yangtu.nearbyshop.admin.dao;

import com.yangtu.nearbyshop.db.domain.NearbyshopGoods;
import com.yangtu.nearbyshop.db.domain.NearbyshopGoodsAttribute;
import com.yangtu.nearbyshop.db.domain.NearbyshopGoodsProduct;
import com.yangtu.nearbyshop.db.domain.NearbyshopGoodsSpecification;

public class GoodsAllinone {
    NearbyshopGoods goods;
    NearbyshopGoodsSpecification[] specifications;
    NearbyshopGoodsAttribute[] attributes;
    // 这里采用 Product 再转换到 NearbyshopGoodsProduct
    NearbyshopGoodsProduct[] products;

    public NearbyshopGoods getGoods() {
        return goods;
    }

    public void setGoods(NearbyshopGoods goods) {
        this.goods = goods;
    }

    public NearbyshopGoodsProduct[] getProducts() {
        return products;
    }

    public void setProducts(NearbyshopGoodsProduct[] products) {
        this.products = products;
    }

    public NearbyshopGoodsSpecification[] getSpecifications() {
        return specifications;
    }

    public void setSpecifications(NearbyshopGoodsSpecification[] specifications) {
        this.specifications = specifications;
    }

    public NearbyshopGoodsAttribute[] getAttributes() {
        return attributes;
    }

    public void setAttributes(NearbyshopGoodsAttribute[] attributes) {
        this.attributes = attributes;
    }

}
