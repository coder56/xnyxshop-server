package com.yangtu.nearbyshop.admin.service;

import com.yangtu.nearbyshop.core.util.ResponseUtil;
import com.yangtu.nearbyshop.db.domain.*;
import com.yangtu.nearbyshop.db.service.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static com.yangtu.nearbyshop.admin.util.AdminResponseCode.*;

@Service
public class AdminCategoryService {
    private final Log logger = LogFactory.getLog(AdminCategoryService.class);

    @Autowired
    private NearbyshopCategoryService categoryService;


    @Transactional
    public Object push(NearbyshopCategory category) {
        Integer id = category.getId();
        if (id == null || category.getPushStatus() == null) {
            return ResponseUtil.badArgument();
        }
        //如果是取消推荐，则推荐排序归0
        if(category.getPushStatus().equals(0)){
            category.setPushSort(0);
        }else {
            List<NearbyshopCategory> cateList =categoryService.queryAllByPush();
            int max = 0;
            for(NearbyshopCategory cate : cateList){
                if(cate.getPushSort() > max){
                    max = cate.getPushSort();
                }
            }
            max += 1;
            category.setPushSort(max);
        }
        categoryService.updateById(category);
        return ResponseUtil.ok();
    }

    @Transactional
    public Object sortUp(NearbyshopCategory category) {
        Integer id = category.getId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }
        List<NearbyshopCategory> cateList =categoryService.queryAllByPush();
        NearbyshopCategory nowGoods = categoryService.findById(id);
        NearbyshopCategory lastGoods = null;
        for(NearbyshopCategory cate : cateList){
            if(cate.getPushSort() == nowGoods.getPushSort()){
                break;
            }else {
                lastGoods = cate;
            }
        }
        if(lastGoods != null){
            int temp;
            temp = nowGoods.getPushSort();
            nowGoods.setPushSort(lastGoods.getPushSort());
            lastGoods.setPushSort(temp);
        }else {
            return ResponseUtil.fail(SORT_OUT_RANGE, "无法移动");
        }

        category.setPushSort(nowGoods.getPushSort());
        categoryService.updateById(category);

        NearbyshopCategory updateGoods = new NearbyshopCategory();
        updateGoods.setId(lastGoods.getId());
        updateGoods.setPushSort(lastGoods.getPushSort());
        categoryService.updateById(updateGoods);
        return ResponseUtil.ok();
    }

    @Transactional
    public Object sortDown(NearbyshopCategory category) {
        Integer id = category.getId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }
        List<NearbyshopCategory> goodsList =categoryService.queryAllByPush();
        NearbyshopCategory nowGoods = categoryService.findById(id);
        NearbyshopCategory nextGoods = null;
        for(NearbyshopCategory shopGoods : goodsList){
            if(shopGoods.getPushSort() > nowGoods.getPushSort()){
                nextGoods = shopGoods;
                break;
            }
        }
        if(nextGoods != null){
            int temp;
            temp = nowGoods.getPushSort();
            nowGoods.setPushSort(nextGoods.getPushSort());
            nextGoods.setPushSort(temp);
        }else {
            return ResponseUtil.fail(SORT_OUT_RANGE, "无法移动");
        }

        category.setPushSort(nowGoods.getPushSort());
        categoryService.updateById(category);

        NearbyshopCategory updateGoods = new NearbyshopCategory();
        updateGoods.setId(nextGoods.getId());
        updateGoods.setPushSort(nextGoods.getPushSort());
        categoryService.updateById(updateGoods);
        return ResponseUtil.ok();
    }
}
