package com.yangtu.nearbyshop.admin.web;

import com.github.pagehelper.PageInfo;
import com.yangtu.nearbyshop.admin.annotation.RequiresPermissionsDesc;
import com.yangtu.nearbyshop.admin.dao.AdminCategoryVo;
import com.yangtu.nearbyshop.admin.service.AdminCategoryService;
import com.yangtu.nearbyshop.core.util.ResponseUtil;
import com.yangtu.nearbyshop.core.validator.Sort;
import com.yangtu.nearbyshop.db.service.NearbyshopCategoryService;
import com.yangtu.nearbyshop.db.domain.*;
import com.yangtu.nearbyshop.db.service.NearbyshopGoodsService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.yangtu.nearbyshop.core.validator.Order;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/category")
@Validated
public class AdminCategoryController {
    private final Log logger = LogFactory.getLog(AdminCategoryController.class);

    @Autowired
    private NearbyshopCategoryService categoryService;

    @Autowired
    private NearbyshopGoodsService goodsService;

    @Autowired
    private AdminCategoryService adminCategoryService;

    /**
     * @param type  1:一级分类   2:二级分类   3：推荐分类
     * @return
     */
    @RequiresPermissions("admin:category:list")
    @RequiresPermissionsDesc(menu={"商品管理" , "商品分类"}, button="查询")
    @GetMapping("/list")
    public Object list(String id, String name,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order,
                       @RequestParam(defaultValue = "") String type) {
        List<NearbyshopCategory> collectList = categoryService.querySelective(id, name, page, limit, sort, order,type);
        long total = PageInfo.of(collectList).getTotal();

        List<AdminCategoryVo> voList = new ArrayList<>();
        for(NearbyshopCategory category : collectList){
            AdminCategoryVo adminCategoryVo = new AdminCategoryVo();
            BeanUtils.copyProperties(category,adminCategoryVo);
            if(category.getLevel().equals("L1")){
                adminCategoryVo.setGoodsCount(goodsService.countByCond(1,category.getId()));
            }else if(category.getLevel().equals("L2")){
                adminCategoryVo.setPname(categoryService.findById(category.getPid()).getName());
                adminCategoryVo.setGoodsCount(goodsService.countByCond(2,category.getId()));
            }
            voList.add(adminCategoryVo);
        }

        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", voList);

        return ResponseUtil.ok(data);
    }

    private Object validate(NearbyshopCategory category) {
        String name = category.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }

        String level = category.getLevel();
        if (StringUtils.isEmpty(level)) {
            return ResponseUtil.badArgument();
        }
        if (!level.equals("L1") && !level.equals("L2")) {
            return ResponseUtil.badArgumentValue();
        }

        Integer pid = category.getPid();
        if (level.equals("L2") && (pid == null)) {
            return ResponseUtil.badArgument();
        }

        return null;
    }

    @RequiresPermissions("admin:category:create")
    @RequiresPermissionsDesc(menu={"商品管理" , "商品分类"}, button="添加")
    @PostMapping("/create")
    public Object create(@RequestBody NearbyshopCategory category) {
        Object error = validate(category);
        if (error != null) {
            return error;
        }
        categoryService.add(category);
        return ResponseUtil.ok(category);
    }

    @RequiresPermissions("admin:category:read")
    @RequiresPermissionsDesc(menu={"商品管理" , "商品分类"}, button="详情")
    @GetMapping("/read")
    public Object read(@NotNull Integer id) {
        NearbyshopCategory category = categoryService.findById(id);
        return ResponseUtil.ok(category);
    }

    @RequiresPermissions("admin:category:update")
    @RequiresPermissionsDesc(menu={"商品管理" , "商品分类"}, button="编辑")
    @PostMapping("/update")
    public Object update(@RequestBody NearbyshopCategory category) {
        Object error = validate(category);
        if (error != null) {
            return error;
        }

        if (categoryService.updateById(category) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:category:delete")
    @RequiresPermissionsDesc(menu={"商品管理" , "商品分类"}, button="删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody NearbyshopCategory category) {
        Integer id = category.getId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }
        categoryService.deleteById(id);
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:category:list")
    @GetMapping("/l1")
    public Object catL1() {
        // 所有一级分类目录
        List<NearbyshopCategory> l1CatList = categoryService.queryL1();
        List<Map<String, Object>> data = new ArrayList<>(l1CatList.size());
        for (NearbyshopCategory category : l1CatList) {
            Map<String, Object> d = new HashMap<>(2);
            d.put("value", category.getId());
            d.put("label", category.getName());
            data.add(d);
        }
        return ResponseUtil.ok(data);
    }

    /**
     * 推商品到首页/取消推荐
     *
     * @param nearbyshopCategory  id: 分类id。  pushStatus:1：首页  0：非首页
     * @return
     */
    @RequiresPermissions("admin:category:push")
    @RequiresPermissionsDesc(menu = {"商品管理" , "商品分类"}, button="推到首页")
    @PostMapping("/push")
    public Object push(@RequestBody NearbyshopCategory nearbyshopCategory) {
        return adminCategoryService.push(nearbyshopCategory);
    }

    /**
     * 分类上移
     *
     * @param nearbyshopCategory  id: 分类id。
     * @return
     */
    @RequiresPermissions("admin:category:sortUp")
    @RequiresPermissionsDesc(menu = {"商品管理" , "商品分类"}, button="上移")
    @PostMapping("/sortUp")
    public Object sortUp(@RequestBody NearbyshopCategory nearbyshopCategory) {
        return adminCategoryService.sortUp(nearbyshopCategory);
    }

    /**
     * 分类下移
     *
     * @param nearbyshopCategory  id: 分类id。
     * @return
     */
    @RequiresPermissions("admin:category:sortDown")
    @RequiresPermissionsDesc(menu = {"商品管理" , "商品分类"}, button="下移")
    @PostMapping("/sortDown")
    public Object sortDown(@RequestBody NearbyshopCategory nearbyshopCategory) {
        return adminCategoryService.sortDown(nearbyshopCategory);
    }
}
