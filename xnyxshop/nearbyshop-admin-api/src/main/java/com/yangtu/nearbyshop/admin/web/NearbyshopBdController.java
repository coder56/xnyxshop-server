package com.yangtu.nearbyshop.admin.web;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yangtu.nearbyshop.admin.annotation.RequiresPermissionsDesc;
import com.yangtu.nearbyshop.admin.util.AdminResponseCode;
import com.yangtu.nearbyshop.core.util.ResponseUtil;
import com.yangtu.nearbyshop.db.domain.NearbyshopBd;
import com.yangtu.nearbyshop.db.domain.NearbyshopBdMerc;
import com.yangtu.nearbyshop.db.domain.NearbyshopBdSettle;
import com.yangtu.nearbyshop.db.domain.NearbyshopMerc;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopBdMercService;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopBdService;
import com.yangtu.nearbyshop.db.vo.SaleCountVo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * BD表 前端控制器
 * </p>
 *
 * @author wanglei
 * @since 2019-09-15
 */
@RestController
@RequestMapping("/admin/bd")
@Validated
public class NearbyshopBdController {
    private final Log logger = LogFactory.getLog(NearbyshopBdController.class);

    @Autowired
    INearbyshopBdService bdService;

    @Autowired
    INearbyshopBdMercService bdMercService;

    /**
     * @apiDefine bd BD相关接口
     *
     */

    /**
     * @api {POST} /admin/bd/create BD新增
     * @apiName bd create
     * @apiGroup bd
     * @apiVersion 1.0.0
     * @apiDescription 接口负责人-王雷
     *
     * @apiParam {String} bdName 名称
     * @apiParam {String} bdPhone 手机
     * @apiParam {String} bdAccount 账号
     *
     * @apiSuccess {int} code 结果码 0:正常返回,-1服务异常,1业务异常
     * @apiSuccess {String} message 前端提示消息
     * @apiSuccess {Object} value 结果集
     * @apiSuccess {String} bizCode 业务异常码
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *     "errno": 0,
     *     "errmsg": "成功"
     * }
     */
    @RequiresPermissions("admin:bd:create")
    @RequiresPermissionsDesc(menu = {"BD管理", "BD统计"}, button = "新增BD")
    @PostMapping("/create")
    public Object saleCountBD(@RequestBody NearbyshopBd bd) {
        if (bd == null || StringUtils.isEmpty(bd.getBdName()) || StringUtils.isEmpty(bd.getBdPhone())
                || StringUtils.isEmpty(bd.getBdAccount())) {
            return ResponseUtil.badArgument();
        }

        int count = bdService.count(new QueryWrapper<NearbyshopBd>()
            .eq("bd_name",bd.getBdName())
            .eq("deleted",0));
        if (count > 0) {
            return ResponseUtil.fail("BD名称已经被使用");
        }
        bd.setAddTime(new Date());
        bd.setBdNo("BD" + System.currentTimeMillis());
        bdService.save(bd);
        return ResponseUtil.ok();
    }

    /**
     * @api {POST} /admin/bd/update BD编辑
     * @apiName bd update
     * @apiGroup bd
     * @apiVersion 1.0.0
     * @apiDescription 接口负责人-王雷
     *
     * @apiParam {String} bdNo bd编号
     * @apiParam {String} [bdName] 名称
     * @apiParam {String} [bdPhone] 手机
     * @apiParam {String} [bdAccount] 账号
     *
     * @apiSuccess {int} code 结果码 0:正常返回,-1服务异常,1业务异常
     * @apiSuccess {String} message 前端提示消息
     * @apiSuccess {Object} value 结果集
     * @apiSuccess {String} bizCode 业务异常码
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *     "errno": 0,
     *     "errmsg": "成功"
     * }
     */
    @RequiresPermissions("admin:bd:update")
    @RequiresPermissionsDesc(menu = {"BD管理", "BD统计"}, button = "编辑BD")
    @PostMapping("/update")
    public Object update(@RequestBody NearbyshopBd bd) {
        if (bd == null || StringUtils.isEmpty(bd.getBdNo())) {
            return ResponseUtil.badArgument();
        }
        NearbyshopBd nearbyshopBd = bdService.getOne(new QueryWrapper<NearbyshopBd>()
                .eq("bd_no",bd.getBdNo())
                .eq("deleted",0));

        if (nearbyshopBd == null) {
            return ResponseUtil.fail("BD不存在");
        }

        if(!StringUtils.isEmpty(bd.getBdName())){
            int count = bdService.count(new QueryWrapper<NearbyshopBd>()
                    .eq("bd_name",bd.getBdName())
                    .ne("bd_no",bd.getBdNo())
                    .eq("deleted",0));
            if (count > 0) {
                return ResponseUtil.fail("BD名称已经被使用");
            }
        }

        bd.setId(nearbyshopBd.getId());
        bdService.updateById(bd);
        return ResponseUtil.ok();
    }

    /**
     * @api {POST} /admin/bd/delete BD删除
     * @apiName bd delete
     * @apiGroup bd
     * @apiVersion 1.0.0
     * @apiDescription 接口负责人-王雷
     *
     * @apiParam {String} bdNo bd编号
     *
     * @apiSuccess {int} code 结果码 0:正常返回,-1服务异常,1业务异常
     * @apiSuccess {String} message 前端提示消息
     * @apiSuccess {Object} value 结果集
     * @apiSuccess {String} bizCode 业务异常码
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *     "errno": 0,
     *     "errmsg": "成功"
     * }
     */
    @RequiresPermissions("admin:bd:delete")
    @RequiresPermissionsDesc(menu = {"BD管理", "BD统计"}, button = "删除BD")
    @PostMapping("/delete")
    public Object delete(@RequestBody NearbyshopBd bd) {
        if (bd == null || StringUtils.isEmpty(bd.getBdNo())) {
            return ResponseUtil.badArgument();
        }
        NearbyshopBd nearbyshopBd = bdService.getOne(new QueryWrapper<NearbyshopBd>()
                .eq("bd_no",bd.getBdNo())
                .eq("deleted",0));

        if (nearbyshopBd == null) {
            return ResponseUtil.fail("BD不存在");
        }

        int count = bdMercService.count(new QueryWrapper<NearbyshopBdMerc>()
            .eq("bd_no",bd.getBdNo()));

        if (count > 0) {
            return ResponseUtil.fail("BD不可删除");
        }
        bdService.removeById(nearbyshopBd.getId());
        return ResponseUtil.ok();
    }

    /**
     * @api {GET} /admin/bd/list BD列表
     * @apiName bd list
     * @apiGroup bd
     * @apiVersion 1.0.0
     * @apiDescription 接口负责人-王雷
     *
     * @apiParam {String} bdName bd名称
     * @apiParam {int} page 页码
     * @apiParam {int} limit 每页数量
     *
     * @apiSuccess {int} code 结果码 0:正常返回,-1服务异常,1业务异常
     * @apiSuccess {String} message 前端提示消息
     * @apiSuccess {Object} value 结果集
     * @apiSuccess {String} bizCode 业务异常码
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *     "errno": 0,
     *     "errmsg": "成功"
     * }
     */
    @RequiresPermissions("admin:bd:list")
    @RequiresPermissionsDesc(menu = {"BD管理", "BD统计"}, button = "BD列表")
    @GetMapping("/list")
    public Object list(String bdName,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        Page<NearbyshopBd> pageVo = new Page<>();
        pageVo.setCurrent(page);
        pageVo.setSize(limit);
        IPage<NearbyshopBd> pList = bdService.getBdList(pageVo,bdName);
        Map<String, Object> data = new HashMap<>();
        data.put("total", pList.getTotal());
        data.put("items", pList.getRecords());
        return ResponseUtil.ok(data);
    }

    /**
     * @api {GET} /admin/bd/mercList BD下的店铺
     * @apiName bd mercList
     * @apiGroup bd
     * @apiVersion 1.0.0
     * @apiDescription 接口负责人-王雷
     *
     * @apiParam {String} bdNo bd编号
     * @apiParam {int} page 页码
     * @apiParam {int} limit 每页数量
     *
     * @apiSuccess {int} code 结果码 0:正常返回,-1服务异常,1业务异常
     * @apiSuccess {String} message 前端提示消息
     * @apiSuccess {Object} value 结果集
     * @apiSuccess {String} bizCode 业务异常码
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *     "errno": 0,
     *     "errmsg": "成功"
     * }
     */
    @RequiresPermissions("admin:bd:mercList")
    @RequiresPermissionsDesc(menu = {"BD管理", "BD统计"}, button = "BD下的店铺列表")
    @GetMapping("/mercList")
    public Object mercList(String bdNo,
                           @RequestParam(defaultValue = "1") Integer page,
                           @RequestParam(defaultValue = "10") Integer limit) {
        Page<NearbyshopMerc> pageVo = new Page<>();
        pageVo.setCurrent(page);
        pageVo.setSize(limit);
        IPage<NearbyshopMerc> pList = bdService.getMercListByBd(pageVo,bdNo);
        Map<String, Object> data = new HashMap<>();
        data.put("total", pList.getTotal());
        data.put("items", pList.getRecords());
        return ResponseUtil.ok(data);
    }

    /**
     * @api {GET} /admin/bd/weekSettle BD业绩列表
     * @apiName bd weekSettle
     * @apiGroup bd
     * @apiVersion 1.0.0
     * @apiDescription 接口负责人-王雷
     *
     * @apiParam {String} bdName bd名称
     * @apiParam {int} page 页码
     * @apiParam {int} limit 每页数量
     *
     * @apiSuccess {int} code 结果码 0:正常返回,-1服务异常,1业务异常
     * @apiSuccess {String} message 前端提示消息
     * @apiSuccess {Object} value 结果集
     * @apiSuccess {String} bizCode 业务异常码
     * @apiSuccessExample Success-Response:
     * HTTP/1.1 200 OK
     * {
     *     "errno": 0,
     *     "errmsg": "成功"
     * }
     */
    @RequiresPermissions("admin:bd:weekSettle")
    @RequiresPermissionsDesc(menu = {"BD管理", "BD统计"}, button = "BD业绩列表")
    @GetMapping("/weekSettle")
    public Object weekSettle(String bdName,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        Page<NearbyshopBd> pageVo = new Page<>();
        pageVo.setCurrent(page);
        pageVo.setSize(limit);
        IPage<NearbyshopBd> pList = bdService.getBdList(pageVo,bdName);
        for(NearbyshopBd bd: pList.getRecords()){
            List<NearbyshopBdSettle> settles = bdService.getBdSettleList(bd.getBdNo());
            bd.setSettleList(settles);
        }
        Map<String, Object> data = new HashMap<>();
        data.put("total", pList.getTotal());
        data.put("items", pList.getRecords());
        return ResponseUtil.ok(data);
    }
}
