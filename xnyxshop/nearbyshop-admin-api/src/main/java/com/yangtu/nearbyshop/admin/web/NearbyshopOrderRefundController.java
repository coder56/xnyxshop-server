package com.yangtu.nearbyshop.admin.web;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 退款表 前端控制器
 * </p>
 *
 * @author wanglei
 * @since 2019-09-14
 */
@RestController
@RequestMapping("/nearbyshop-order-refund")
public class NearbyshopOrderRefundController {

}
