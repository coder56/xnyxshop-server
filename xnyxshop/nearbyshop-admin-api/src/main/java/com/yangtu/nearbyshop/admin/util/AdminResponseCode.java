package com.yangtu.nearbyshop.admin.util;

public class AdminResponseCode {
    public static final Integer ADMIN_INVALID_NAME = 601;
    public static final Integer ADMIN_INVALID_PASSWORD = 602;
    public static final Integer ADMIN_NAME_EXIST = 602;
    public static final Integer ADMIN_ALTER_NOT_ALLOWED = 603;
    public static final Integer ADMIN_DELETE_NOT_ALLOWED = 604;
    public static final Integer ADMIN_INVALID_ACCOUNT = 605;
    public static final Integer GOODS_UPDATE_NOT_ALLOWED = 610;
    public static final Integer GOODS_NAME_EXIST = 611;
    public static final Integer GOODS_STOCK_NUM_NOT_ALLOWED = 612;
    public static final Integer GOODS_PRICE_SET_FAULT = 613;
    public static final Integer ORDER_CONFIRM_NOT_ALLOWED = 620;
    public static final Integer ORDER_REFUND_FAILED = 621;
    public static final Integer ORDER_REPLY_EXIST = 622;
    public static final Integer USER_INVALID_NAME = 630;
    public static final Integer USER_INVALID_PASSWORD = 631;
    public static final Integer USER_INVALID_MOBILE = 632;
    public static final Integer USER_NAME_EXIST = 633;
    public static final Integer USER_MOBILE_EXIST = 634;
    public static final Integer ROLE_NAME_EXIST = 640;
    public static final Integer ROLE_SUPER_SUPERMISSION = 641;
    public static final Integer MERC_ORG_NO_INVALID= 650;

    //上移下移，已经是极限
    public static final Integer SORT_OUT_RANGE = 701;

    //没有待锁定订单
    public static final Integer LOCK_NO_ORDER = 801;

    //不予许删除团长
    public static final Integer MERC_DELETE_INVALID = 901;

    //订单不允许退款
    public static final Integer ORDER_CANT_REFUND = 902;
}
