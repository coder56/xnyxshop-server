package com.yangtu.nearbyshop.admin.web;

import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageInfo;
import com.qcloud.cos.utils.Jackson;
import com.yangtu.nearbyshop.core.corg.until.HttpClientUtils;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopOrderRefundService;
import com.yangtu.nearbyshop.db.service.itf.INearbyshopOrderService;
import com.yangtu.nearbyshop.db.vo.CouponResult;
import com.yangtu.nearbyshop.db.vo.CouponUseVo;
import com.yangtu.nearbyshop.db.vo.SaleCountVo;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.yangtu.nearbyshop.admin.annotation.RequiresPermissionsDesc;
import com.yangtu.nearbyshop.core.util.ResponseUtil;
import com.yangtu.nearbyshop.core.validator.Order;
import com.yangtu.nearbyshop.core.validator.Sort;
import com.yangtu.nearbyshop.db.service.NearbyshopCouponService;
import com.yangtu.nearbyshop.db.service.NearbyshopCouponUserService;
import com.yangtu.nearbyshop.db.domain.*;
import com.yangtu.nearbyshop.db.util.CouponConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/coupon")
@Validated
public class AdminCouponController {
    private final Log logger = LogFactory.getLog(AdminCouponController.class);

    @Autowired
    private NearbyshopCouponService couponService;
    @Autowired
    private NearbyshopCouponUserService couponUserService;

    @Autowired
    private INearbyshopOrderService orderService;

    @Value("${nearbyshop.orgName}")
    private String orgName;

    @RequiresPermissions("admin:coupon:list")
    @RequiresPermissionsDesc(menu={"营销管理" , "优惠券管理"}, button="查询")
    @GetMapping("/list")
    public Object list(String name, Short type, Short status,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        List<NearbyshopCoupon> couponList = couponService.querySelective(name, type, status, page, limit, sort, order);
        long total = PageInfo.of(couponList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", couponList);

        return ResponseUtil.ok(data);
    }

    @RequiresPermissions("admin:coupon:listuser")
    @RequiresPermissionsDesc(menu={"营销管理" , "优惠券管理"}, button="查询用户")
    @GetMapping("/listuser")
    public Object listuser(Integer userId, Integer couponId, Short status,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        List<NearbyshopCouponUser> couponList = couponUserService.queryList(userId, couponId, status, page, limit, sort, order);
        long total = PageInfo.of(couponList).getTotal();
        Map<String, Object> data = new HashMap<>();
        data.put("total", total);
        data.put("items", couponList);

        return ResponseUtil.ok(data);
    }

    private Object validate(NearbyshopCoupon coupon) {
        String name = coupon.getName();
        if(StringUtils.isEmpty(name)){
            return ResponseUtil.badArgument();
        }
        return null;
    }

    @RequiresPermissions("admin:coupon:create")
    @RequiresPermissionsDesc(menu={"营销管理" , "优惠券管理"}, button="添加")
    @PostMapping("/create")
    public Object create(@RequestBody NearbyshopCoupon coupon) {
        Object error = validate(coupon);
        if (error != null) {
            return error;
        }

        // 如果是兑换码类型，则这里需要生存一个兑换码
        if (coupon.getType().equals(CouponConstant.TYPE_CODE)){
            String code = couponService.generateCode();
            coupon.setCode(code);
        }

        couponService.add(coupon);
        return ResponseUtil.ok(coupon);
    }

    @RequiresPermissions("admin:coupon:read")
    @RequiresPermissionsDesc(menu={"营销管理" , "优惠券管理"}, button="详情")
    @GetMapping("/read")
    public Object read(@NotNull Integer id) {
        NearbyshopCoupon coupon = couponService.findById(id);
        return ResponseUtil.ok(coupon);
    }

    @RequiresPermissions("admin:coupon:update")
    @RequiresPermissionsDesc(menu={"营销管理" , "优惠券管理"}, button="编辑")
    @PostMapping("/update")
    public Object update(@RequestBody NearbyshopCoupon coupon) {
        Object error = validate(coupon);
        if (error != null) {
            return error;
        }
        if (couponService.updateById(coupon) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok(coupon);
    }

    @RequiresPermissions("admin:coupon:delete")
    @RequiresPermissionsDesc(menu={"营销管理" , "优惠券管理"}, button="删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody NearbyshopCoupon coupon) {
        couponService.deleteById(coupon.getId());
        return ResponseUtil.ok();
    }


//    @RequiresPermissions("admin:coupon:settleList")
//    @RequiresPermissionsDesc(menu={"营销管理" , "优惠券管理"}, button="结算")
    @GetMapping("/settleList")
    public Object settleList(String startTime, String endTime, Integer couponId,String mercNo,
                           @RequestParam(defaultValue = "1") Integer page,
                           @RequestParam(defaultValue = "10") Integer limit) {
        Page<CouponUseVo> pageVo = new Page<>();
        pageVo.setCurrent(page);
        pageVo.setSize(limit);
        IPage<CouponUseVo> pList = orderService.getCouponUse(pageVo,startTime,endTime,couponId,mercNo);
        Map<String, Object> data = new HashMap<>();
        for(CouponUseVo vo: pList.getRecords()){
            vo.setOrgName(orgName);
        }
        CouponResult re1 = new CouponResult();
        CouponResult re2 = new CouponResult();
        if(orgName.equals("扬途电商")){
            String result1 = HttpClientUtils.sendGetRequest("https://wmsj.jingweitech.cn/xnyxshop/admin/coupon/settleList?limit=100000&couponId="+couponId+"&mercNo="+mercNo,null);
            String result2 = HttpClientUtils.sendGetRequest("https://shop.jingweitech.cn/xnyxshop/admin/coupon/settleList?limit=100000&couponId="+couponId+"&mercNo="+mercNo,null);
            re1 = Jackson.fromJsonString(result1,CouponResult.class);
            re2 = Jackson.fromJsonString(result2,CouponResult.class);
        }
        data.put("total", pList.getTotal()+re1.getTotal()+re2.getTotal());
        pList.getRecords().addAll(re1.getItems());
        pList.getRecords().addAll(re2.getItems());
        data.put("items", pList.getRecords());

        return ResponseUtil.ok(data);
    }

    @PostMapping("/settle")
    public Object settle(@RequestBody NearbyshopCouponUser coupon) {
        coupon.setSettleStatus(1);
        if (couponUserService.update(coupon) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok();
    }

    @GetMapping("/testProp")
    public Object testProp() {
        return ResponseUtil.ok(orgName);
    }
}
