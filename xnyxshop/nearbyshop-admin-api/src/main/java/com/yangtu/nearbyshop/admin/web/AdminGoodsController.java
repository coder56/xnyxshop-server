package com.yangtu.nearbyshop.admin.web;

import com.yangtu.nearbyshop.admin.annotation.RequiresPermissionsDesc;
import com.yangtu.nearbyshop.admin.dao.GoodsAllinone;
import com.yangtu.nearbyshop.admin.service.AdminGoodsService;
import com.yangtu.nearbyshop.core.util.ResponseUtil;
import com.yangtu.nearbyshop.core.validator.Sort;
import com.yangtu.nearbyshop.db.domain.NearbyshopGoods;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import com.yangtu.nearbyshop.core.validator.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/admin/goods")
@Validated
public class AdminGoodsController {
    private final Log logger = LogFactory.getLog(AdminGoodsController.class);

    @Autowired
    private AdminGoodsService adminGoodsService;

    /**
     * 查询商品
     *
     * @param goodsSn
     * @param name
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @param type   1:在售的   2:推荐的   3：下架的
     * @return
     */
    @RequiresPermissions("admin:goods:list")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品档案"}, button = "查询")
    @GetMapping("/list")
    public Object list(String goodsSn, String name,Integer categoryId,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order,
                       @RequestParam(defaultValue = "type") String type) {
        return adminGoodsService.list(goodsSn, name, categoryId,page, limit, sort, order,type);
    }

    @GetMapping("/catAndBrand")
    public Object list2() {
        return adminGoodsService.list2();
    }

    /**
     * 编辑商品
     *
     * @param goodsAllinone
     * @return
     */
    @RequiresPermissions("admin:goods:update")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品档案"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody GoodsAllinone goodsAllinone) {
        return adminGoodsService.update(goodsAllinone);
    }

    /**
     * 推商品到首页/取消推荐
     *
     * @param nearbyshopGoods  id: 商品id。  pushStatus:1：首页  0：非首页
     * @return
     */
    @RequiresPermissions("admin:goods:push")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品档案"}, button = "推到首页")
    @PostMapping("/push")
    public Object push(@RequestBody NearbyshopGoods nearbyshopGoods) {
        return adminGoodsService.push(nearbyshopGoods);
    }

    /**
     * 商品上移
     *
     * @param nearbyshopGoods  id: 商品id。
     * @return
     */
    @RequiresPermissions("admin:goods:sortUp")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品档案"}, button = "上移")
    @PostMapping("/sortUp")
    public Object sortUp(@RequestBody NearbyshopGoods nearbyshopGoods) {
        return adminGoodsService.sortUp(nearbyshopGoods);
    }

    /**
     * 商品下移
     *
     * @param nearbyshopGoods  id: 商品id。
     * @return
     */
    @RequiresPermissions("admin:goods:sortDown")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品档案"}, button = "下移")
    @PostMapping("/sortDown")
    public Object sortDown(@RequestBody NearbyshopGoods nearbyshopGoods) {
        return adminGoodsService.sortDown(nearbyshopGoods);
    }

    /**
     * 修改商品库存
     *
     * @param id
     * @param goodsNum
     * @return
     */
    @RequiresPermissions("admin:goods:updatenum")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品档案"}, button = "库存修改")
    @GetMapping("/updatenum")
    public Object updategoodsnum(@NotNull Integer id, Integer goodsNum) {
        return adminGoodsService.updateGoodsNum(id, goodsNum);
    }

    /**
     * 删除商品
     *
     * @param goods
     * @return
     */
    @RequiresPermissions("admin:goods:delete")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品删除"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody NearbyshopGoods goods) {
        return adminGoodsService.delete(goods);
    }

    /**
     * 商品上架
     *
     * @param goods
     * @return
     */
    @RequiresPermissions("admin:goods:onSale")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品上下架"}, button = "上架")
    @PostMapping("/onSale")
    public Object onSale(@RequestBody NearbyshopGoods goods) {
        NearbyshopGoods update = new NearbyshopGoods();
        update.setId(goods.getId());
        update.setIsOnSale(true);
        adminGoodsService.updateGoods(update);
        return ResponseUtil.ok();
    }

    /**
     * 商品下架
     *
     * @param goods
     * @return
     */
    @RequiresPermissions("admin:goods:offSale")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品上下架"}, button = "下架")
    @PostMapping("/offSale")
    public Object offSale(@RequestBody NearbyshopGoods goods) {
        NearbyshopGoods update = new NearbyshopGoods();
        update.setId(goods.getId());
        update.setIsOnSale(false);
        adminGoodsService.updateGoods(update);
        return ResponseUtil.ok();
    }

    /**
     * 添加商品
     *
     * @param goodsAllinone
     * @return
     */
    @RequiresPermissions("admin:goods:create")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品上架"}, button = "上架")
    @PostMapping("/create")
    public Object create(@RequestBody GoodsAllinone goodsAllinone) {
        return adminGoodsService.create(goodsAllinone);
    }

    /**
     * 商品详情
     *
     * @param id
     * @return
     */
    @RequiresPermissions("admin:goods:read")
    @RequiresPermissionsDesc(menu = {"商品管理", "商品档案"}, button = "详情")
    @GetMapping("/detail")
    public Object detail(@NotNull Integer id) {
        return adminGoodsService.detail(id);

    }

}
