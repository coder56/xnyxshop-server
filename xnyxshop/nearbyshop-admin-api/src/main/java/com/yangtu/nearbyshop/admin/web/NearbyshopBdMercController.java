package com.yangtu.nearbyshop.admin.web;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * BD门店表 前端控制器
 * </p>
 *
 * @author wanglei
 * @since 2019-09-15
 */
@RestController
@RequestMapping("/nearbyshop-bd-merc")
public class NearbyshopBdMercController {

}

