package com.yangtu.nearbyshop.admin.dao;

public class MercInf {
    String id;
    String username;
    String mercName;
    String mercAddr;
    String longitude;
    String latitude;
    String provinceId;
    String cityId;
    String areaId;
    String realName;
    String cardNo;
    String bankNm;
    String idNo;
    String bankCd;
    String orgNo;
    String mobile;
    String referMerc;
    String bdNo;

    public String getReferMerc() {
        return referMerc;
    }

    public void setReferMerc(String referMerc) {
        this.referMerc = referMerc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getMercName() {
        return mercName;
    }

    public void setMercName(String mercName) {
        this.mercName = mercName;
    }

    public String getMercAddr() {
        return mercAddr;
    }

    public void setMercAddr(String mercAddr) {
        this.mercAddr = mercAddr;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getRealName() {
        return realName;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String getCardNo() {
        return cardNo;
    }

    public void setCardNo(String cardNo) {
        this.cardNo = cardNo;
    }

    public String getBankNm() {
        return bankNm;
    }

    public void setBankNm(String bankNm) {
        this.bankNm = bankNm;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getBankCd() {
        return bankCd;
    }

    public void setBankCd(String bankCd) {
        this.bankCd = bankCd;
    }

    public String getOrgNo() {
        return orgNo;
    }

    public void setOrgNo(String orgNo) {
        this.orgNo = orgNo;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getBdNo() {
        return bdNo;
    }

    public void setBdNo(String bdNo) {
        this.bdNo = bdNo;
    }

    @Override
    public String toString() {
        return "MercInf{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", mercName='" + mercName + '\'' +
                ", mercAddr='" + mercAddr + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", provinceId='" + provinceId + '\'' +
                ", cityId='" + cityId + '\'' +
                ", areaId='" + areaId + '\'' +
                ", realName='" + realName + '\'' +
                ", cardNo='" + cardNo + '\'' +
                ", bankNm='" + bankNm + '\'' +
                ", idNo='" + idNo + '\'' +
                ", bankCd='" + bankCd + '\'' +
                ", orgNo='" + orgNo + '\'' +
                ", mobile='" + mobile + '\'' +
                ", referMerc='" + referMerc + '\'' +
                '}';
    }
}
