package com.yangtu.nearbyshop.admin;

import com.yangtu.nearbyshop.db.domain.NearbyshopGoods;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.yangtu.nearbyshop.core.qcode.QCodeService;
import com.yangtu.nearbyshop.db.service.NearbyshopGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class CreateShareImageTest {
    @Autowired
    QCodeService qCodeService;
    @Autowired
    NearbyshopGoodsService nearbyshopGoodsService;

    @Test
    public void test() {
        NearbyshopGoods good = nearbyshopGoodsService.findById(1155015);
        String url = qCodeService.createGoodShareImage(good.getId().toString(), good.getPicUrl(), good.getName());
        System.out.println(url);
    }
}
