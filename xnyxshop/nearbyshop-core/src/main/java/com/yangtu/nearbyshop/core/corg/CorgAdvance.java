package com.yangtu.nearbyshop.core.corg;

import java.util.Map;

/**
 * Created by leo on 2017/12/26.
 */
public interface CorgAdvance {
    default Map<String, String> advance(Map<String, String> context){ return null;}
    default Map<String, String> queryAdvance(Map<String, String> context){ return null;}
    default Map<String, String> balanceQry(Map<String, String> context){ return null;}
}
