package com.yangtu.nearbyshop.core.corg;


import java.util.Map;

/**
 * create by helicheng on 2017/9/11.
 */
public interface CorgMercChangeable {
    /**
     * 新增商户机构
     * @param context
     * @return
     */
    default Map<String, String> addMerc(Map<String, String> context) throws  Exception{ return null;};

    default Map<String, String> updMerc(Map<String, String> context){ return null;};

    default Map<String, String> updMercStl(Map<String, String> context){ return null;};

    default Map<String, String> uploadPic(Map<String, String> context){ return null;};

    default Map<String, String> queryMerc(Map<String, String> context){ return null;};

    default Map<String, String> tieCard(Map<String, String> context){ return null;};

    default Map<String, String> tieCardSms(Map<String, String> context){ return null;};

    default Map<String, String> queryTieCard(Map<String, String> context){ return null;};

    default Map<String,Object> lbnkNoGet(Map<String, String> context){ return null;};

    default Map<String,String> bindDirectory(Map<String, String> context){ return null;};

    default Map<String,String> bindPayAppid(Map<String, String> context){ return null;};
    }
