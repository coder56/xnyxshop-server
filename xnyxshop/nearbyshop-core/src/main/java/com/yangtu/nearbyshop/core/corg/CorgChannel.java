package com.yangtu.nearbyshop.core.corg;

import java.util.Map;

/**
 *机构通道-
 * create by helicheng on 2017/9/1.
 */
public interface CorgChannel {

    default Map<String, String> quickPayOpen(Map<String, String> context){ return null;}
    default Map<String, String> verfiySMS(Map<String, String> context){ return null;}
    default Map<String, String> sendSMS(Map<String, String> context){ return null;}
    default Map<String, String> quickPay(Map<String, String> context) throws Exception { return null;}
    default Map<String, String> paySendSMS(Map<String, String> context) throws Exception { return null;}
    default Map<String, String> proxyPay(Map<String, String> context){ return null;}
    default Map<String, String> queryOrder(Map<String, String> context) throws Exception { return null;}
    default Map<String, String> realAuth(Map<String, String> context){ return null;}
    default Map<String, String> openCard(Map<String, String> context) throws Exception { return null;}
    default Map<String, String> openCardSMS(Map<String, String> context) throws Exception { return null;}
    default Map<String, String> quickPayFront(Map<String, String> context) throws Exception { return null;}
    default Map<String, String> unionPay(Map<String, String> context) throws Exception { return null;}
    default Map<String, String> qrCodeGet(Map<String, String> context) throws Exception { return null;}
    default Map<String, String> scanQrPay(Map<String, String> context) throws Exception { return null;}
    default Map<String, String> inteGrationPay(Map<String, String> context) throws Exception { return null;}
    default Map<String, String> refundApply(Map<String, String> context) throws Exception { return null;}
    default Map<String, String> refundQry(Map<String, String> context) throws Exception { return null;}
    default Map<String, String> addFzMerclist(String mercNo){ return null;}

}
