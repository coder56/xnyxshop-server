package com.yangtu.nearbyshop.core.corg;

/**
 * Created by zjc on 2018/03/19.
 */
public interface Const {

    String  GE_TUI_URL = "http://sdk.open.api.igexin.com/apiex.htm";

    String CMM_PARAM_SIGN = "sign";

    /**
     * 支付超时时间
     */
    String CMM_EXPIRE_TIME = "9";

    /**
     * retCode 返回码
     */
    String CMM_PARAM_RETURN_CODE="retCode";



    /**
     * retMsg 返回信息
     */
    String CMM_PARAM_RETURN_MSG="retMsg";

    /**
     * upTranId 返回上游订单号
     */
    String UP_CORG_TRAN_ID="upTranId";

    /**
     * mercNo 返回信息
     */
    String CMM_PARAM_MERC_NO="mercNo";

    /**
     * 成功返回码
     */
    String SUC_RETURN_CODE="0000";

    /**
     * 验证码ehcache
     */
    String SMS_VERIFICATION_CODE = "smsVerificationCode";

    /**
     * 验证码错误次数
     */
    int SMS_VERIFICATION_ERROR_COUNT = 3;

    /**
     * 验证码长度
     */
    int SMS_CODE_LENGTH = 6;

    /**
     * 登录密码输入错误次数
     */
    int PWD_ERROR_MAX_CNT = 3;



    String ZIP_START_PATH = "./upload/";//URL包路径
}
