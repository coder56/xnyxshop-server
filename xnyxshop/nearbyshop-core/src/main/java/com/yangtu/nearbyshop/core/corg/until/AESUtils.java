package com.yangtu.nearbyshop.core.corg.until;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by zjc on 2017/10/18.
 */
public class AESUtils {


        private static final String KEY_ALGORITHM = "AES";
        private static final String DEFAULT_CIPHER_ALGORITHM = "AES/ECB/PKCS5Padding";//默认的加密算法


        /**
         * AES 加密操作
         *
         * @param content 待加密内容
         * @param encryptKey 加密密码
         * @return 返回Base64转码后的加密数据
         */
        public static String encrypt(String content, String encryptKey) {
            try {
                Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);// 创建密码器

                byte[] byteContent = content.getBytes("utf-8");

                cipher.init(Cipher.ENCRYPT_MODE, getSecretKey(encryptKey));// 初始化为加密模式的密码器

                byte[] result = cipher.doFinal(byteContent);// 加密

                return Base64.encodeBase64String(result);//通过Base64转码返回
            } catch (Exception ex) {
            }

            return null;
        }


        /**
         * AES 解密操作
         *
         * @param content
         * @param encryptKey
         * @return
         */
        public static String decrypt(String content, String encryptKey) {

            try {
                //实例化
                Cipher cipher = Cipher.getInstance(DEFAULT_CIPHER_ALGORITHM);

                //使用密钥初始化，设置为解密模式
                cipher.init(Cipher.DECRYPT_MODE, getSecretKey(encryptKey));

                //执行操作
                byte[] result = cipher.doFinal(Base64.decodeBase64(content));

                return new String(result, "utf-8");
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            return null;
        }


        /**
         * 生成加密秘钥
         *
         * @return
         */
        private static SecretKeySpec getSecretKey(final String encryptKey) {
            //返回生成指定算法密钥生成器的 KeyGenerator 对象
            KeyGenerator kg = null;

            try {
                kg = KeyGenerator.getInstance(KEY_ALGORITHM);
                SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
                secureRandom.setSeed(encryptKey.getBytes());
                //AES 要求密钥长度为 128
                kg.init(128, secureRandom);

                //生成一个密钥
                SecretKey secretKey = kg.generateKey();
//                System.out.println(Base64.encodeBase64String(secretKey.getEncoded()));
                return new SecretKeySpec(secretKey.getEncoded(), KEY_ALGORITHM);// 转换为AES专用密钥
            } catch (NoSuchAlgorithmException ex) {
            }

            return null;
        }


    /**
     * 签名键值对
     * @param dataMap 签名键值对
     * @param encriptkey 加密key
     * @return
     */
    public static String sign(Map<String,String> dataMap, String encriptkey){
        Map<String,String> tMap=new TreeMap<String,String>(dataMap);
        StringBuffer buf = new StringBuffer();
        for (String key : tMap.keySet()) {
            if(StringUtils.isBlank(tMap.get(key))){
                continue;
            }
            buf.append(key).append("=").append((String) tMap.get(key)).append("&");
        }
        if(buf.length()<1){
            return "";
        }
        String signatureStr = buf.substring(0, buf.length() - 1);
        System.out.println("AES签名字段："+signatureStr);
        return AESUtils.encrypt(signatureStr,encriptkey);
    }

    public static void main(String[] args) {
        String str = AESUtils.encrypt("Interface=YPTmdPay8Interface&bankBranch=天心区&bankCard=6221551100221938&bankCity=长沙市&bankProv=湖南省&idcard=622223198805230014&installCity=长沙市&installCounty=天心区&installProvince=湖南省&orderType=YPTType1&phone=13548661930&sign=E3A8C616B796CB56C0B16E646112B0AB", "acccd6fa0caf52a0e5e5fda8bd3ff55a");
        System.out.println(str);
    }

}

