package com.yangtu.nearbyshop.core.qcode;

import cn.binarywang.wx.miniapp.api.WxMaService;
import com.yangtu.nearbyshop.core.storage.StorageService;
import com.yangtu.nearbyshop.core.system.SystemConfig;
import com.yangtu.nearbyshop.db.domain.NearbyshopGoods;
import com.yangtu.nearbyshop.db.domain.NearbyshopGroupon;
import com.yangtu.nearbyshop.db.domain.NearbyshopGrouponRules;
import com.yangtu.nearbyshop.db.service.NearbyshopGoodsService;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.net.URL;
import java.text.AttributedString;

@Service
public class QCodeService {
    @Autowired
    WxMaService wxMaService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private NearbyshopGoodsService goodsService;

    public String createGrouponShareImage(String goodName, String goodPicUrl, NearbyshopGroupon groupon, NearbyshopGrouponRules grouponRules) {
        try {
            //创建该商品的二维码
            File file = wxMaService.getQrcodeService().createWxaCodeUnlimit("groupon," + groupon.getId(), "pages/index/index");
            FileInputStream inputStream = new FileInputStream(file);
            NearbyshopGoods goods = goodsService.findById(grouponRules.getGoodsId());
            //将商品图片，商品名字,商城名字画到模版图中
            byte[] imageData = drawPicture(inputStream, goodPicUrl, goodName, goods.getRetailPrice(), goods.getCounterPrice());
            ByteArrayInputStream inputStream2 = new ByteArrayInputStream(imageData);
            //存储分享图
            String url = storageService.store(inputStream2, imageData.length, "image/jpeg", getKeyName(groupon.getId().toString()));

            return url;
        } catch (WxErrorException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }


    /**
     * 创建商品分享图
     *
     * @param goodId
     * @param goodPicUrl
     * @param goodName
     */
    public String createGoodShareImage(String goodId, String goodPicUrl, String goodName) {
        if (!SystemConfig.isAutoCreateShareImage())
            return "";

        try {
            //创建该商品的二维码
            File file = wxMaService.getQrcodeService().createWxaCodeUnlimit("goods," + goodId, "pages/index/index");
            FileInputStream inputStream = new FileInputStream(file);
            NearbyshopGoods goods = goodsService.findById(Integer.parseInt(goodId));
            //将商品图片，商品名字,商城名字画到模版图中
            byte[] imageData = drawPicture(inputStream, goodPicUrl, goodName, goods.getRetailPrice(), goods.getCounterPrice());
            ByteArrayInputStream inputStream2 = new ByteArrayInputStream(imageData);
            //存储分享图
            String url = storageService.store(inputStream2, imageData.length, "image/jpeg", getKeyName(goodId));

            return url;
        } catch (WxErrorException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * 创建商品分享图封面
     *
     * @param goodId
     * @param goodPicUrl
     * @param goodName
     */
    public String createGoodAdverImage(String goodId, String goodPicUrl, String goodName) {
        if (!SystemConfig.isAutoCreateShareImage())
            return "";

        try {
            //创建该商品的二维码
            //File file = wxMaService.getQrcodeService().createWxaCodeUnlimit("goods," + goodId, "pages/index/index");
            //FileInputStream inputStream = new FileInputStream(file);
            NearbyshopGoods goods = goodsService.findById(Integer.parseInt(goodId));
            //将商品图片，商品名字,商城名字画到模版图中
            byte[] imageData = drawPictureShare(goodPicUrl,goods.getRetailPrice(), goods.getCounterPrice());
            ByteArrayInputStream inputStream2 = new ByteArrayInputStream(imageData);
            //存储分享图
            String url = storageService.store(inputStream2, imageData.length, "image/jpeg", getKeyName("SHARE_"+goodId));

            return url;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return "";
    }

    private String getKeyName(String goodId) {
        return "GOOD_QCODE_" + goodId + ".jpg";
    }

    /**
     * 将商品图片，商品名字画到模版图中
     *
     * @param goodPicUrl 商品图片地址
     * @return
     * @throws IOException
     */
    private byte[] drawPictureShare(String goodPicUrl, BigDecimal curPrice, BigDecimal oldPrice) throws IOException {
        //底图
        ClassPathResource backResource = new ClassPathResource("shareback.png");
        BufferedImage back = ImageIO.read(backResource.getInputStream());

        //红图
        ClassPathResource redResource = new ClassPathResource("red.png");
        BufferedImage red = ImageIO.read(redResource.getInputStream());


        //商品图片
        URL goodPic = new URL(goodPicUrl);
        BufferedImage goodImage = ImageIO.read(goodPic);

        //小程序二维码
        //BufferedImage qrCodeImage = ImageIO.read(qrCodeImg);

        // --- 画图 ---

        //底层为商品图片的大小
        BufferedImage baseImage = new BufferedImage(back.getWidth(), back.getHeight(), BufferedImage.TYPE_4BYTE_ABGR_PRE);

        //画上商品图片
        drawImgInImg(baseImage, goodImage, 0, 0, back.getWidth(), back.getHeight());

        //画上商品图片
//        drawImgInImg(baseImage, goodImage, 71, 69, 660, 660);
         drawImgInImg(baseImage, red, 0, 440, 800, 160);

        //画上小程序二维码
//        drawImgInImg(baseImage, qrCodeImage, 520, 670, 200, 200);

//        if (goodName.length() > 12) {
//            //写上商品名称
//            drawTextInImg(baseImage, goodName.substring(0, 12), 30, 710);
//            //写上商品名称
//            drawTextInImg(baseImage, goodName.substring(12, goodName.length()), 30, 750);
//        }else {
//            //写上商品名称
//            drawTextInImg(baseImage, goodName, 30, 750);
//        }

        int len = curPrice.toPlainString().length() + 1;



        //写上新价格
        drawTextCurPriceShare(baseImage, "￥" +curPrice.toPlainString() , 50, 545,80);

        //写上老价格
        drawTextCurPriceShareDelete(baseImage, "￥" +oldPrice.toPlainString(), 80+(len*55), 545,30);
        //写上商城名称
        //drawTextInImgCenter(baseImage, SystemConfig.getMallName(), 90);


        //转jpg
        BufferedImage result = new BufferedImage(baseImage.getWidth(), baseImage
                .getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        result.getGraphics().drawImage(baseImage, 0, 0, null);
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        ImageIO.write(result, "jpg", bs);

        //最终byte数组
        return bs.toByteArray();
    }

    /**
     * 将商品图片，商品名字画到模版图中
     *
     * @param qrCodeImg  二维码图片
     * @param goodPicUrl 商品图片地址
     * @param goodName   商品名称
     * @return
     * @throws IOException
     */
    private byte[] drawPicture(InputStream qrCodeImg, String goodPicUrl, String goodName, BigDecimal curPrice, BigDecimal oldPrice) throws IOException {
        //底图
        ClassPathResource redResource = new ClassPathResource("back.png");
        BufferedImage red = ImageIO.read(redResource.getInputStream());


        //商品图片
        URL goodPic = new URL(goodPicUrl);
        BufferedImage goodImage = ImageIO.read(goodPic);

        //小程序二维码
        BufferedImage qrCodeImage = ImageIO.read(qrCodeImg);

        // --- 画图 ---

        //底层空白 bufferedImage
        BufferedImage baseImage = new BufferedImage(red.getWidth(), red.getHeight(), BufferedImage.TYPE_4BYTE_ABGR_PRE);

        //画上图片
        drawImgInImg(baseImage, red, 0, 0, red.getWidth(), red.getHeight());

        //画上商品图片
//        drawImgInImg(baseImage, goodImage, 71, 69, 660, 660);
        drawImgInImg(baseImage, goodImage, 30, 190, 690, 438);

        //画上小程序二维码
        drawImgInImg(baseImage, qrCodeImage, 520, 670, 200, 200);

        if (goodName.length() > 12) {
            //写上商品名称
            drawTextInImg(baseImage, goodName.substring(0, 12), 30, 710);
            //写上商品名称
            drawTextInImg(baseImage, goodName.substring(12, goodName.length()), 30, 750);
        }else {
            //写上商品名称
            drawTextInImg(baseImage, goodName, 30, 750);
        }

        //写上商品名称
        drawTextCurPrice(baseImage, curPrice.toPlainString() + "元", 164, 812);

        //写上商品名称
        drawTextOldPrice(baseImage, oldPrice.toPlainString() + "元", 126, 863);
        //写上商城名称
        drawTextInImgCenter(baseImage, SystemConfig.getMallName(), 90);


        //转jpg
        BufferedImage result = new BufferedImage(baseImage.getWidth(), baseImage
                .getHeight(), BufferedImage.TYPE_3BYTE_BGR);
        result.getGraphics().drawImage(baseImage, 0, 0, null);
        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        ImageIO.write(result, "jpg", bs);

        //最终byte数组
        return bs.toByteArray();
    }

    private void drawTextInImgCenter(BufferedImage baseImage, String textToWrite, int y) {
        Graphics2D g2D = (Graphics2D) baseImage.getGraphics();
        g2D.setColor(new Color(255, 61, 51));

        String fontName = "Microsoft YaHei";

        Font f = new Font(fontName, Font.PLAIN, 28);
        g2D.setFont(f);
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        // 计算文字长度，计算居中的x点坐标
        FontMetrics fm = g2D.getFontMetrics(f);
        int textWidth = fm.stringWidth(textToWrite);
        int widthX = (baseImage.getWidth() - textWidth) / 2;
        // 表示这段文字在图片上的位置(x,y) .第一个是你设置的内容。

        g2D.drawString(textToWrite, widthX, y);
        // 释放对象
        g2D.dispose();
    }

    private void drawTextInImg(BufferedImage baseImage, String textToWrite, int x, int y) {
        Graphics2D g2D = (Graphics2D) baseImage.getGraphics();
        g2D.setColor(new Color(167, 136, 69));

        //TODO 注意，这里的字体必须安装在服务器上
        g2D.setFont(new Font("Source Han Sans CN Normal", Font.PLAIN, 40));
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2D.drawString(textToWrite, x, y);
        g2D.dispose();
    }

    private void drawTextCurPriceShare(BufferedImage baseImage, String textToWrite, int x, int y,int fontsize) {
        Graphics2D g2D = (Graphics2D) baseImage.getGraphics();
        g2D.setColor(new Color(245, 245, 245));

        //TODO 注意，这里的字体必须安装在服务器上
        g2D.setFont(new Font("Source Han Sans CN Normal", Font.PLAIN, fontsize));
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2D.drawString(textToWrite, x, y);
        g2D.dispose();
    }

    private void drawTextCurPriceShareDelete(BufferedImage baseImage, String textToWrite, int x, int y,int fontsize) {
        Graphics2D g2D = (Graphics2D) baseImage.getGraphics();
        g2D.setColor(new Color(245, 245, 245));
        Font plainFont = new Font("SimSun", Font.PLAIN, fontsize);
        AttributedString as = new AttributedString(textToWrite);
        as.addAttribute(TextAttribute.FONT, plainFont);
        as.addAttribute(TextAttribute.STRIKETHROUGH, TextAttribute.STRIKETHROUGH_ON, 0, textToWrite.length());

        //TODO 注意，这里的字体必须安装在服务器上
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2D.drawString(as.getIterator(), x, y);
        g2D.dispose();
    }

    private void drawTextCurPrice(BufferedImage baseImage, String textToWrite, int x, int y) {
        Graphics2D g2D = (Graphics2D) baseImage.getGraphics();
        g2D.setColor(new Color(255, 61, 51));

        //TODO 注意，这里的字体必须安装在服务器上
        g2D.setFont(new Font("Source Han Sans CN Normal", Font.PLAIN, 42));
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2D.drawString(textToWrite, x, y);
        g2D.dispose();
    }

    private void drawTextOldPrice(BufferedImage baseImage, String textToWrite, int x, int y) {
        Graphics2D g2D = (Graphics2D) baseImage.getGraphics();
        g2D.setColor(new Color(102, 102, 102));

        //TODO 注意，这里的字体必须安装在服务器上
        g2D.setFont(new Font("Source Han Sans CN Normal", Font.PLAIN, 30));
        g2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g2D.drawString(textToWrite, x, y);
        g2D.dispose();
    }

    private void drawImgInImg(BufferedImage baseImage, BufferedImage imageToWrite, int x, int y, int width, int heigth) {
        Graphics2D g2D = (Graphics2D) baseImage.getGraphics();
        g2D.drawImage(imageToWrite, x, y, width, heigth, null);
        g2D.dispose();
    }
}
