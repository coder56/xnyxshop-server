package com.yangtu.nearbyshop.core;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.yangtu.nearbyshop.db", "com.yangtu.nearbyshop.core"})
@MapperScan("com.yangtu.nearbyshop.db.dao")
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}