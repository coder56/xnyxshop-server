package com.yangtu.nearbyshop.core.corg.until;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.beanutils.BeanUtils;
import org.springframework.cglib.beans.BeanMap;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * Created by leo on 2017/10/16.
 */
public class BeanTools {

    /**
     * 将对象装换为map
     * @param bean
     * @return
     */
    public static <T> Map<String, Object> beanToMap(T bean) {
        Map<String, Object> map = Maps.newHashMap();
        if (bean != null) {
            BeanMap beanMap = BeanMap.create(bean);
            for (Object key : beanMap.keySet()) {
                map.put(key+"", beanMap.get(key));
            }
        }
        return map;
    }

    /**
     * 将对象装换为map
     * @param bean
     * @return
     */
    public static <T> Map<String, String> beanToMapStr(T bean) {
        Map<String, String> map = Maps.newHashMap();
        if (bean != null) {
            BeanMap beanMap = BeanMap.create(bean);
            for (Object key : beanMap.keySet()) {
                if(beanMap.get(key)==null){
                    continue;
                }
                if(beanMap.get(key) instanceof BigDecimal){
                    map.put(key+"", (String.valueOf(beanMap.get(key))));
                }else if(beanMap.get(key) instanceof Integer){
                    map.put(key+"", (String.valueOf(beanMap.get(key))));
                } else if(beanMap.get(key) instanceof String){
                    map.put(key+"", (String)beanMap.get(key));
                }else{
                    continue;
                }
            }
        }
        return map;
    }

    public static <T> void copyMapToBean(Map<String,String> map,T bean) throws Exception {
        try {
            Field[] fields = bean.getClass().getDeclaredFields();
            for (int i = 0; i < fields.length; i++) {
                Class claz = fields[i].getType();
                String name = fields[i].getName();
                if(map.get(name)==null){
                    continue;
                }
                if (BigDecimal.class.equals(claz)) {
                    BeanUtils.setProperty(bean, name, new BigDecimal(map.get(name)));
                } else if (String.class.equals(claz)) {
                    BeanUtils.setProperty(bean, name, map.get(name));
                } else if (Integer.class.equals(claz)) {
                    BeanUtils.setProperty(bean, name, Integer.valueOf(map.get(name)));
                } else {
                    continue;
                }
            }
        }catch (Exception e){
            throw new Exception("系统异常,反射类异常");
        }
    }
    /**
     * 将map装换为javabean对象
     * @param map
     * @param bean
     * @return
     */
    public static <T> T mapToBean(Map<String, Object> map,T bean) {
        BeanMap beanMap = BeanMap.create(bean);
        beanMap.putAll(map);
        return bean;
    }

    /**
     * 将List<T>转换为List<Map<String, Object>>
     * @param objList
     * @return
     */
    public static <T> List<Map<String, Object>> objectsToMaps(List<T> objList) {
        List<Map<String, Object>> list = Lists.newArrayList();
        if (objList != null && objList.size() > 0) {
            Map<String, Object> map = null;
            T bean = null;
            for (int i = 0,size = objList.size(); i < size; i++) {
                bean = objList.get(i);
                map = beanToMap(bean);
                list.add(map);
            }
        }
        return list;
    }


}
