package com.yangtu.nearbyshop.core;

import cn.binarywang.wx.miniapp.config.WxMaConfig;
import com.github.binarywang.wxpay.config.WxPayConfig;
import com.yangtu.nearbyshop.core.qcode.QCodeService;
import com.yangtu.nearbyshop.core.storage.AliyunStorage;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class GenImageTest {
    @Autowired
    private QCodeService qCodeService;

    @Autowired
    private WxMaConfig wxMaConfig;


    @Test
    public void test() throws IOException {
        String picurl = "https://yangtuoss.oss-cn-shanghai.aliyuncs.com/48jngf0lt8ll2709zjvt.jpg";
        String goodsId = "1181207";
        String shareUrl = qCodeService.createGoodAdverImage(goodsId,picurl, "测试长长长");
        System.out.println(shareUrl);

        //System.out.println("apid=" + wxMaConfig.getAppid());
    }

    @Test
    public void testRead(){
        String ps = "classpath:apiclient_cert.p12";
        Object inputStream;
        if (ps.startsWith("classpath:")) {
            String path = StringUtils.removeFirst(ps, "classpath:");
            if (!path.startsWith("/")) {
                path = "/" + path;
            }

            inputStream = WxPayConfig.class.getResourceAsStream(path);
            if (inputStream == null) {
                System.out.println("no file");
            }
        } else {
            try {
                File file = new File(ps);
                if (!file.exists()) {
                    System.out.println("no file2");
                }

                inputStream = new FileInputStream(file);
            } catch (IOException var12) {
                System.out.println("no file3");
            }
        }
    }
}
