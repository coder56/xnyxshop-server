package com.yangtu.nearbyshop.core;

import java.math.BigDecimal;

public class TestJava {
    public static void main(String[] args) {
        BigDecimal bg = new BigDecimal("0");
        BigDecimal b1 = new BigDecimal("0.01");
        System.out.println(bg.add(b1).setScale(2, BigDecimal.ROUND_UP));
    }
}
